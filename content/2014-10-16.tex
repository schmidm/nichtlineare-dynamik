\chapter{Dissipative Systeme}

\section{Einleitung}

Der Begriff \acct{Chaos} leitet sich aus dem griechischen ab und hatte
in der Antike verschiedene Bedeutungen:
\begin{itemize}
\item Der unendlich leere Raum, der vor aller Zeit existierte.
\item Rohe, ungeformte Masse, in die der Schöpfer der Welt Ordnung und
  Harmonie bringt.
\end{itemize}
Heute verwenden wir den Begirff weniger spirituell. Man bezeichnet so
einen Zustand der Unordnung und Irregularität.

Wie erzeugen wir aber Chaos? Eine Möglichkeit ist durch einen
Zufallsprozess. Die Ordnung eines neuen, sortierten Kartenspiels wird
zerstört durch \enquote{mischen}. Dies ist äquivalent zu einer
zufälligen Änderung der Reihenfolge. Zufallszahlen haben ebenfalls
keine Ordnung im Sinne, dass sie nicht geordnet sind (im Gegensatz zu,
z.B.\ $x_n=2^n$). Ein weiterer Zufallsprozess ist das Rauschen.
Darunter versteht man eine Funktion ohne reguläre Strukturen
(z.B.\ Verstärkerrauschen, erzeugt durch zufällige thermische
Prozesse).

Für uns fallen Zufallsprozesse in die Kategorie \acct[Chaos!nicht
deterministisch]{nicht deterministisches Chaos}. In diesem Skript
wollen wir uns jedoch mit dem
\acct[Chaos!deterministisch]{deterministischen Chaos} beschäftigen.
Determinismus bedeutet, dass es eine Vorschrift (Differentialgleichung
ohne iterative Abbildung) gibt nach der sich das Verhalten des Systems
aus gegebenen Anfangsbedingungen berechnen lässt.  Die naive (und
falsche) Annahme hierbei ist, dass deterministische Gesetze in einem
regulären Verhalten resultieren.  Ein Gegenbeispiel lieferte Poincaré
1892 mit dem Dreikörperproblem.

\begin{theorem}[Definition]
  Als deterministisches Chaos bezeichnet man die Unordnung und
  Irregularität bei Systemen, die deterministischen Gesetzen (ohne
  Zufallseinwirkung) gehorchen.
\end{theorem}

\begin{example}
  Es folgen Beispiele für nichtlineare Systeme mit (möglicherweise) deterministischem Chaos.
  \begin{itemize}
  \item Getriebenes Pendel
  \item Flüssigkeiten beim Einsetzen der Turbulenz (Meteorologie)
  \item Laser
  \item Nichtlineare optische Systeme
  \item Chemische Reaktionen
  \item Klassische Vielteilchensysteme (z.B.\ Dreikörperproblem)
  \item Teilchenbeschleuniger
  \item Modelle zur Populationsdynamik
  \end{itemize}
\end{example}

Die Irregularität beruht auf der Eigenschaft nichtlinearer Systeme
anfänglich benachbarte Bahnen exponentiell schnell zu separieren.
Die Anfangsbedingungen lassen sich nicht beliebig genau angeben.
Für das Verhalten nichtlinearer Systeme ist in der Regel nur eine
Kurzzeit- und keine Langzeitvorhersage möglich. Dies bezeichnet man
nach Lorentz (1963) als Schmetterlingseffekt.

\begin{notice}[Fragen:]
  \begin{itemize}
  \item Kann man z.B.\ aus den Differentialgleichungen vorhersagen, ob
    ein System deterministisches Chaos zeigt oder nicht?
  \item Gibt es quantitative Maße für die Chaotizität eines Systems
    bzw.\ einer Bewegung?
  \end{itemize}
\end{notice}

Eine Mindmap zur Klassifikation von Systemen, die deterministisches Chaos zeigen findet sich in Abbildung~\ref{fig:1.1}.

\begin{figure}[p]
  \centering
  \begin{tikzpicture}[mindmap]
    \begin{scope}[
      every node/.style={concept,execute at begin node=\hskip0pt},
      root concept/.append style={
        concept color=black,fill=white,line width=1ex,text=black,font=\scshape
      },
      level 1/.append style={font=\small},
      level 2/.append style={font=\footnotesize},
      text=white,
      grow cyclic,
      ]
      \node [root concept] {Nichtlineare Systeme} % root
      child [concept color=DarkOrange3] { node {Dissipative Systeme}
        child { node {Bifukrationen} }
        child { node {Intermittenz} }
        child { node {Seltsame Attraktoren} }
      }
      child [concept color=MidnightBlue] { node {Konservative Systeme}
        child { node {Klassische Systeme} }
        child { node {Quantensysteme} }
      };
    \end{scope}
  \end{tikzpicture}
  \caption{Klassifikation von Systemen, die deterministisches Chaos zeigen.}
  \label{fig:1.1}
\end{figure}

\paragraph{Dissipative Systeme}
Ein dissipatives System ist abhängig von (mindestens) einem externen
Kontrollparameter $r$.
Es gibt verschiedene Wege ins Chaos, die dann universell sind für
jeweils eine Vielzahl experimentell realisierbarer Systeme.

Klassifikation dieser Wege:
\begin{itemize}
\item Bifurkationen, Periodenverdopplungen
\item Intermittenz (reguläre und chaotische Intermittenz)
\item Seltsame Attraktoren
\end{itemize}

\paragraph{Konservative Systeme}
Chaos ist möglich in nicht integrablen Hamiltonschen Systemen. Das
KAM-Theorem erklärt die Koexistenz regulärer und chaotischer
Strukturen im Phasenraum. Die Frage nach dem Verhalten von
Quantensystemen, deren klassischer Grenzfall Chaos zeigt führt auf das
Quantenchaos.

\section{Experimente und einfache Modelle zum deterministischen Chaos}

\begin{enumerate}
\item Das getriebene Pendel
  \begin{equation*}
    \ddot\theta + 2 \gamma \dot\theta + \frac{g}{\ell} \sin\theta = \alpha \sin \omega t.
  \end{equation*}
\item Das Rayleigh-Bénard-System in einer Zelle. Lorentz-Modell (3D):
  \begin{equation*}
    \dot{\bm x} = f(\bm x)
  \end{equation*}
\item Getriebene chemische Reaktionen: Die
  Belousov-Zhabotionsky-Reaktion, vereinfacht
  \begin{equation*}
    A + B \leftrightharpoons C
  \end{equation*}
  Differentialgleichung für die Konzentrationen:
  \begin{equation*}
    \dot c_{\substack{A\\B\\C}} = f_{\substack{A\\B\\C}}(c_A,c_b,c_C).
  \end{equation*}
  Diese ist nichtlinear durch Produkte der Konzentrationen, z.B.\
  $c_A \cdot c_B$.
\item Hamiltonsche Systeme: Hénon-Heiles-System
  \begin{equation*}
    H = \underbrace{\frac12 (p_1^2+p_2^2)+\frac12(q_1^2+q_2^2)}_{\text{Harmonischer Oszillator}}
    + \underbrace{q_1^2q_2 - \frac13 q_2^3}_{\mathclap{\text{Nichtlinearer Kopplungsterm}}}
  \end{equation*}
\end{enumerate}

In der Poincaré-Abbildung entsprechen reguläre Strukturen der
Integrabilität. Stochastische Strukturen deuten auf Chaos hin.

\minisec{Das Rayleigh-Bénard-System in einer Zelle}

\begin{minipage}{.4\linewidth}
  \centering
  \begin{tikzpicture}[gfx]
    \draw[MidnightBlue] (0,0) -- node[below] {$T+\delta T$} (3,0);
    \draw[MidnightBlue] (0,2) -- node[above] {$T$} (3,2);
    \foreach \i in {.5,1.5,2.5} {
      \draw[DarkOrange3,->] (\i,.2) -- (\i,1.8);
    }
  \end{tikzpicture}
\end{minipage}%
\begin{minipage}{.6\linewidth}
  Flüssigkeitsschicht zwischen zwei Platten mit unterschiedlichen
  Temperaturen im Gravitationsfeld. Die Flüssigkeit hat eine endliche
  Viskosität. Ein weiterer Anstieg von $\delta T$ resultiert in
  chaotischer Bewegung (Turbulenz).
\end{minipage}

\noindent
\begin{minipage}{.4\linewidth}
  \centering
  \begin{tikzpicture}[gfx]
    \draw[MidnightBlue] (0,0) -- node[below] {$T+\delta T$} (3,0);
    \draw[MidnightBlue] (0,2) -- node[above] {$T$} (3,2);
    \draw[DarkOrange3,->] (1.25,1) arc (0:360:.5);
    \draw[DarkOrange3,->] (2.75,1) arc (0:360:.5);
  \end{tikzpicture}
\end{minipage}%
\begin{minipage}{.6\linewidth}
  Konvektionsrollen.
\end{minipage}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: