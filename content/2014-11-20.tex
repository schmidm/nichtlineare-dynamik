\section{Intermittenzroute zum Chaos}

Als \acct{Intermittenz} bezeichnet man die Unterbrechung einer
periodischen Bewegung durch chaotische Phasen. Mit Intermittenzroute
bezeichnet man die Zunahme der chaotischen Phasen bei Variation eines
Kontrollparameters bis schließlich die Bewegung vollkommen chaotisch
wird. Man unterscheidet zwischen drei verschiedenen Typen von
Intermittenz je nach Typ der verantwortlichen Bifurkation (nach Pomeau
und Manneville, 1980):
\begin{description}
\item[Typ I:] Tangentenbifurkation,
\item[Typ II:] Hopf-Bifurkation,
\item[Typ III:] Inverse Periodenverdopplung (Gabelbifurkation).
\end{description}

\paragraph{Typ I} Ein Beispiel wäre die logistische Abbildung beim
Verschwinden des $p=3$-Zyklus bei $r = r_C = 1 + \sqrt{8} \approx
\num{3.83}$.
\begin{align*}
  \bar x_{n+1} = \varepsilon + \bar x_n + \mu \bar x_n^2
\end{align*}

\begin{center}
  \begin{tikzpicture}[gfx]
    \node at (-1.5,1) {$\varepsilon > 0$};
    \draw[->] (-2,0) -- (2,0) node [above] {$x_n$};
    \draw[->] (0,-1.5) -- (0,1.5) node [right] {$x_{n+1}$};
    \draw[name path=A] (-2,-1.5) -- coordinate[pos=.2] (A) (2,1.5);
    \draw[name path=B,DarkOrange3] (-2,-.5) to[bend right] (1,1.5);
    \begin{scope}[MidnightBlue]
      \path[name path=C] (A) -- (A |- 0,1.5);
      \draw[name intersections={of=B and C,name=i1}] (A) -- (i1-1);
      \path[name path=C] (i1-1) -- (i1-1 -| 2,0);
      \draw[name intersections={of=A and C,name=i2}] (i1-1) -- (i2-1);
      \path[name path=C] (i2-1) -- (i2-1 |- 0,1.5);
      \draw[name intersections={of=B and C,name=i3}] (i2-1) -- (i3-1);
      \path[name path=C] (i3-1) -- (i3-1 -| 2,0);
      \draw[name intersections={of=A and C,name=i4}] (i3-1) -- (i4-1);
      \path[name path=C] (i4-1) -- (i4-1 |- 0,1.5);
      \draw[name intersections={of=B and C,name=i5}] (i4-1) -- (i5-1);
      \path[name path=C] (i5-1) -- (i5-1 -| 2,0);
      \draw[name intersections={of=A and C,name=i6}] (i5-1) -- (i6-1);
      \path[name path=C] (i6-1) -- (i6-1 |- 0,1.5);
      \draw[name intersections={of=B and C,name=i7}] (i6-1) -- (i7-1);
      \path[name path=C] (i7-1) -- (i7-1 -| 2,0);
      \draw[name intersections={of=A and C,name=i8}] (i7-1) -- (i8-1);
      \path[name path=C] (i8-1) -- (i8-1 |- 0,1.5);
      \draw[name intersections={of=B and C,name=i9}] (i8-1) -- (i9-1);
      \path[name path=C] (i9-1) -- (i9-1 -| 2,0);
      \draw[name intersections={of=A and C,name=i10}] (i9-1) -- (i10-1);
      \path[name path=C] (i10-1) -- (i10-1 |- 0,1.5);
      \draw[name intersections={of=B and C,name=i11}] (i10-1) -- (i11-1);
      \path[name path=C] (i11-1) -- (i11-1 -| 2,0);
      \draw[name intersections={of=A and C,name=i12}] (i11-1) -- (i12-1);
    \end{scope}
    \draw[dotted] (i5-1) -- (i5-1 -| -.7,0) (i7-1) -- (i7-1 -| -.7,0);
    \path (i5-1 -| -.7,0) -- node[left] {$\varepsilon$} (i7-1 -| -.7,0);
  \end{tikzpicture}
\end{center}

Frage: Wie lang sind die nicht chaotischen (laminaren) Phasen, oder genauer was ist die Zahl der Iterationsschritte einer laminaren Phase?

Antwort:
\begin{align*}
  x_{n+1} &= x_n + \varepsilon + \mu x_n^2 \\
  \underbrace{x_{n+1}-x_n}_{\text{(*)}} &= \varepsilon + \mu x_n^2 \;,\quad \mu > 0
\end{align*}
Dabei ist (*) klein für Iterationen innerhalb des Tunnels $\diff x$.
\begin{align*}
  \frac{\diff x}{\diff n} &= \varepsilon + \mu x^2 \\
  \implies \int \frac{\diff x}{\varepsilon + \mu x^2} &= \int \diff n \\
  \implies n &\sim \int_{-\infty}^{\infty} \frac{\diff x}{\varepsilon + \mu x^2}
  = \frac{\pi}{\sqrt{\mu\varepsilon}} \\
  \implies \Aboxed{ n &\sim \frac{1}{\sqrt{\varepsilon}} }
\end{align*}
Dies ist die Zahl der Iterationsschritte einer laminaren Phase.

Laminares Signal: monoton wachsend
\begin{center}
  \begin{tikzpicture}[gfx]
    \draw[->] (0,0) -- (0,2) node [above] {$x_{n+1}$};
    \draw[->] (0,0) -- (3,0) node [right] {$n$};
    \draw plot[ycomb] coordinates {
      (.2,.2)
      (.4,.4)
      (.6,.6)
      (.8,.8)
      (1,1)
    };
  \end{tikzpicture}
\end{center}

\paragraph{Typ II} Wie bereits erwähnt tritt die Intermittenz in der
Hopf-Bifurkation auf. Betrachte eine zweidimensionale Abbildung in der
Umgebung der Hopf-Bifurkation
\begin{align*}
  r_{n+1} &= (1+\varepsilon) r_n + \mu r_n^3 \;,\quad \mu > 0 \\
  \vartheta_{n+1} &= \vartheta_n + \Omega \\
  r_{n+1} - r_n &= \varepsilon r_n + \mu r_n^3 \\
  \frac{\diff r}{\diff n} &= \varepsilon r + \mu r^3 \\
  n &\sim \int_{r_0}^\infty \frac{\diff r}{\varepsilon r + \mu r^3}
  \sim \frac{1}{\varepsilon}
\end{align*}

\begin{center}
  \begin{tikzpicture}[gfx,
    AC/.style={name intersections={of=A and C,name=#1}},
    BC/.style={name intersections={of=B and C,name=#1}}]
    \begin{scope}
      \node at (1,2.5) {$\varepsilon < 0$};
      \draw[->] (0,0) -- (3,0) node [right] {$x_n$};
      \draw[->] (0,0) -- (0,3) node [above] {$x_{n+1}$};
      \draw[name path=A] (0,0) -- (3,3);
      \draw[name path=B,DarkOrange3] (0,0) to[bend right=40] coordinate[pos=.2] (A) (2.5,3);
      \begin{scope}[MidnightBlue]
        \path[name path=C] (A) -- (A |- 0,3);
        \draw[AC=i1] (A) -- (i1-1);
        \path[name path=C] (i1-1) -- (i1-1 -| 3,0);
        \draw[BC=i2] (i1-1) -- (i2-1);
        \path[name path=C] (i2-1) -- (i2-1 |- 0,3);
        \draw[AC=i3] (i2-1) -- (i3-1);
        \path[name path=C] (i3-1) -- (i3-1 -| 3,0);
        \draw[BC=i4] (i3-1) -- (i4-1);
        \path[name path=C] (i4-1) -- (i4-1 |- 0,3);
        \draw[AC=i5] (i4-1) -- (i5-1);
        \path[name path=C] (i5-1) -- (i5-1 -| 3,0);
        \draw[BC=i6] (i5-1) -- (i6-1);
        \path[name path=C] (i6-1) -- (i6-1 |- 0,3);
        \draw[AC=i7] (i6-1) -- (i7-1);
        \path[name path=C] (i7-1) -- (i7-1 -| 3,0);
        \draw[BC=i8] (i7-1) -- (i8-1);
      \end{scope}
    \end{scope}
    \begin{scope}[shift={(4,0)}]
      \node at (1,2.5) {$\varepsilon > 0$};
      \draw[->] (0,0) -- (3,0) node [right] {$x_n$};
      \draw[->] (0,0) -- (0,3) node [above] {$x_{n+1}$};
      \draw[name path=A] (0,0) -- coordinate[pos=.2] (A) (3,2);
      \draw[name path=B,DarkOrange3] (0,0) to[bend right=10] (2,3);
      \begin{scope}[MidnightBlue]
        \path[name path=C] (A) -- (A |- 0,3);
        \draw[BC=i1] (A) -- (i1-1);
        \path[name path=C] (i1-1) -- (i1-1 -| 3,0);
        \draw[AC=i2] (i1-1) -- (i2-1);
        \path[name path=C] (i2-1) -- (i2-1 |- 0,3);
        \draw[BC=i3] (i2-1) -- (i3-1);
        \path[name path=C] (i3-1) -- (i3-1 -| 3,0);
        \draw[AC=i4] (i3-1) -- (i4-1);
        \path[name path=C] (i4-1) -- (i4-1 |- 0,3);
        \draw[BC=i5] (i4-1) -- (i5-1);
        \draw[name path=C] (i5-1) -- (i5-1 -| 3,0);
      \end{scope}
    \end{scope}
  \end{tikzpicture}
\end{center}

Laminares Signal: Spiralen
\begin{center}
  \begin{tikzpicture}[gfx]
    \draw[->] (0,0) -- (2,0) node[right] {$r_n$};
    \draw plot[domain=0:30,samples=100,smooth]
    ({exp(-.1*\x)*cos(\x r)},{exp(-.1*\x)*sin(\x r)});
  \end{tikzpicture}
\end{center}

\paragraph{Typ III} Hier tritt die Intermittenz bei der inversen
Periodenverdopplung auf (Gabelbifurkation).
\begin{align*}
  x_{n+1} &= - (1+\varepsilon) x_n - \mu x_n^3 \;,\quad \mu > 0 \\
  \left| \frac{\diff x}{\diff n} \right| &= c |x| + \mu |x|^3 \\
  \overset{\text{siehe Typ II}}{\implies} n &\sim \frac{1}{\varepsilon}
\end{align*}

\begin{center}
  \begin{tikzpicture}[gfx,
    AC/.style={name intersections={of=A and C,name=#1}},
    BC/.style={name intersections={of=B and C,name=#1}}]
    \begin{scope}
      \node at (1,2) {$\varepsilon < 0$};
      \draw[->] (-2,0) -- (2,0) node [right] {$x_n$};
      \draw[->] (0,-2) -- (0,2) node [above] {$x_{n+1}$};
      \draw (-2,-2) -- (2,2) (2,-2) -- (-2,2);
      \draw[DarkOrange3] plot[domain=-1.3:1.3] (\x,{-\x^3});
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \node at (1,2) {$\varepsilon > 0$};
      \draw[->] (-2,0) -- (2,0) node [right] {$x_n$};
      \draw[->] (0,-2) -- (0,2) node [above] {$x_{n+1}$};
      \draw[name path=A] (-2,-2) -- (2,2) (2,-2) -- coordinate[pos=.59] (A) (-2,2);
      \draw[name path=B,DarkOrange3] plot[domain=-1:1] (\x,{-\x^3-\x});
      \begin{scope}[MidnightBlue]
        \path[name path=C] (A) -- (A |- 0,3);
        \draw[BC=i1] (A) -- (i1-1);
        \path[name path=C] (i1-1) -- (i1-1 -| 3,0);
        \draw[AC=i2] (i1-1) -- (i2-1);
        \path[name path=C] (i2-1) -- (i2-1 |- 0,-3);
        \draw[BC=i3] (i2-1) -- (i3-1);
        \path[name path=C] (i3-1) -- (i3-1 -| -3,0);
        \draw[AC=i4] (i3-1) -- (i4-1);
        \path[name path=C] (i4-1) -- (i4-1 |- 0,3);
        \draw[BC=i5] (i4-1) -- (i5-1);
        \path[name path=C] (i5-1) -- (i5-1 -| 3,0);
        \draw[AC=i6] (i5-1) -- (i6-1);
        \path[name path=C] (i6-1) -- (i6-1 |- 0,-3);
        \draw[BC=i7] (i6-1) -- (i7-1);
        \path[name path=C] (i7-1) -- (i7-1 -| -3,0);
        \draw[AC=i8] (i7-1) -- (i8-1);
        \path[name path=C] (i8-1) -- (i8-1 |- 0,3);
        \draw[BC=i9] (i8-1) -- (i9-1);
        \path[name path=C] (i9-1) -- (i9-1 -| 3,0);
        \draw[AC=i10] (i9-1) -- (i10-1);
      \end{scope}
    \end{scope}
  \end{tikzpicture}
\end{center}

Laminares Signal: alternierend wachsend
\begin{center}
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,0) -- (0,-1) (0,1) node[right] {$x_n$}
    |- (3,0) node[right] {$n$};
    \draw plot[ycomb] coordinates {
      (.2,.2)
      (.4,-.4)
      (.6,.6)
      (.8,-.8)
      (1,1)
      (1.2,-1.2)
    };
  \end{tikzpicture}
\end{center}

\section{Seltsame Attraktoren in dissipativen Systemen}

Die Beschreibung dynamischer Systeme erfolgt durch diskrete
Abbildungen oder kontinuierliche Flüsse. Ein kontinuierlicher Fluss
ist definiert durch ein Differentialgleichungs"|system erster Ordnung.
\begin{equation*}
  \dot{\bm x} = \bm F(\bm x) \;,\quad \bm x \in \mathbb R^n
\end{equation*}
Dissipativ heißt, ein beliebiges Volumenelement, das durch eine
Fläche $S$ im Phasenraum $\{\bm x\}$ umschlossen wird, sich im
Laufe der Zeit auf Null zusammen zieht.

\begin{theorem}[Divergenztheorem]
  \begin{equation*}
    \frac{\diff V}{\diff t} = \int_V \diff^n x \left(
      \sum_{i=1}^n \frac{\partial F_i}{\partial x_i} \right)
  \end{equation*}
  für dissipative Systeme gilt
  \begin{equation*}
    \frac{\diff V}{\diff t} < 0.
  \end{equation*}
\end{theorem}

\begin{example}
  Ein Beispiel für einen dissipativen Fluss ist das Lorenzmodell.
  \begin{align*}
    \dot X &= -\sigma X + \sigma Y \\
    \dot Y &= - X Z + r X - Y \\
    \dot Z &= X Y - b Z
  \end{align*}
  Man kann dies in Matrixschreibweise überführen
  \begin{align*}
    \dot{\bm x} &= \bm F(\bm x) \\
    \div \bm F &= - (\sigma + 1 + b) < 0 \\
    \implies V(t) &= V_0 \ee^{-(\sigma +1 + b)t}
  \end{align*}
  Die Lösung für $t \to \infty$ kann ein stabiler Fixpunkt sein, aber
  für bestimmte Parameter (z.B.\ $r = 28$, $\sigma = 10$, $b =
  8/3$), existieren keine stabilen Fixpunkte, die Bahnen sind
  chaotisch.
\end{example}

Wie verträgt sich das chaotische Verhalten mit der Forderung eines für
$t \to \infty$ verschwindenden Volumens? Die Bahn läuft gegen einen
sogenannten \acct[seltsamer Attraktor]{seltsamen Attraktor}.

Zum Begriff:
\begin{description}
\item[Attraktor:] Alle Bahnen in einem beschränkten Gebiet des
  Phasenraumes werden für hinreichend lange Zeiten zum Attraktor
  hingezogen.
\item[Seltsam:] Auf dem Attraktor gibt es eine sensitive Abhängigkeit
  von den Anfangsbedingungen, d.h.\ trotz Volumenkontraktion müssen
  die Längen nicht in alle Richtungen abnehmen. Anfänglich
  infinitesimal benachbarte Punkte entfernen sich exponentiell
  voneinander auf dem Attraktor.
\end{description}

Eigenschaften: Alle bisher gefundenen seltsamen Attraktoren in
dissipativen Systemen haben eine gebrochene Hausdorffdimension, sind
also Fraktale.

Die notwendigen Bedingungen für das Auftreten eines seltsamen
Attraktors sind:
\begin{itemize}
\item Streckung des Volumenelements in mindestens einer Dimension,
\item Abnahme des Volumens, Schrumpfung in den anderen Dimensionen,
\item Beschränkung des Gebiets (erlaubten Phasenraumbereichs),
  Faltungsprozess.
  \begin{center}
    \begin{tikzpicture}[gfx,scale=.9]
      \begin{scope}
        \draw (-1,-1) rectangle (1,1);
        \draw (-1,1) -- ++(45:.5) -- ++(2,0) coordinate(A) -- ++(0,-2) -- ++(-135:.5);
        \draw (A) -- (1,1);
      \end{scope}
      \draw[MidnightBlue,->] (2,0) -- node[above,text width=2.5cm,align=center] {Streckung und Stauchung} (3.5,0);
      \begin{scope}[shift={(5.5,0)},x=1.4cm,y=.5cm]
        \draw (-1,-1) rectangle (1,1);
        \draw (-1,1) -- ++(45:.5) -- ++(2,0) coordinate(A) -- ++(0,-2) -- ++(-135:.5);
        \draw (A) -- (1,1);
      \end{scope}
      \draw[MidnightBlue,->] (8,0) -- node[above,text width=2cm,align=center] {Faltung} (9,0);
      \begin{scope}[shift={(11,0)}]
        \draw (0,1) -- (-1,1) -- (-1,.5) -- (0,.5);
        \draw (0,-1) -- (-1,-1) -- (-1,-.5) -- (0,-.5);
        \draw (0,.5) arc (90:-90:.5);
        \draw (0,1) arc (90:-90:1);
      \end{scope}
    \end{tikzpicture}
  \end{center}
\end{itemize}

\begin{notice}[Erinnerung:]
  Der Bernoulli-Shift $x_{n+1} = 2 x_n \bmod 1$ führt zu einer
  Streckung und Faltung des Einheitsintervalls.

  Für eine Abnahme des Volumens brauchen wir mehr als eine
  Dimension. Dies haben wir bei der Bäcker-Transformation kennen
  gelernt:
  \begin{align*}
    f &: [0,1) \times [0,1) \to [0,1) \times [0,1) \\
    x_{n+1} &= 2 x_n \bmod 1 \\
    y_{n+1} &=
    \begin{cases*}
      a y_n & für $0 \le x_n < \frac{1}{2}$ \\
      \frac12 + a y_n & für $\frac12 < x_n < 1$ \\
    \end{cases*}
  \end{align*}
  Für Punktmengen
  \begin{align*}
    x_{n+1} &= f(x_n)
  \end{align*}
  zeigt sich dissipatives Verhalten, sofern die Determinante der
  Jacobi-Matrix
  \begin{align*}
    \left| \det\left(\frac{\partial f(x_i)}{\partial x_j}\right) \right| < 1
  \end{align*}
  ist. Anschaulich bedeutet dies, dass für jeden Iterationsschritt das
  Volumen schrumpft. Die Bäcker-Transformation ist dissipativ für
  $a<1/2$ (Stauchung in $y$-Richtung).
  \begin{center}
    \begin{tikzpicture}[gfx]
      \draw (-.5,-.5) rectangle (.5,.5);
      \draw[->] (1,0) -- (2,0);
      \draw (2.5,-.25) rectangle (4,.25);
      \draw (3.25,-.25) -- (3.25,.25);
      \draw[->] (4.5,0) -- (5.5,0);
      \draw (6,-.5) rectangle (7,.5);
      \draw[pattern=north east lines] (6,.25) rectangle (7,0);
      \draw[pattern=north east lines] (6,-.25) rectangle (7,-.5);
      \draw[decorate,decoration=brace] (7,-.5) -- node[below] {$a$}(6,-.5);
      \draw[decorate,decoration=brace] (7,.25) -- node[right] {$a$}(7,0);
      \draw[decorate,decoration=brace] (7,-.25) -- node[right] {$a$}(7,-.5);
    \end{tikzpicture}
  \end{center}
  Nach $n$-Iterationen ist das Volumen $V_n = (2 a)^n$, also eine
  Überdeckung durch Flächen $a^{2n}$ mit Kantenlänge $\varepsilon_n =
  a$. Die Anzahl der Überdeckungen ist $N(n) = V_n/a^{2n} =
  (2a)^n/a^{2n} = (2/a)^n$. Damit ergibt sich die Hausdorffdimension
  \begin{align*}
    D &= - \lim_{n\to\infty} \frac{\ln N(n)}{\ln \varepsilon_n} \\
    &= - \lim_{n\to\infty} \frac{n \ln (2/a)}{n \ln a} \\
    &= 1 - \frac{\ln 2}{\ln a} \\
    &= 1 + \frac{\ln 2}{|\ln a|}
  \end{align*}
  also gilt $1 < D < 2$ für $0 < a < 1/2$. Im Limes $n\to\infty$
  ergibt sich ein seltsamer Attraktor.
\end{notice}

\begin{example}
  Ein weiteres Beispiel ist die dissipative Hénon-Abbildung.
  \begin{align*}
    x_{n+1} &= 1 - a x_n^2 + y_n \\
    y_{n+1} &= b x_n
  \end{align*}
  Die Abbildung kontrahiert die Fläche, d.h.\ ist dissipativ für
  $|b|<1$.
\end{example}



%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: