Im Folgenden betrachten wir ein entfaltetes Spektrum
\begin{equation*}
  x_i = \bar{N}(E_i)
\end{equation*}
und suchen die Nächste-Nachbar-Verteilung $P(s)$ für Abstände
\begin{equation*}
  s = x_{i+1} - x_i \, .
\end{equation*}
Wir fragen uns: Findet ein Ereignis bei $x=x_0$ statt, wie groß ist
dann die Wahrscheinlichkeit für
\begin{enumerate}
\item Ein einziges Ereignis (Level) in $[x_0 +s, x_0+s+\diff s]$
\item Kein Ereignis in $[x_0,x_0+s]$
\end{enumerate}
Um dieser Frage nachzugehen führen wir folgende Definitionen ein:
\begin{itemize}
\item $g(s) \diff s$: Wahrscheinlichkeit für Ereignis $[x_0+s ,x_0 +s +\diff s]$ (Homogenität: unabhängig von $x_0$) 
\item $P(s)$: Wahrscheinlichkeitsdichte
  \begin{align}
    P(s) &= g(s) \left( 1 - \int_{0}^{s} P(s') \diff s' \right) \notag \\
    &= g(s) \int_{s}^{\infty} P(s') \diff s' \label{eq:p_stern}
  \end{align}
\end{itemize}
Es ergibt sich somit
\begin{equation*}
  \frac{\diff P}{\diff s} = \frac{\diff g}{\diff s} \underbrace{\int_{s}^{\infty} P(s') \diff s'}_{= P/g \, \textrm{aus \eqref{eq:p_stern}}} - g P = \left( \frac{1}{g} \frac{\diff g}{\diff s} - g\right) P
\end{equation*}
Eine Lösung dieser Differentialgleichung lautet
\begin{equation*}
  P(s) = c g(s) \ee^{- \int_{0}^{s} g(s') \diff s'}
\end{equation*}
und kann für integrable Systeme berechnet werden. 

\paragraph{Integrable Systeme:} Ausgehend von der Torusquantisierung
lässt sich zeigen, dass $g(s)$ nicht von $s$ abhängt, d.h.
\begin{equation*}
  g(s) = \alpha \, .
\end{equation*}
Dies entspricht dem Poisson-Prozess für unabhängige Ereignisse. Für
$P(s)$ erhalten wir somit als Lösung
\begin{equation*}
  P(s) = c \alpha \ee^{-\alpha s}
\end{equation*}
Die Konstanten $c$ und $\alpha$ werden dabei aus der
Normierungsbedingung
\begin{equation*}
  \int_{0}^{\infty} P(s) \diff s = 1
\end{equation*}
und dem mittleren Abstand 
\begin{equation*}
  \bar{s} = \int_{0}^{\infty} s P(s) \diff s = 1
\end{equation*}
gewonnen. Als Endergebnis ergibt sich
\begin{equation*}
  P(s) = \ee^{-s} \, ,
\end{equation*}
d.h. reguläre Systeme sind poissonverteilt. Für kleine Werte von $s$
gilt
\begin{equation*}
  P(s) \approx \bar{c} g(s)
\end{equation*}

\paragraph{Chaotische Systeme}
Für chaotische Systeme verhält sich die Wahrscheinlichkeitsdichte
$P(s)$ anders. Für kleine Werte $s$ erwarten wir
\begin{equation*}
  P(s) \approx s^\beta, \qquad \beta = 
  \begin{cases}
    1 &, \textrm{Systeme mit Zeitumkehrinvarianz} \\
    2 &, \textrm{Systeme ohne Zeitumkehrinvarianz} 
  \end{cases}
\end{equation*}

Für Systeme mit Zeitumkehrinvarianz gilt
\begin{equation*}
  [H,T] = 0
\end{equation*}
mit dem Zeitumkehroperator $T$ (antiunitär). Dieser hat die
Eigenschaften
\begin{align*}
  T^2 &= \mathds{1}, \\
  T \psi(x,t) &= \psi^\ast(x,t) \, .
\end{align*}
Vertauscht der Hamiltonoperator mit dem Zeitumkehroperator, so lässt
sich der Hamiltonoperator als reelle Matrix darstellen.
\begin{proof}
  Sei $\phi_i$ eine beliebige Basis. Wähle eine neue Basis
  \begin{align*}
    \psi_i &= a_i \phi_i + T a_i \psi_i, \qquad a_i \in \mathds{C} \\
    \intertext{mit}
    T \psi_i &= \psi_i \qquad \Braket{\psi_j|\psi_i} = \delta_{ij} 
               \intertext{Für die Matrixelemente gilt dann}
               H_{ij} &= \Braket{\psi_i|H|\psi_j} \\
           &= \Braket{T \psi_i| T H | \psi_j}^\ast \\
           &= \Braket{T \psi_i|H | T \psi_j}^\ast \\
           &= \Braket{\psi_i|H |\psi_j}^\ast \\
           &= H^\ast_{ij}.
  \end{align*}
\end{proof}
\section{Theorie der Zufallsmatrizen (Random Matrix Theory)}

\begin{theorem}[Wignersche Vermutung]
  Bei chaotischen Quantensystemen (Systeme ohne Symmetrien,
  bzw.\ gute Quantenzahlen, außer eventuell Zeitumkehrinvarianz) wird
  die Nächste-Nachbar-Verteilung der Niveauabstände beschrieben durch
  ein Ensemble von Zufallsmatrizen, wobei die Matrixelemente
  entsprechend bestimmter Verteilungsfunktionen zufällig gewählt
  werden.
\end{theorem}

\subsection{Systeme mit Zeitumkehrinvarianz $[H,T] =0$}

Um Systeme mit Zeitumkehrinvarianz zu betrachten benötigen wir das
\acct{Gaußsche orthogonale Ensemble} (GOE). Im Folgenden betrachten wir den
Hamiltonoperator in Form einer reell symmetrischen
$(2 \times 2)$-Matrix
\begin{equation*}
  H =
  \begin{pmatrix}
    H_{11} & H_{12} \\
    H_{12} & H_{22}
  \end{pmatrix}
\end{equation*}
mit Matrixelementen $H_{ij}$, welche Zufallszahlen gemäß des
Wahrscheinlichkeitsmaßes $\mathcal{P}(H)$ sind. Dabei haben wir einige
Forderungen an dieses Wahrscheinlichkeitsmaß:
\begin{enumerate}
\item $\iiint \mathcal{P}(H) \diff H_{11} \diff H_{12} \diff H_{22} = 1$
\item $\mathcal{P}(H)$ soll invariant unter orthogonalen
  Transformationen $O$ der Form
  \begin{equation*}
    O =
    \begin{pmatrix}
      \cos \vartheta & - \sin \vartheta \\
      \sin \vartheta & \cos \vartheta
    \end{pmatrix}
  \end{equation*}
sein.
\item Die einzelnen Wahrscheinlichkeitsdichten
  $\mathcal{P}_{ij}(H_{ij})$ sollen statistisch unabhängig sein, d.h.
  \begin{equation*}
    \mathcal{P}(H) = \mathcal{P}_{11}(H_{11}) \mathcal{P}_{12}(H_{12}) \mathcal{P}_{22}(H_{22})
  \end{equation*}
\end{enumerate}
Wir betrachten nun die zuvor erwähnte orthogonale Transformation für
kleine Winkel $\vartheta$
\begin{equation*}
  O =
  \begin{pmatrix}
    1 & - \vartheta \\
    \vartheta & 1 \\
  \end{pmatrix} \, .
\end{equation*}
Unter dieser infinitesimalen Transformation soll $\mathcal{P}(H)$
invariant sein, es folgt daher
\begin{equation*}
  H' = O H O^T
\end{equation*}
und für die einzelnen Elemente
\begin{align*}
  H'_{11} &= H_{11} - 2 \vartheta H_{12} \equiv H_{11} + \Delta H_{11}\\
  H'_{22} &= H_{11} + 2 \vartheta H_{12}  \equiv H_{22} + \Delta H_{22}\\
  H'_{12} &= H_{12} + \vartheta (H_{11} - H_{22})  \equiv H_{12} + \Delta H_{12} \\
\end{align*}
Die Änderung des Wahrscheinlichkeitsmaßes $\Delta \mathcal{P}(H)$
\begin{equation*}
  \mathcal{P}(H') = \mathcal{P}(H) +  \Delta \mathcal{P}(H)
\end{equation*}
muss nach Forderung 2 verschwinden.
\begin{align*}
  \Delta \mathcal{P}(H) = &\left( \frac{\diff \mathcal{P}_{11}}{\diff H_{11}} \Delta H_{11}\right) \mathcal{P}_{22} \mathcal{P}_{12} + \mathcal{P}_{11} \left( \frac{\diff \mathcal{P}_{22}}{\diff H_{22}} \Delta H_{22}\right) \mathcal{P}_{12} \\
  &+ \mathcal{P}_{11} \mathcal{P}_{22} \left( \frac{\diff \mathcal{P}_{12}}{\diff H_{12}} \Delta H_{12}  \right) 
\intertext{mit}
\frac{\diff \mathcal{P}_{ij}}{\diff H_{ij}} =  & \mathcal{P}_{ij} \frac{\diff \ln \mathcal{P}_{ij}}{\diff H_{ij}}
\intertext{folgt}
\Delta \mathcal{P}(H) = & - \vartheta \underbrace{\left( 2H_{12} \frac{\diff \ln \mathcal{P}_{11}}{\diff H_{11}} - 2 H_{12} \frac{\diff \ln \mathcal{P}_{22}}{\diff H_{22}}
 - (H_{11} - H_{22})  \frac{\diff \ln \mathcal{P}_{12}}{\diff H_{12}} \right)}_{=0} \underbrace{\mathcal{P}_{11} \mathcal{P}_{22} \mathcal{P}_{12}}_{= \mathcal{P}}
\end{align*}
Aus der Bedingung, dass die Klammer verschwinden muss erhalten wir
\begin{equation*}
  \frac{1}{H_{11} - H_{22}} \left(  \frac{\diff \ln \mathcal{P}_{11}}{\diff H_{11}} -  \frac{\diff \ln \mathcal{P}_{22}}{\diff H_{22}}\right)  - \frac{1}{2 H_{12}}  \frac{\diff \ln \mathcal{P}_{12}}{\diff H_{12}} = 0 
\end{equation*}
Eine Lösung dieser Gleichung wird beschrieben durch die Gaußfunktion
\begin{equation*}
  \mathcal{P}(H) = c \ee^{-A(H^2_{11} + H^2_{22} + 2 H^2_{12})} = c \ee^{-A \tr{H^2}} \, ,
\end{equation*}
wobei $c$ und $A$ Konstanten sind, welche durch die Normierung und den
Mittelwert festgelegt werden. Für ein Zwei-Niveau-System ergeben sich
zwei Energien $E_+$ und $E_-$ und somit einen Energieabstand
\begin{equation*}
  \Delta E = E_+ - E_- \, .
\end{equation*}
Da wir ein $(2 \times 2)$-Matrixmodell betrachten, lassen sich die
Eigenenergien allgemein formulieren
\begin{align*}
  E_\pm = \frac{1}{2}  (H_{11} + H_{22}) \pm \frac{1}{2} \sqrt{(H_{11} - H_{12})^2 + 4 H^2_{12}} 
\end{align*}
Für beliebigen Winkel $\vartheta$ können wir immer eine Transformation
finden, sodass für den Hamiltonoperator gilt
\begin{align*}
  H &=  O^T
  \begin{pmatrix}
    E_+ & 0 \\
    0   & E_- 
  \end{pmatrix}
  O \\
  H_{11} &= E_+ \cos^2\vartheta + E_- \sin^2 \vartheta \\
  H_{22} &= E_+ \sin^2 \vartheta + E_- \cos^2\vartheta \\
  H_{12} &= (E_+ -E_-) \cos \vartheta \sin\vartheta
\end{align*}
Dabei muss gelten
\begin{align*}
  \mathcal{P}(H) \diff H_{11} \diff H_{22} \diff H_{12} &= \mathcal{P}(E_+,E_-,\vartheta) \diff E_+ \diff E_- \diff \vartheta \\
  \mathcal{P}(E_+,E_-,\vartheta) &= \mathcal{P}(H) \left| \det{\frac{\partial(H_{11},H_{22},H_{12})}{\partial (E_+,E_-,\vartheta)}}\right| \\
  &= \mathcal{P}(H) |E_+ - E_-| \\
  &= c \ee^{-A\tr{H^2}} |E_+ - E_-| \\
  &= c \ee^{-A(E^2_+ + E^2_-)} |E_+ - E_-|\, .
\intertext{Mit}
s &= E_+ - E_- \, , \qquad E_0 = \frac{1}{2} (E_+ + E_-) 
\intertext{folgt}
E_\pm &= E_0 \pm \frac{s}{2}
\intertext{und somit}
\mathcal{P}(s,E_0) &= c \,  s  \, \ee^{-A \left(\frac{1}{2} s^2 + 2 E^2_0\right)} \underbrace{\bigg|\det{\frac{\partial (E_+,E_-)}{\partial (s,E_0)}}\bigg|}_{=1}.
\end{align*}
Mittelung über $E_0$ führt zu der Verteilung
\begin{align*}
  \mathcal{P}(s) &= \int_{-\infty}^{\infty} \mathcal{P}(s,E_0) \diff E_0 = s \ee^{-\frac{A}{2} s^2} \, \underbrace{c \, \int_{-\infty}^{\infty} \ee^{-2 E^2_0} \diff E_0}_{=\tilde{c}} \\
  &= \tilde{c} s \ee^{-\frac{A}{2} s^2}
\end{align*}
Die Konstanten $\tilde{c}$ und $A$ werden nun noch mittels der
Normierung
\begin{equation*}
  \int_{0}^{\infty} \mathcal{P}(s) \diff s = 1
\end{equation*}
und dem Erwartungswert
\begin{equation*}
  \int_{0}^{\infty} s \mathcal{P}(s) \diff s =1 
\end{equation*}
bestimmt. Letzten Endes ergibt sich die sogenannte
\acct{Wignerverteilung} für ein Gaußsches orthogonales Ensemble (GOE)
\begin{equation*}
  \boxed{
    \mathcal{P}(s) = \frac{\pi}{2} s \, \ee^{-\frac{\pi}{4} s^2}
  }
\end{equation*}
%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: