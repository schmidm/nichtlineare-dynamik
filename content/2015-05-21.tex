
\subsection{EBK-Quantisierung (Einstein, Brioullin, Keller)}
Die \acct{EBK-Quantisierung} besteht darin, die Winkelvariablen wie
folgt anzusetzen
\begin{align*} 
  I_k &= \hbar \left( n_k + \frac{\alpha_k}{4} \right), \quad n_k \in \mathbb{N}_0 \\
  E(n_1,\ldots,n_k) &= H\left(\bm{I} = \hbar \left(\bm{n} + \frac{\bm{\alpha}}{4}  \right)\right),
\end{align*}
wobei $k \in \{1,\ldots,N \}$. Es stellt sich noch die Frage, was im
klassischen chaotischen System bleibt. Es zeigt sich, dass periodische
Bahnen existieren (eventuell instabil). Jedoch gilt es zu beachten,
dass die unabhängigen Wege $C_k$ nicht den periodischen Bahnen
entsprechen. Zudem existieren auf den quantisierten Tori in der Regel
keine periodischen Bahnen, da die Frequenzverhältnisse immer
irrational sind. Es existieren jedoch quasi-periodische
Bahnen. Dennoch gibt es eine Verbindung zwischen semiklassischer (hier
\acct{EKB-Quantisierung}) und periodischen Bahnen klassischer
Systeme. Diese beschreibt die \acct{periodic orbit theory}.

Wir betrachten ein integrables System mit $N=2$. Die Torusquantisierung
liefert das Eigenwertspektrum $E(\bm{n})$. Die Zustandsdichte ist
durch
\begin{align}
  d(E) &= \sum\limits_{n_1=0}^{\infty}\sum\limits_{n_2=0}^{\infty} \delta(E-E(n_1,n_2)) \label{eq:27.5.15-1}
\end{align}
gegeben. Wir wollen nun eine semiklassische Form finden. Dazu
wenden wir die \acct{Poissonsche Summenformel}
\begin{align*}
  \sum\limits_{n=0}^{\infty} f(n) &= \sum\limits_{M=-\infty}^{\infty} \int\limits_{0}^{\infty} f(n) \ee^{2 \pi \ii M n} \diff n + \frac{1}{2} f(0)
\end{align*}
auf Gleichung~\eqref{eq:27.5.15-1} an. Somit erhalten wir
\begin{multline*}
  d(E) = \underbrace{\sum\limits_{M_1,M_2} \int\diff n_1 \int \diff n_2 \delta(E-E(n_1,n_2)) \ee^{2 \pi \ii (M_1 n_1 + M_2 n_2) }}_{=d^{(2)}(E)} \\
  + \underbrace{\frac{1}{2} \sum\limits_M \int \diff n_1 \delta(E-E(n_1,0)) \ee^{2
\pi \ii M n_1} 
  + \frac{1}{2} \sum\limits_M \int \diff n_2 \delta(E-E(0,n_2)) \ee^{2
\pi \ii M n_2}}_{\text{semikl. Korrekturen in $\hbar$}} \\
+ \frac{1}{4} \delta(E-E(0,0)).
\end{multline*}
Hierbei ist der letzte Term uninteressant für Spektren mit $E>0$. Die
beiden mittleren Terme führen zu semiklassischen Korrekturen höherer
Ordnung in $\hbar$. Den ersten Term nennen wir $d^{(2)}(E)$. Mit
\begin{align*}
  n_k &= \frac{1}{\hbar} I_k - \frac{\alpha_k}{4}
\end{align*}
folgt
\begin{align*}
  d^{(2)}(E) &= \frac{1}{\hbar^2} \sum\limits_{M_1,M_2} \ee^{- \ii \frac{\pi}{2}(M_1 \alpha_1 + M_2 \alpha_2)} \int\limits_{\hbar \alpha_1/4}^{\infty} \diff I_1 \int\limits_{\hbar \alpha_2 / 4}^{\infty} \diff I_2 \delta (E-H(I_1,I_2)) \ee^{2 \pi \ii (M_1I_1 + M_2 I_2)/\hbar}.
\end{align*}
Betrachte den Term für $M_1 = M_2 = 0$, wobei wir eine zusätzliche
Integration über $\phi_1$ und $\phi_2$ einfügen und die unteren
Integrationsgrenzen auf Null setzen. Eine Berücksichtigung der
korrekten Grenzen führt wieder lediglich auf Korrekturen höherer
Ordnung in $\hbar$.
\begin{align*}
  \tilde{d}^{(2)}(E) &=  \frac{1}{(2\pi\hbar)^2} \underbrace{ \int\limits_{0}^{2\pi} \diff \phi_1 \int\limits_{0}^{2\pi} \diff \phi_2 \int\limits_{0}^{\infty} \diff I_1 \int\limits_{0}^{\infty} \diff I_2}_{\text{Gesamter Phasenraum}} \delta(E-H(I_1,I_2)) \\
  &= \frac{1}{(2\pi\hbar)^2} \int \diff\bm{p} \int \diff \bm{q} \delta(E-H(\bm{p},\bm{q})).
\end{align*}
Dies ist der \acct{Thomas-Fermi-Term} bzw.\ die mittlere
Zustandsdichte. Man erhält ihn, indem man die Summe über
Quantenzustände durch Integrale über den Phasenraum geteilt durch
$\hbar^N$ ersetzt. Betrachten wir nun den oszillierenden Anteil mit
($M_1 \neq 0 \neq M_2$). Mit
\begin{align*}
  \delta(x) &= \frac{1}{2\pi\hbar} \int\limits_{-\infty}^{\infty} \ee^{\ii \frac{\tau x}{\hbar}} \diff \tau 
\intertext{folgt}
  \delta d(E) &= \frac{1}{2\pi\hbar^3} \sum_{M_1 \neq 0 \neq M_2} \ee^{- \ii \frac{\pi}{2} \bm{M} \bm{\alpha}} \int\diff I_1 \int \diff I_2 \ee^{\ii \phi(\bm{I})} \\
  \phi(\bm{I}) &= \frac{1}{\hbar} \left(2 \pi \bm{M} \cdot \bm{I} + \tau(E-H(\bm{I})) \right).
\end{align*}
Mit der stationären Phase $\bm{\nabla} \phi(\bm{I}) = 0$ finden wir
\begin{align}
  2 \pi M_i &= \omega_i \tau = \tau \omega_i(I_1,I_2), \label{eq:21.5.15-6}
\end{align}
wobei
\begin{align*}
  \omega_i &= \frac{\partial H}{\partial I_i}
\end{align*}
am Sattelpunkt. Damit zeigt sich
\begin{align*}
  \frac{M_1}{M_2} &= \frac{\omega_1}{\omega_2} \in \mathbb{Q},
\end{align*}
d.h.\ es handelt sich um resonante Tori. Somit tragen nur Tori mit
kommensurablen Frequenzen (periodischen Bahnen) zu $d(E)$ bei. Die
$M_i$ sind hierbei Umlaufzeiten (und keine Quantenzahlen). Somit
ergibt sich
\begin{align*}
  \delta d(E) &= \frac{1}{4 \hbar^2} \sum_{\bm{M}} \ee^{- \ii \frac{\pi}{2} \bm{M}\cdot\bm{\alpha} - \ii \frac{\pi}{4} \sigma_{\bm{M}}\neq 0} = \int\limits_{-\infty}^{\infty} \diff \tau \frac{1}{\tau |k|^{1/2}} \ee^{\frac{\tau}{\hbar}(2 \pi \bm{M}\cdot \bm{I}_M + \tau (E-H(\bm{I}_M)))}.
\end{align*}
$I_{\bm{M}}$ entspricht hier der Wirkung der resonanten Tori (Lösung
von Gleichung~\eqref{eq:21.5.15-6}). Weiter haben wir
\begin{align*}
  k &= \det\left( \frac{\partial^2 H}{\partial I_1 \partial I_2} \right), \\
  \sigma_{\bm{M}} &= \sum\limits_{i=1}^{2} \sign(\lambda_i),
\end{align*}
wobei $\lambda_i$ die Eigenwerte der Hesse-Matrix sind.

\begin{notice}[Wichtig:]
  Die Summe über die quantisierten EBK-Tori ist
  \begin{align*}
    d(E) &= \sum\limits_{\bm{n}} \delta(E-E(\bm{n})).
  \end{align*}
  Verwenden wir die Poissonsche Summenformel und die stationäre Phase
  erhalten wir
  \begin{align*}
    d(E) &= \tilde{d}^{(2)}(E) + \delta d(E).
  \end{align*}
  Der erste Term entspricht gerade der mittleren Zustandsdichte im
  Phasenraum (TF-Term). Der zweite Term beschreibt die periodischen
  Bahnen auf den resonanten Tori, wobei die Summe
  $\sum_{\bm{M} \in \mathbb{Z}^N}$ mit $\bm{M} \neq 0$ auftritt.
\end{notice}
Ausführen der (nicht-trivialen) $\tau$-Integration (mit
Stationärer-Phasen-Approximation) liefert die \acct{Berry-Tabor-Formel
  für integrable Systeme} (1976). Für $N=2$ ist
\begin{align*}
  \boxed{d(E) = \tilde{d}^{(2)}(E) + \sum_{\bm{M}\neq 0} \frac{T_{\bm{M}}}{\pi \sqrt{ \hbar^3 M_2^3 |d_E''| }} \cos\left( \frac{S_{\bm{M}}}{\hbar} - \frac{\pi}{2} \sigma_{\bm{M}} - \frac{\pi}{4} \right)}
\end{align*}
mit
\begin{align*}
  H(I_1,I_2=d_E(I_1)) = E.
\end{align*}
$S_{\bm{M}}$ entspricht hierbei der Wirkung und $T_{\bm{M}}$ der
Umlaufdauern der periodischen Bahnen (auch für vielfache Umläufe).

\begin{example}[2D-Rechteckbillard:]
  Die Hamiltonfunktion des Systems lautet
  \begin{align*}
    H &= \frac{1}{2m} \left( p_x^2 + p_y^2\right) \\
    I_j &= \frac{1}{2\pi} \oint p_j \diff x_j = \frac{a_j}{\pi}\cdot p_j \\
    \implies H &= \frac{\pi^2}{2m} \left( \frac{I_1^2}{a_1^2} + \frac{I_2^2}{a_2^2}  \right).
  \end{align*}
  Der Maslov-Index (unendlich hohe Potentialwände) ist
  $\alpha_1 = \alpha_2 = 4$. Stellen wir uns das Rechteckbillard vor
  mit den Kantenlängen $a_1$ und $a_2$ und betrachten einen Umlauf mit
  $a_1=\const$ oder $a_2= \const$. Aufgrund der unendlich hohen
  Potentialwände ist an jeder Wand ein Phasensprung von $\pi$ nötig,
  damit an diesen Stellen die Wellenfunktion verschwindet.  Wird
  beachtet, dass $\alpha = [\sigma]/2$ gilt, sowie
  $\exp(\ii \pi \sigma/4)$ in der Wellenfunktion auftritt, dann folgt
  $[\sigma] = 2 \cdot 4$ und somit $\alpha = 4$. 

  Die exakte Lösung der Schrödingergleichung mit Randbedingungen ist
  \begin{align*}
    E &= \frac{\hbar^2 \pi^2}{2m} \left( \left(  \frac{n_1}{a_1} \right)^2 + \left( \frac{n_2}{a_2}  \right)^2  \right), \quad n_i \in \mathbb{N}.
  \end{align*}
  Für die $I$'s finden wir
  \begin{align*}
    I_j &= \hbar \left( n_j + \frac{\alpha_j}{4}  \right),  \quad n_j \in \mathbb{N}_0 \\
    &= \hbar \tilde{n}_j,  \quad\tilde{n}_j \in \mathbb{N}.
  \end{align*}
  Wir sehen, dass die semiklassische Lösung mit der
  quantenmechanischen übereinstimmt. Für die Hesse-Matrix gilt
  \begin{align*}
    \frac{\partial^2 H}{\partial I_i \partial I_j} &= \frac{\pi^2}{m}
                                                     \begin{pmatrix}
                                                       a_1^{-2} & 0 \\
                                                       0 & a_2^{-2}
                                                     \end{pmatrix}.
  \end{align*}
  Somit können wir $k$ zu 
  \begin{align*}
    k &= \frac{\pi^4}{(ma_1a_2)^2}
  \end{align*}
  berechnen, sowie $\sigma_{\bm{M}} = 2$. Die stationäre Phase ergibt
  \begin{align*}
    2 \pi \bm{M} &= \tau \bm{\omega} = \tau \nabla_{I}H \\
                 &= \frac{\pi^2 \tau }{m} \begin{pmatrix}
                   I_1 a_1^{-2} \\
                   I_2 a_2^{-2}
                 \end{pmatrix} \\
    \implies I_j & = \frac{2m}{\pi \tau} a_j^2 M_j \\
    \implies 2 \pi \bm{M} \cdot \bm{I} &= \frac{4 m}{\tau}(a_1^2 M_1^2 + a_2^2M_2^2) \\
    \implies - H \tau &= - \frac{2m}{\tau} (a_1^2M_1^2 + a_2^2M_2^2)
  \end{align*}
  Einsetzen (mit TF-Term) ergibt
  \begin{align*}
    d^{(2)}(E) &= \frac{ma_1a_2}{4 \pi^2 \hbar^2}\underbrace{\ee^{-\ii\frac{\pi}{2}}}_{=-\ii} \sum_{\bm{M}} \int\limits_{-\infty}^{\infty} \diff \tau \frac{1}{\tau} \ee^{\frac{\ii}{\hbar}\left(E \tau + \frac{c}{\tau}\right)}
  \end{align*}
  mit $c = 2 m (a_1^2M_1^2+a_2^2M_2^2)$. Wir verwenden die stationäre
  Phase mit $d(\tau) = 1 / \tau$ sowie $f(\tau) = E \tau + c/\tau$ und
  erhalten
  \begin{align*}
    \tau_{1,2} &= \pm \sqrt{\frac{c}{E}},\\
    \frac{d(\tau_{1,2})}{\sqrt{|f''(\tau_{1,2})|}} &= \pm \frac{1}{\sqrt{2} [cE]^{1/4}}
  \end{align*}
  und 
  \begin{align*}
    \int\limits_{-\infty}^{\infty} \diff \tau \frac{1}{\tau} \ee^{\frac{\ii}{\hbar}\left(E \tau + \frac{c}{\tau} \right)} &\approx  \frac{\sqrt{2\pi\hbar}}{\sqrt{2} [Ec]^{1/4}} 2 \ii \sin\left(\frac{2 \sqrt{cE}}{\hbar} + \frac{\pi}{4} \right) \\
    &= \frac{2 \ii \sqrt{\pi \hbar}}{(Ec)^{1/4}} \cos\left( \frac{2\sqrt{cE}}{\hbar}- \frac{\pi}{4} \right).
  \end{align*}
  Die stationäre Phase gilt nicht für $c=0$ (TF-Term $M_1=M_2=0$)
  \begin{align*}
    \int\limits_{-\infty}^{\infty} \diff \tau \frac{1}{\tau} \ee^{\ii E \tau \hbar} = 2 \pi \ii.
  \end{align*}
  Verwenden wir
  \begin{align*}
    2 \frac{\sqrt{cE}}{\hbar} &= \frac{\sqrt{2mE}}{\hbar} L_{M_1,M_2} = \frac{1}{\hbar} S_{M_1,M_2}(E),
  \end{align*}
  wobei $L_{M_1,M_2}$ die Länge und $S_{M_1,M_2}$ die Wirkung der
  periodischen Bahnen auf dem rationalen Torus mit Umlaufzahlen $M_1$
  und $M_2$ sind, finden wir für die Zustandsdichte
  \begin{align*}
    d^{(2)}(E) &= \underbrace{\frac{ma_1a_2}{2 \pi \hbar^2}}_{=\tilde{d}^{(2)}(E)} + \underbrace{   \frac{ma_1a_2}{2 \pi^{3/2} \hbar^2} \sum_{\bm{M}\neq 0} \sqrt{\frac{2 \hbar }{S_{M_1,M_2}(E)}} \cos\left(\frac{1}{\hbar} S_{M_1,M_2}(E) - \frac{\pi}{4} \right) }_{=\delta d(E)}.
  \end{align*}
  Der erste Term $\tilde{d}^{(2)}(E)$ entspricht dem TF-Term, der
  zweite Term hingegen den oszillierenden
  Periodischen-Orbit-Beiträgen. Die damit gefundene Darstellung von
  $d^{(2)}(E)$ wird als \acct{Berry-Tabor-Formel} für das
  Rechteckbillard bezeichnet. Die exakte Lösung, d.h.\ ohne stationäre
  Phase für $\tau$ ist
  \begin{align*}
    d^{(2)}(E) &= \frac{ma_1a_2}{2 \pi \hbar^2} \sum_{\bm{M}} \mathcal{J}_0\left( \frac{1}{\hbar}  S_{M_1,M_2}(E)\right), 
  \end{align*}
  wobei $\mathcal{J}_0$ eine Besselfunktion ist.
\end{example}

\begin{notice}
  Für die isolierten Bahnen in chaotischen Systemen wird die
  Gutzwiller-Spurformel angewendet. Im Gegensatz dazu werden in der
  EBK-Quantisierung Bahnscharen behandelt.
\end{notice}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: