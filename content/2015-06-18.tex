\subsubsection{Weg zur semiklassischen Zustandsdichte (Gutzwiller-Spurformel)} 
Die letzten Abschnitte hatten das Ziel eine semiklassische Näherung
der Zustandsdichte $d(E)$ zu erhalten, welche ausschließlich von
Parametern der klassischen Bahnen abhängt. Die Vorgehensweise hierbei
soll im Folgenden nocheinmal skizziert werden.
\begin{enumerate}
\item Der Ausgangspunkt war der exakte quantenmechanische Propagator,
  der aus dem Feynmanschen Pfadintegral folgte. In diesen Propagator
  gehen alle Wege von $x'$ nach $x$ ein, auch diese, die es klassisch
  nicht geben würde.
\item Anwendung der stationären Phasenapproximation auf die
  Feynmanschen Pfadintegrale. Das Resultat war die van Vleck-Formel,
  in der alle klassischen Wege von $x'$ nach $x$ eingehen, wobei die
  Energie variabel bleibt.
\item Berechnung der semiklassischen Greenschen Funktion
  $G^+_{E,\mathrm{SCL}}$ über eine Zeitintegration mittels stationärer
  Phase. In die semiklassische Greensche Funktion gehen alle klassiche
  Wege von $x'$ nach $x$ ein, diesmal jedoch bei fester Energie $E$.
\item Spurbildung der semiklassischen Greenschen Funktion
  \begin{equation*}
    \tr{G^+_{E,\mathrm{SCL}}} = \int \diff^N x G^+_{E,\mathrm{SCL}}(x,x) \, , \qquad x =x' \, .
  \end{equation*}
  Im Ortsraum geschlossene Wege von $x'$ nach $x$ (Impulse $p'$ und
  $p$ beliebig). Man erhält zwei Arten von Beiträgen der Greenschen
  Funktion.
\begin{enumerate}
\item $x \to x'$ Weglänge geht gegen Null. Es ergibt sich der
  Thomas-Fermi-Term:
  \begin{equation*}
    \bar{d}(E) = \frac{1}{(2\pi \hbar)^N} \int \diff^N x \diff^N p \delta(E-H(x,p)) 
  \end{equation*}
\item Beiträge von Bahnen (Weglänge größer Null), die am Ort $x$
  starten und dorthin zurückkehren.
\end{enumerate}
Im Folgenden soll der Thomas-Fermi-Term etwas genauer untersucht
werden. Dazu betrachten wir den Hamiltonoperator
\begin{equation*}
  H(x,p) = \frac{p^2}{2m} + V(x)
\end{equation*}
und die Greensche Funktion für den Fall $x \approx x'$
\begin{equation*}
  G(x',x,E) = - \frac{m}{2\pi \hbar^2} \frac{\ee^{\ii k r}}{r} \approx - \frac{m}{2 \pi \hbar^2} \left( \frac{1}{r} + \ii k \right)
\end{equation*}
wobei im letzten Schritt eine Taylorentwicklung für kleine $r$
durchgeführt wurde. Mit der Energie
\begin{equation*}
  E= \frac{\hbar^2}{2m} k^2 + V(x)
\end{equation*}
und $r = |x- x'|$ folgt die mittlere Zustandsdichte
\begin{equation*}
  \bar{d}(E) = - \frac{1}{\pi} \tr\left[{\textrm{Im}(G(x',x,E))}\right] = \frac{m}{2\pi^2\hbar^2} \int \diff^3 x \, \sqrt{2m(E-V(x))} 
\end{equation*}
Die mittlere Anzahl an Zuständen lautet somit
\begin{align*}
  \bar{N}(E) &= \int_{-\infty}^{E} \bar{d}(E') \diff E' = \int_{-\infty}^{\infty} \bar{d}(E') \Theta(E-E') \diff E' \\
  &= \frac{m}{2\pi^2\hbar^2} \int \diff^3 x \int_{-\infty}^{\infty} k \, \Theta(E-H(x,\hbar k)) \diff E' \\
  &= \frac{m}{2\pi^2\hbar^2} \int \diff^3 x \int_{-\infty}^{\infty} \frac{\hbar^2}{m} k^2 \diff k \,  \Theta(E-H(x,\hbar k)) \\
  &= \frac{1}{8 \pi^3} \int \diff^3 x \int \diff^3 k \, \Theta(E-H(x,\hbar k)) \\
  &= \frac{1}{(2 \pi \hbar)^3} \int \diff^3 x \int \diff^3 p \, \Theta(E - H(x,p)) \\
  \leadsto \bar{d}(E) &= \frac{\diff \bar{N}(E)}{\diff E} = \frac{1}{(2 \pi \hbar)^3}  \int \diff^3 x \int \diff^3 p \, \delta(E - H(x,p))
\end{align*}
\item Berechnung der Spur von $G^+_{E,\mathrm{SCL}}$ mittels
  stationärer Phasenapproximation
  \begin{equation*}
    \tr{G^+_{E,\mathrm{SCL}}} = \frac{2\pi}{(2\pi \hbar \ii)^{(N+1)/2}} \int \diff^N x \, \sum_{\textrm{k. Tj,}} \sqrt{|D|} \ee^{-\ii S(x,x,E) - \ii \mu \pi/2}
  \end{equation*}
  Stationäre Phase:
  \begin{align*}
    \left( \frac{\partial S(x,x,E)}{\partial x}\right)_{x_0} &=    \bigg( \underbrace{\frac{\partial S(x,x',E)}{\partial x'}}_{=-p'} + \underbrace{\frac{\partial S(x,x',E)}{\partial x}}_{=p}  \bigg)_{x = x'=x_0} \\
    &= -p' + p =0
  \end{align*}
  Hier variieren wir beide Argumente von $S(x,x',E)$ am Start- und
  Endpunkt der Trajektorie. Somit entspricht $-p'$ den Anfangs- und
  $p$ den Endimpuls. Somit tragen in der Summe
  $\sum_{\textrm{k. Tj,}}$ nur diejenigen klassischen Trajektorien
  bei, die die Bedingung $x=x'$ und $p=p'$ erfüllen, also erstreckt
  sich die Summe über alle periodischen Bahnen.
\item Ausführung der Integration (in der Umgebung der periodischen Bahnen). Dazu wählt man lokale Koordinaten in der Umgebung einer periodischen Bahn
  \begin{align*}
    x = &(q, x_{\perp,1},\ldots ,x_{\perp,N-1}) = (q, x_\perp) \\
    \diff^N x \to & \diff q \diff^{N-1} x_\perp
  \end{align*}
  Die Integration wird also über die parallel und senkrecht Komponenten der Bahn aufgespalten. In diesen Koordinaten gilt für die Amplitude
  \begin{align*}
    D &= \det{
      \begin{pmatrix}
       \frac{\partial^2 S}{\partial x \partial x} &        \frac{\partial^2 S}{\partial x' \partial E} \\
       \frac{\partial^2 S}{\partial E \partial x} &       \frac{\partial^2 S}{\partial E \partial E} 
      \end{pmatrix}
    } \\
    &= - \frac{\partial^2 S}{\partial E \partial q} \frac{\partial^2 S}{\partial E \partial q'} \det{ \frac{\partial^2 S}{\partial x'_\perp \partial x'_\perp}}
    \intertext{mit} 
    \frac{\partial S}{\partial x} &= p \qquad \frac{\partial S}{\partial x'} = - p' \qquad \frac{\partial S}{\partial E} = t \\ 
    \intertext{folgt}
    D &= (-1)^N \frac{1}{\dot{q} \dot{q}'} \det{\frac{\partial p'_\perp}{\partial x_\perp}}
  \end{align*}
  Die Entwicklung der Wirkung in der Umgebung der periodischen Bahnen
  führt auf
  \begin{equation*}
    S(x,x,E) = \underbrace{\oint  p \diff x }_{S_{\mathrm{PO}}(E)} + \frac{1}{2} \sum_{i,j=1}^{N-1} W_{ij}(q) x_{\perp,i} x_{\perp,j}
  \end{equation*}
  mit
  \begin{equation*}
    W_{ij}(q) = \left( \frac{\partial^2 S}{\partial x_\perp \partial x_\perp} + \frac{\partial^2 S}{\partial x_\perp \partial x'_\perp} + \frac{\partial^2 S}{\partial x'_\perp \partial x_\perp} + \frac{\partial^2 S}{\partial x'_\perp \partial x'_\perp} \right)_{x_\perp = x'_\perp=0}
  \end{equation*}
  Setzen wir die gefundene Wirkung in $\tr{G^+_{E,\mathrm{SCL}}}$ ein,
  so ergeben sich Fresnel-Integrale für jede Ortskoordinate senkrecht
  zur periodischen Bahn.
  \begin{equation*}
    \tr{G^+_{E,\mathrm{SCL}}} = \frac{1}{\hbar} \sum_{\mathrm{PO}} \ee^{\ii/\hbar S_{\mathrm{PO}}(E) - \ii \frac{\pi}{2} (\mu+\nu)} \int \diff q \, \sqrt{|D(q)|} \frac{1}{\sqrt{|\det{W(q)}|}} 
  \end{equation*}
Den Parameter $\nu$ erhält man dabei aus den Eigenwerten der Matrix $W(q)$ und der Summenindex $\mathrm{PO}$ ist so zu verstehen, dass über alle periodischen Bahnen summiert wird. Es lässt sich zusätzlich zeigen (mathematisch aufwendig), dass
\begin{equation*}
  \left| \frac{\det{W(q)}}{D(q)} \right| = \dot{q}^2 \left|\det{M_{\mathrm{PO}} - \mathds{1}}\right|
\end{equation*}
gilt. Dabei ist $M_{\mathrm{PO}}$ die Monodromiematrix der perdiodischen Bahn. Damit ergibt sich schließlich
\begin{align*}
  \tr{G^+_{E,\mathrm{SCL}}} &= \frac{1}{\hbar} \sum_{\mathrm{PO}} \ee^{\ii/\hbar S_{\mathrm{PO}}(E) - \ii \frac{\pi}{2} (\mu+\nu)} \frac{1}{\left|\det{M_{\mathrm{PO}} - \mathds{1}}\right|} \int \frac{1}{\dot{q}} \diff q \\
\end{align*}
unter Ausnutzung von $\mu + \nu = \sigma_{\mathrm{PO}}$ und 
\[ \int \frac{1}{\dot{q}} \diff q  = \int_{\mathrm{PPO}} \diff t = T_{\mathrm{PPO}}\] 
erhalten wir das Endergebnis, nämlich die \acct{Gutzwiller-Spurformel} (M.Gutzwiller 1970)
\begin{equation*}
  \boxed{
    d(E) = \bar{d}(E) + \frac{1}{\pi \hbar} \sum_{\mathrm{PO}} \frac{T_{\mathrm{PPO}}}{\sqrt{ \left|\det{M_{\mathrm{PO}} - \mathds{1}}\right|}} \cos \left(\frac{S_{\mathrm{PO}}}{\hbar} - \frac{\pi}{2} \sigma_{\mathrm{PO}} \right)
  }
\end{equation*}
Mit:
\begin{itemize}
\item $T_{\mathrm{PPO}}$: Umlaufzeit für einen Umlauf der Bahn
  (primitive periodische Orbits).
\item $S_{\mathrm{PO}}$: Klassische Wirkung,
  $S_{\mathrm{PO}} = \oint_{\mathrm{PO}} p \diff x$.
\item $\sigma_{\mathrm{PO}}$: Maslov-Index
\item $M_{\mathrm{PO}}$: Monodromiematrix (symplektische $(2N-2) \times (2N-2)$-Matrix).
\end{itemize}
\end{enumerate}
Beachte, dass wir nur isolierte periodische Bahnen betrachten, da
sonst der Nenner verschwindet und damit die Spurformel divergiert.

Wir wollen nun noch einmal die Monodromiematrix in Erinnerung
rufen. Diese ist definiert durch
\begin{equation*}
  \begin{pmatrix}
    x_\perp(T_{\mathrm{PO}}) \\
    p_\perp(T_{\mathrm{PO}}) \\
  \end{pmatrix} = 
  M_{\mathrm{PO}}
  \begin{pmatrix}
    x_\perp(0) \\
    p_\perp(0) \\
  \end{pmatrix}
\end{equation*}
und ist symplektisch. Dies bedeutet, dass sowohl $\lambda$, als auch
$1/\lambda$ Eigenwerte der Monodromiematrix $M_{\mathrm{PO}}$ sind.
\begin{align*}
  H(q,p) &= \frac{p^2}{2} + V(q) \qquad q = q^{(0)} + \delta q \qquad p = p^{(0)} + \delta p \\
  \frac{\diff}{\diff t}
  \begin{pmatrix}
    \delta q \\
    \delta p
  \end{pmatrix}
  &= 
  \underbrace{\begin{pmatrix}
    0 & \mathds{1} \\
    - \frac{\partial^2 V(q)}{\partial q_i \partial q_j} & 0
  \end{pmatrix}}_{\equiv \nu^{(0)}(t)}
 \begin{pmatrix}
    \delta q \\
    \delta p
  \end{pmatrix}
\end{align*}
Diagonalisieren von $\nu^{(0)}(t=T)$ führt auf
\begin{equation*}
  \left(
      \begin{array}{ccccc}
      1 & \cdots & \cdots & \cdots & \cdots \\
      \vdots & 1 & \cdots & \cdots & \cdots \\
      \vdots & \vdots & \lambda & \cdots & \cdots \\     
      \vdots & \vdots & \vdots &  1/\lambda & \cdots  \\
      \vdots & \vdots & \vdots &  \vdots & \ddots \\
    \end{array}
    \right)
\end{equation*}
Die Stabilitätsmatrix, die in der Gutzwiller-Spurformel für isolierte
periodische Bahnen auftritt, entspricht gerade der Untermatrix, die
nicht die trivialen Eigenwerte $1$ enthalten.
%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End:
