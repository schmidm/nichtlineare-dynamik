\subsection{Universelles Verhalten beim Übergang von Quasiperiodizität zum Chaos}
Wir untersuchen den Übergang von quasiperiodischer Bewegung auf einem
Zweiertorus zu chaotischer Bewegung anhand einer einfachen
Poincaréabbildung.

\subsubsection{Die eindimensionale Kreisabbildung}
\begin{enumerate}[1. {Schritt:}]
\item Poincaréabbildung für die ungestörte Bewegung auf dem
  Zweiertorus in Polarkoordinaten (beachte $\theta =
  \text{Winkel}/2\pi \in [0,1)$).
  \begin{equation*}
    \theta_{n+1} = f(\theta_n) = (\theta_n + \Omega) \bmod 1
  \end{equation*}
  mit der Windungszahl $\Omega = \omega_1/\omega_2$. $\omega_1$ und
  $\omega_2$ sind die Frequenzen auf dem Zweiertorus.
  \begin{align*}
    \Omega &= \frac{\omega_1}{\omega_2} = \frac{p}{q}
    \quad\text{rational: Periodische Bewegung} \\
    \Omega &= \frac{\omega_1}{\omega_2}
    \quad\text{irrational: Quasiperiodische Bewegung} \\
  \end{align*}

\item Hinzunahme einer nichtlinearen Störung.
  \begin{equation*}
    \theta_{n+1} = f(\theta_n) = \left(\theta_n + \Omega
      - \frac{k}{2\pi} \sin(2\pi \theta_n) \right) \bmod 1
  \end{equation*}
  mit dem Kontrollparameter $k$, der die Stärke der nichtlinearen
  Störung bestimmt.
\end{enumerate}

\begin{notice}[Motivation:]
  Die eindimensionale Kreisabbildung beschreibt als physikalisches
  System einen getriebenen Rotator für den Fall, dass ein konstantes
  Drehmoment $\Gamma\Omega$ zu dem antreibenden Moment addiert wird
  (mit $\Gamma$ groß).
  \begin{equation*}
    \ddot\varphi + \gamma \dot\varphi
    = k f(\varphi) \sum_{n=0}^\infty \delta(t-nT) + \Gamma\Omega
  \end{equation*}
\end{notice}

Bestimmte universelle Eigenschaften der Kreisabbildung sind
unabhängig von der speziellen Form von $f(\theta)$ (vergleiche
Universalität beim Feigenbaumszenario) die Funktion $f$ muss die
folgenden Bedingungen erfüllen:
\begin{itemize}
\item $f(\theta+1) = 1 + f(\theta)$ (hier ohne $\bmod 1$ zu verstehen)
\item Für $|k| < 1$ existieren $f(\theta)$ und ihr Inverses und
  $f(\theta)$ ist differenzierbar (d.h.\ $f(\theta)$ ist ein
  Diffeomorphismus).
\item Bei $k=1$ wird $f^{-1}(\theta)$ nicht differenzierbar und für
  $|k|>1$ existiert kein eindeutiges Inverses von $f(\theta)$.
\end{itemize}

\begin{theorem}[Zur Windungszahl:]
  Für $k\neq0$, d.h.\ mit Störung gilt nicht mehr einfach
  $w=\Omega$. Statt dessen:
  \begin{equation*}
    w = \lim_{n\to\infty} \frac{1}{n} \Bigl[
    \underbrace{f^n(\theta_0)}_{\mathclap{\text{ohne $\bmod 1$}}} - \theta_0 \Bigr]
  \end{equation*}
  \begin{description}
  \item[$w = p/q$ rational:] Modenkopplung.
  \item[$w$ irrational:] Quasiperiodische oder chaotische
    Bewegung.
  \end{description}
\end{theorem}

Zur Verdeutlichung der Modenkopplung: Beobachtung von Huygens
(1629--1695) zur Synchronisation zweier Wanduhren
($w=1$). Hinweis: Modenkopplung heißt nicht, dass die
Trajektorien selbst exakt periodisch sein müssen. Die Kreisabbildung
und damit auch die Windungszahl $w$ der Bewegung hängen ab von
den zwei Parametern $\{\Omega,k\}$.

Diskussion des Phasendiagramms der Kreisabbildung:
\begin{itemize}
\item Arnoldzungen (Bereiche mit Modenkopplung, $w = p/q$
  rational) im Gebiet $|k|<1$. Für $|k|<1$ haben die Gebiete mit und
  ohne Modenkopplung in der $\Omega$-$k$-Ebene eine endliche Fläche.
\item Bei $k=1$ wandern die Arnoldzungen so aufeinander zu, dass die
  Bereiche von $\Omega$, in denen keine Modenkopplung auftritt eine
  selbstähnliche Cantormenge (Fraktal) mit Maß Null ist.
\item $|k|>1$: Chaotisches Verhalten wird möglich. Chaotische und
  nicht chaotische Gebiete sind in der $\Omega$-$k$-Ebene eng
  miteinander verwoben.
\end{itemize}

\subsubsection{Modenkopplung und Farey-Baum}

Frage: Für $k$ fest gegeben, in welchen $\Omega$-Intervallen tritt
Modenkopplung auf?

Für einen Zustand mit rationaler Windungszahl $w = p/q$ lässt
sich das zugehörige $\Omega$-Intervall $\Omega=\Omega(k)$ aus der
Bedingung bestimmen, dass ein stabiler $q$-Zyklus
$f^q_{\Omega,k}(\theta_i^*) = p + \theta_i^*$ mit Elementen
$\theta_1^*,\ldots,\theta_q^*$ in der Kreisabbildung auftritt, d.h.\
es muss gelten
\begin{equation*}
  \left| f'^q_{\Omega,k}(\theta_i^*)\right|
  = \biggl| \prod_{i=1}^q f'_{\Omega,k}(\theta_i^*) \biggr|
  = \biggl| \prod_{i=1}^q 1 - k \cos(2\pi\theta_i^*) \biggr| < 1
\end{equation*}

\begin{example}
  \begin{align*}
    w &= \frac{p}{q} = \frac{0}{1} = 0 \\
    f_{\Omega,k}(\theta_0) &= 0 + \theta_0 \\
    \implies \Omega &= \frac{k}{2\pi} \sin(2\pi\theta_0) \\
    \left| f'_{\Omega,k}(\theta_0)\right| &= |1-k\cos(2\pi\theta_0)|  < 1
  \end{align*}
  Lösung für $k<1$: $|\Omega| < k/(2\pi)$.

  Allgemeine Lösung für beliebiges $w=p/q$ (Bak und Bohr, 1984):
  \begin{itemize}
  \item Für $0< k \le 1$ gehört zu jeder rationalen Windungszahl
    $w=p/q$ ein endliches Intervall $\Delta\Omega(w=p/q,k)$.
  \item Für $0<k<1$ haben die $\Omega$-Intervalle zusammen ein Maß
    \begin{equation*}
      0 < S = \sum_{p,q} \Delta\Omega(w=p/q,k) < 1 .
    \end{equation*}
  \item Für $k=1$ bilden die Intervalle eine vollständige
    \acct{Teufelstreppe} (vollständig heißt Maß $S(w=p/q,k=1)
    = 1$).
  \end{itemize}
\end{example}

Die Teufelstreppe ist eine monoton steigende Funktion auf dem
Intervall $[0,1]$ wobei zu jedem rationalen Funktionswert $p/q$ eine
Stufe endlicher Breite gehört. Die Stufenbreite nimmt mit wachsendem
Nenner $q$ ab. Für zwei Windungszahlen $w = p/q$ und $w' = p'/q'$ hat
die größte Stufe, die zwischen den beiden Stufen liegt, die
Windungszahl $(p+p')/(q+q')$. Die Stufen der Teufelstreppe lassen sich
anhand eines \acct{Farey-Baumes} ordnen, d.h.\ alle rationalen Zahlen
werden nach aufsteigendem Nenner angeordnet, gemäß der Regel, dass die
größte rationale Zahl zwischen $p/q$ und $p'/q'$ durch $(p+p')/(q+q')$
gegeben ist.

Der Farey-Baum ordnet die Bereiche, in denen Modenkopplung auftritt,
nicht nur für die Kreisabbildung sondern auch eine Vielzahl realer
physikalischer Systeme, z.B.\ getriebenes Pendel, Josephson-Kontakte
und System mit Ladungsdichtewellen. Der Übergang von quasiperiodischem
in chaotisches Verhalten wird durch zwei Typen von Universalität
gekennzeichnet.

\subsubsection{Lokale Universalität}
\begin{itemize}
\item Übergang von quasiperiodischem zu chaotischem Verhalten bei
  einer speziellen Windungszahl $w$.
\item Es zeigen sich enge Parallelen zum Feigenbaum-Szenario. Wir
  betrachten als Beispiel wieder die Kreisabbildung:
  \begin{equation*}
    \theta_{n+1} = \left(\theta_n + \Omega
      - \frac{k}{2\pi} \sin(2\pi\theta_n) \right) \bmod 1
    \equiv f(\theta_n)
  \end{equation*}
  Um eine spezielle Windungszahl $w$ festzuhalten, müssen zwei
  Parameter $\Omega$ und $k$, $\Omega(k)$ angepasst werden.
  \begin{equation*}
    w = \lim_{n\to\infty} \frac{1}{n} \left( f^n(\theta_0) - \theta_0 \right)
  \end{equation*}
  wählen wir als irrationale Zahl (notwendig für Quasiperiodizität)
  den sogenannten \acct*{Goldenen Schnitt}
  \begin{equation*}
    w^* = \frac{1}{2} (\sqrt{5} - 1) = \num{0.6180339}
  \end{equation*}
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: