\subsection{Systeme ohne Zeitumkehrinvarianz (antiunitäre Symmetrie)}

Hierfür ist es sinnvoll das \acct{Gaußsche unitäre Ensemble} (GUE)
einzuführen.  Der Hamiltonoperator liegt in der Form einer
hermiteschen $(2\times 2)$-Matrix
\begin{equation*}
  H =
  \begin{pmatrix}
    H_{11} & H_{12} \\
    H^\ast_{12} & H_{22}
  \end{pmatrix}
\end{equation*}
vor. Er enthält vier reelle Parameter
\begin{equation*}
  H_{11}, \quad H_{22}, \quad \real H_{12}, \quad \imag H_{12} \, ,
\end{equation*}
die durch Zufallszahlen gemäß des Wahrscheinlichkeitsmaßes
$\mathcal{P}(H)$ festgelegt werden. Wieder haben wir einige
Forderungen an unser Wahrscheinlichkeitsmaß:
\begin{enumerate}
\item Normierung: $ \int \mathcal{P}(H) \diff H_{11} \diff H_{22} \diff \real H_{12} \diff \imag H_{12} =1 $
\item $\mathcal{P}(H)$ ist invariant unter unitären Transformationen
  $U$ mit $U^\dagger U = U U^\dagger = \mathds{1}$
\item Statistische Unabhängigkeit: $\mathcal{P}(H) = \mathcal{P}_{11}(H_{11}) \mathcal{P}_{22}(H_{22}) \mathcal{P}^R_{12}(\real H_{12}) \mathcal{P}^I_{12} (\imag H_{12})$
\end{enumerate}
Im Folgenden betrachten wir eine infinitesimale unitäre Transformation
\begin{align*}
  U &= \mathds{1} - \ii \bm{\varepsilon} \cdot \bm{\sigma} \\
\intertext{mit}
\bm{\varepsilon} &= (\varepsilon_x,\varepsilon_y,\varepsilon_z)^T \, ,\qquad \bm{\sigma} = (\sigma_x , \sigma_y ,\sigma_z)^T
\end{align*}
Damit folgt:
\begin{align*}
  H' &= H + \diff H \qquad \textrm{mit} \, \diff H = - \ii [\bm{\varepsilon}\cdot \bm{\sigma},H] \\
  \diff H_{11} &= -2 \varepsilon_x \imag H_{12} - 2 \varepsilon_y \real H_{12} \\
  \diff H_{22} &=  2 \varepsilon_x \imag H_{12} + 2 \varepsilon_y \real H_{12} \\
  \diff \real H_{12} &= \varepsilon_y (H_{11} - H_{22}) + 2 \varepsilon_z \imag H_{1}\\
  \diff \imag H_{12} &=  \varepsilon_y (H_{11} - H_{22}) - 2 \varepsilon_z \real H_{1}\\
\end{align*}
Die Änderung des Wahrscheinlichkeitsmaßes unter der infinitesimalen
unitären Transformation soll verschwinden
\begin{align*}
  \Delta \mathcal{P}(H) &= \left\{ \varepsilon_x
    \left[ -2 \imag H_{12} \left(  \frac{\diff \ln \mathcal{P}_{11}}{\diff H_{11}} - \frac{\diff \ln \mathcal{P}_{22}}{\diff H_{22}}\right) + (H_{11} - H_{22}) \frac{\diff \ln \mathcal{P}^I_{12}}{\diff  \imag H_{12}}  \right]
\right. \\
    &+ \varepsilon_y \left[
      -2 \real H_{12} \left(  \frac{\diff \ln \mathcal{P}_{11}}{\diff H_{11}} - \frac{\diff \ln \mathcal{P}_{22}}{\diff H_{22}}\right) + (H_{11} - H_{22}) \frac{\diff \ln \mathcal{P}^R_{12}}{\diff  \imag H_{12}}  
      \right] \left. \right. \\
    &+ \varepsilon_z \left[ 
      2 \imag H_{12} \frac{\diff \ln \mathcal{P}^R_{12}}{\diff \real H_{12}}
      - 2 \real H_{12} \frac{\diff \ln \mathcal{P}^I_{12}}{\diff \real H_{12}}
    \right]  \Big. \Bigg\} \mathcal{P}(H) \\
    &= 0
\end{align*}
Aus dieser Bedingung folgt, dass alle Terme der Form
$\varepsilon_i \{\ldots \}$ verschwinden müssen. Dies führt auf drei
Gleichungen
\begin{align*}
  \frac{1}{H_{11} -H_{22}} \left(  \frac{\diff \ln \mathcal{P}_{11}}{\diff H_{11}} -  \frac{\diff \ln \mathcal{P}_{22}}{\diff H_{22}}\right)  - \frac{1}{2 \imag H_{12}}  \frac{\diff \ln \mathcal{P}^I_{12}}{\diff \imag H_{12}} &= 0 \, ,\\
  \frac{1}{H_{11} -H_{22}}  \left(  \frac{\diff \ln \mathcal{P}_{11}}{\diff H_{11}} -  \frac{\diff \ln \mathcal{P}_{22}}{\diff H_{22}} \right) - \frac{1}{2 \imag H_{12}}  \frac{\diff \ln \mathcal{P}^R_{12}}{\diff \real H_{12}} &= 0 \, ,\\
  \frac{1}{\real H_{12}}  \frac{\diff \ln \mathcal{P}^R_{12}}{\diff \real H_{12}} -  \frac{1}{\imag H_{12}}  \frac{\diff \ln \mathcal{P}^I_{12}}{\diff \imag H_{12}} &= 0 \, .
\end{align*}
Um dieses Gleichungssystem zu lösen, benutzen wir den Ansatz
\begin{equation*}
  \mathcal{P}(H) = c \, \ee^{-A(H^2_{11} + H^2_{22} + 2 (\real H_{12})^2 + 2 (\imag H_{12})^2)} = c \, \ee^{-A \tr{H^2}}
\end{equation*}
Die Konstante $c$ wird durch die Normierung bestimmt. Wir verbleiben
mit der Konstanten $A$. Die Diagonalisierung von
\begin{equation*}
  H =
  \begin{pmatrix}
    E_+ & 0 \\
    0   & E_-
  \end{pmatrix} = U H U^\dagger
\end{equation*}
ist möglich mittels einer unitärer Transformation
\begin{equation*}
  U =
  \begin{pmatrix}
    \cos \vartheta & - \ee^{\ii \varphi} \sin \vartheta \\
    \ee^{\ii \varphi} \sin \vartheta & \cos \vartheta
  \end{pmatrix}
\end{equation*}
Somit erhalten wir:
\begin{align*}
  H_{11} &= E_+ \cos^2 \vartheta + E_- \sin^2\vartheta\\
  H_{11} &= E_+ \sin^2 \vartheta + E_- \cos^2 \vartheta\\
  H_{12} &= H^\ast_{21} = (E_+ - E_-) \ee^{\ii \varphi} \cos \vartheta \sin \vartheta\\ 
  \det{\frac{\partial (H_{11}, H_{22}, \real H_{12} , \imag H_{12})}{\partial (E_+ , E_-, \vartheta, \varphi)}} &= (E_+ - E_-)^2 \cos \vartheta \sin \vartheta
\end{align*}
Mittelung über $\vartheta$ führt auf
\begin{align*}
  \mathcal{P}(E_+,E_-) &= c (E_+ - E_-)^2 \, \ee^{-A (E^2_+ + E^2_-)}
\end{align*}
Mit
\begin{align*}
  E_+ &= E_0 + \frac{s}{2}, \qquad E_- = E_0 - \frac{s}{2}
\end{align*}
erhalten wir
\begin{align*}
  \mathcal{P}(s) &= c' \, s^2 \, \ee^{-A \frac{s^2}{2}} \, .
\end{align*}
Die Konstanten $c'$ und $A$ werden wieder über die Normierung
\begin{equation*}
  \int_{0}^{\infty} \mathcal{P}(s) \diff s = 1
\end{equation*}
und dem Erwartungswert
\begin{equation*}
  \int_{0}^{\infty} s \, \mathcal{P}(s) \diff s = 1
\end{equation*}
bestimmt. Somit erhalten wir für das Gaußsche unitäre Ensemble (GUE)
\begin{equation*}
  \boxed{
    \mathcal{P}(s) = \frac{32}{\pi^2} \, s^2 \, \ee^{-\frac{4}{\pi} s^2}
  }
\end{equation*}
Für kleine Abstände finden wir das Verhalten
\begin{equation*}
  \mathcal{P}(s) \propto s^2 \, .
\end{equation*}

\subsection{Systeme mit Zeitumkehrinvarianz $[H,T] = 0$ und $T^2 = -\mathds{1}$ (Kramers-Entartung)}

Hierfür führen wir das \acct{Gaußsche symplektische Ensemble} (GSE)
ein. Dieses besteht aus einem Hamiltonoperator der Form
\begin{equation*}
  H =
  \begin{pmatrix}
    h_{11} & h_{12} \\
    h_{21} & h_{22} 
  \end{pmatrix}
\end{equation*}
mit den quarternionischen $2\times 2$-Blöcken $h_{ij}$. Als Basis
verwenden wir hierbei die Einheitsmatrix $\mathds{1}$ und die
Sigma-Matrizen $-\ii \bm{\sigma}$. Der Hamiltonoperator $H$ lässt
sich durch eine symplektische Transformation auf eine Diagonalform
\begin{align*}
  \begin{pmatrix}
    E_+ \mathds{1} & 0 \\
    0 & E_- \mathds{1}
  \end{pmatrix}
\end{align*}
bringen ($E_+$ und $E_-$ sind zweifach entartet). Wieder machen wir
den Ansatz
\begin{equation*}
  \mathcal{P}(H) = c \ee^{- A \tr{H^2}}
\end{equation*}
um die Verteilungsfunktion zu erhalten. Die einzelnen Schritte wollen
wir hierbei nicht diskutieren.

\minisec{Zusammenfassung} 
Für alle Wahrscheinlichkeitsdichten $\mathcal{P}(s)$ in den
unterschiedlichen Fällen erhalten wir also
\begin{align*}
  \boxed{ \mathcal{P}(s) = 
  \begin{cases}
    \ee^{-s} &, \textrm{Poissonverteilung für integrable Systeme} \\
    \frac{\pi}{2} \, s \, \ee^{- \frac{\pi}{4} s^2}&, \textrm{Wignerverteilung für GOE-Statistik} \\
    \frac{32}{\pi^2} \, s^2 \, \ee^{- \frac{4}{ \pi} s^2} &, \textrm{für GUE-Statistik} \\
    \frac{2^{18}}{3^6 \pi^3} \, s^4 \, \ee^{- \frac{64}{9 \pi} s^2} &, \textrm{für GSE-Statistik} \\
  \end{cases} }
\end{align*}
Die einzelnen Wahrscheinlichkeitsdichten sind in
Abbildung~\ref{fig:16.7.15-1} dargestellt.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = 0, xmax = 2.5, 
      ymin = 0, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,
      ylabel= $\mathcal{P}(x)$,
      xlabel = $x$,
      width = 6cm,
      xmajorgrids = true
      ]
      \draw [help lines] (axis cs: -1,0) -- (axis cs: 5,0);
      \draw [help lines] (axis cs: 0,-0.5) -- (axis cs: 0,1.5);
      \addplot [MidnightBlue,samples=20,smooth]  {1*exp(-(x))};
      \addplot [DarkOrange3,samples=75,smooth]  {pi/2*x*exp(-pi/4*x^2)};
      \addplot [samples=75,smooth]  {32/pi^2*x^2*exp(-4/pi*x^2)};
      \addplot [purple,samples=75,smooth]  {2^(18)/(pi^3*3^6)*x^4*exp(-64/(9*pi)*x^2)};
      \node[label={[MidnightBlue]-10:{$\mathcal{P}_ {\mathrm{P}}$}}] at (axis cs: 1.75,1.5) {};
      \node[label={[DarkOrange3]-10:{$\mathcal{P}_ {\mathrm{GOE}}$}}] at (axis cs: 1.75,1.2) {};
      \node[label={[]-10:{$\mathcal{P}_ {\mathrm{GUE}}$}}] at (axis cs: 1.75,0.9) {};
      \node[label={[purple]-10:{$\mathcal{P}_ {\mathrm{GSE}}$}}] at (axis cs: 1.75,0.6) {};
    \end{axis}
  \end{tikzpicture} 
  \caption{Wahrscheinlichkeitsdichten $\mathcal{P}(s)$ für die
    Poissonverteilung $\mathcal{P}_\mathrm{P}$, GOE-Statistik
    $\mathcal{P}_{\mathrm{GOE}}$, GUE-Statistik
    $\mathcal{P}_{\mathrm{GUE}}$ und GSE-Statistik
    $\mathcal{P}_{\mathrm{GSE}}$.}
  \label{fig:16.7.15-1}
\end{figure}

\minisec{Beispiele} 
Eine Vielzahl von verschiedenen System besitzen die obig diskutierten
Wahrscheinlichkeitsdichten, bspw.:
\begin{enumerate}
\item Integrable Systeme: Poissonverteilung
  \begin{itemize}
  \item Rechteckbillard mit irrationalen Seitenverhältnissen
  \item Kreisbillard
  \item Wasserstoffatom im Magnetfeld bei Energien
    $\tilde{E} = E \gamma^{-2/3} < -0.5$
  \end{itemize}
\item Systeme mit Zeitumkehrinvarianz $[H,T] =0$ und
  $T^2 = \mathds{1}$: GOE-Statistik
  \begin{itemize}
  \item Stadion Billard 
  \item Sinai Billard (siehe Abbildung~\ref{fig:16.7.15-1})
  \item Wasserstoffatom im Magnetfeld bei Energien
    $\tilde{E} = -0.13 < E \gamma^{-2/3} < 0$
  \end{itemize}
\item Systeme ohne Zeitumkehrivarianz: GUE-Statistik
  \begin{itemize}
  \item Mikrowellenresonator mit magnetisierten Ferritstreifen
  \item Rydberg Exzitonen in starken Magnetfeldern
  \end{itemize}
\end{enumerate}

\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = 0.45\textwidth]{sinai.png}};
  \end{tikzpicture}
  \caption{Spektrale Analyse des Sinai-Billard und GOE-Statistik. Zum
    Vergleich ist auch die Poisson-Statistik eingezeichnet. Von
    \cite{bohigas}.}
  \label{fig:16.7.15-1}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End:
