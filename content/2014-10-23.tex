Eine Flüssigkeit befindet sich zwischen zwei Platten im
Gravitationsfeld. Der Abstand zwischen den beiden Platten ist $h$. Die
obere Platte wird auf konstanter Temperatur $T$ gehalten, die untere
Platte wird geheizt mit $T + \delta T$. Die Konvektionsrollen, die bei
zu starkem Heizen auftreten sind Indikator für ein reguläres
Verhalten.

Wir können das System theoretisch beschreiben. Uns interessiert im
wesentlichen das Geschwindigkeitsfeld der Flüssigkeit $\bm v(\bm
x,t)$. Die Ausgangsgleichungen sind
\begin{itemize}
\item Navier-Stokes-Gleichung für die Strömung:
  \index{Navier-Stokes-Gleichung}
  \[ \varrho \frac{\diff \bm v}{\diff t} = \bm F - \nabla p + \mu
  \nabla^2 \bm v \] mit der Dichte $\varrho$ und der Viskosität
  $\mu$. Für die Kraft $\bm F$ nehmen wir die Gravitationskraft
  $\varrho g \bm e_z$ an.
\item Wärmeleitungsgleichung:\index{Wärmeleitungsgleichung}
  \[ \frac{\diff T}{\diff t} = \kappa \nabla^2 T \]
  mit der Wärmekapazität $\kappa$.
\item Kontinuitätsgleichung:\index{Kontinuitätsgleichung}
  \[ \frac{\partial \varrho}{\partial t} + \nabla \cdot (\varrho \bm
  v) = 0 \] mit den Randbedingungen $T(x,y,z=h,t) = T_0$ und
  $T(x,y,z,z=0,t) = T_0 + \delta T$.
\end{itemize}
Was können wir tun um das System zu vereinfachen?
\begin{itemize}
\item Translationsinvarianz in $y$-Richtung ($v_y = 0$).
\item Die auftretenden Koeffizienten hängen nicht von der Temperatur
  ab, mit Ausnahme der Dichte $\varrho = \bar\varrho(1-2\delta T)$.
\end{itemize}
Damit können wir die Kontinuitätsgleichung schreiben als
\begin{equation*}
  \frac{\partial u}{\partial x} + \frac{\partial w}{\partial z} = 0
\end{equation*}
mit $u = v_x \equiv -\partial_z \psi$ und
$w = v_z \equiv \partial_x \psi$. Hier $\psi(x,z.t)$ ist lediglich eine
weitere Funktion, die die obigen Bedingungen erfüllt.

Dann brauchen wir einen Ansatz für das Temperaturfeld:
\begin{equation*}
  T(x,z,t) = T_0 + \delta T - \frac{\delta T}{h} z + \Theta(x,z,t).
\end{equation*}
Das bedeutet, dass sich für $\Theta = 0$ ein lineares Temperaturprofil
ausbildet. Das bedeutet, dass $\Theta$ die Abweichung davon angibt.

Damit folgt:
\begin{align*}
  \frac{\partial}{\partial t} \nabla^2 \psi
  &= - \det \frac{\partial(\psi,\nabla^2\psi)}{\partial(x,z)}
  + \nu \left( \frac{\partial^4}{\partial x^4} + \frac{\partial^4}{\partial z^4} \right) \psi
  + g a \frac{\partial \Theta}{\partial x} \\
  \frac{\partial}{\partial t} \Theta
  &= - \det \frac{\partial(\psi,\Theta)}{\partial(x,z)}
  + \frac{\delta T}{h} \frac{\partial \psi}{\partial x}
  + \kappa \nabla^2 \Theta
\end{align*}
mit der kinematischen Viskosität $\nu = \mu/\bar\varrho$.

Als Vereinfachung nehmen wir folgende Randbedingungen an
\begin{equation*}
  \Theta(0,0,t) = \Theta(0,h,t) = \psi(0,0,t) = \nabla^2 \psi(0,0,t) = \nabla^2 \psi(0,h,t) = 0.
\end{equation*}
Dies erlaubt eine Fourierentwicklung der Ortsabhängigkeit von $\psi$
und $\Theta$. Die Berücksichtigung nur der niedrigsten Terme liefert
den Ansatz
\begin{align*}
  \frac{a}{1 + a^2} \frac{1}{\kappa} \psi &= \sqrt{2}\, {\color{DarkOrange3}X(t)} \sin\left(\frac{\pi a}{h}\right) \sin\left(\frac{\pi}{h}z\right) \\
  \frac{\pi R}{R_C \delta T} \Theta &= \sqrt{2}\, {\color{DarkOrange3}Y(t)} \cos\left(\frac{\pi a}{h}\right) \sin\left(\frac{\pi}{h}z\right)
  - {\color{DarkOrange3}Z(t)} \sin\left(\frac{2\pi}{h}z\right)
 \end{align*}
 mit der Rayleigh-Zahl $R \equiv gah^3\delta T/\kappa \nu$, dem
 Verhältnis der Längen in der Zelle $a$ und $R_C = \pi^4
 (1+a^2)^3/a^2$. Einsetzen und Vernachlässigung höherer Harmonischer
 liefert das \acct{Lorenz-Modell}
 \begin{align*}
   \dot X &= - \sigma X + \sigma Y &          && \sigma &= \frac{\nu}{\kappa} && \text{(Prandl-Zahl)} \\
   \dot Y &= r X - Y - X Z         &\text{mit}&&      r &= \frac{4}{1 + a^2} \\
   \dot Z &= X Y - b Z             &          &&      b &= \frac{R}{R_C} \sim \delta T
 \end{align*}
 wobei der Punkt hier die Ableitung nach $\tau = \pi^2 (1+a^2) \kappa
 t / h^2$ ist. Dies sind drei gekoppelte Differentialgleichungen
 erster Ordnung. Sie lassen sich auch zu einem Vektor zusammenfassen
 $\dot{\bm x} = \bm F(\bm x)$.

\paragraph{1. Schritt:}
Wir suchen nach stationären Lösungen. Mathematisch gesehen sind dies
die Fixpunkte $\bm F(\bm x) = 0$ der Gleichung. Wir finden drei
Fixpunkte
\begin{equation*}
  \bm x_1 = 0 \;,\quad
  \bm x_{2,3} =
  \begin{pmatrix}
    \pm \sqrt{b(r-1)} \\
    \pm \sqrt{b(r-1)} \\
    r-1\\
  \end{pmatrix}
\end{equation*}

\paragraph{2. Schritt:}
Als nächstes muss die Differentialgleichung um die Fixpunkte
linearisiert werden um Stabilität der gefundenen stationären Lösungen
zu untersuchen. Es zeigt sich:
\begin{enumerate}
\item $\bm x_1 = 0$ ist ein stabiler Fixpunkt ($\lambda_i < 0$) für
  $0<r<1$. Dies entspricht einer reinen Wärmeleitung.
\item Die Linearisierung der Lorenzgleichung um die Fixpunkt $\bm
  x_{2,3}$ ist reell für $r > 1$.
  \begin{equation*}
    \left.\frac{\partial F_i}{\partial x_i}\right|_{\bm x = \bm x_{2,3}} =
    \underbrace{
    \begin{pmatrix}
      -\sigma & \sigma & 0 \\
      1 & -1 & -c \\
      c & c & -b \\
    \end{pmatrix}
    }_{\mathcal A}
  \end{equation*}
  mit $c \equiv \pm \sqrt{b(r-1)}$. Die Eigenwerte von $\mathcal A$
  sind gegeben durch das charakteristische Polynom $\chi(\lambda)$:
  \begin{equation*}
    \chi(\lambda) = \det(\mathcal A - \lambda\mathds{1}) = \ldots = - \bigl[ \lambda^3 + (\sigma+b+1)\lambda^2 + b(\sigma+r)\lambda + 2b\sigma(r-1)\bigr].
  \end{equation*}
  Man erhält die Eigenwerte als Nullstellen von $\chi(\lambda) = 0$ (vgl.\ Abbildung~\ref{fig:2.1}):
  \begin{align*}
    r < 1 &: \lambda_{1,2} < 0, \lambda_3 > 0 : \text{instabile Fixpunkte ($\bm x_{2,3}$) (aber $\bm x_1=0$ ist stabil)} \\
    r = 0 &: \lambda_1 = 0, \lambda_2 = -b, \lambda_3 = -(\sigma+1) : \text{marginal stabiler Fixpunkt} \\
    1 < r < r_1 &: \text{drei reelle Nullstellen}, \lambda_{1,2,3} < 0 : \parbox[t]{5cm}{stabile Fixpunkte ($\bm x_{2,3}$)\\Konvektion (stationäres Geschwindigkeitsfeld)} \\
  \end{align*}
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx]
      \def\s{1}
      \def\b{1}
      \clip (-3.5,-2.5) rectangle (3.5,2.5);
      \draw[->] (0,-2) -- (0,2) node[left] {$\chi(\lambda)$};
      \draw[->] (-3,0) -- (3,0) node[below] {$\lambda$};
      \def\r{1}
      \draw plot[domain=-3:3,smooth] (\x,{-((\x)^3+(\s+\b+1)*(\x)^2+\b*(\s+\r)*\x+2*\b*\s*(\r-1))});
      \def\r{1.25}
      \draw[MidnightBlue] plot[domain=-3:3,smooth] (\x,{-((\x)^3+(\s+\b+1)*(\x)^2+\b*(\s+\r)*\x+2*\b*\s*(\r-1))});
      \def\r{1.5}
      \draw[DarkOrange3] plot[domain=-3:3,smooth] (\x,{-((\x)^3+(\s+\b+1)*(\x)^2+\b*(\s+\r)*\x+2*\b*\s*(\r-1))});
    \end{tikzpicture}
    \caption{
      Illustration zu den verschiedenen Fällen von $r$ und den
      zugehörigen Nullstellen des charakteristischen Polynoms.
    }
    \label{fig:2.1}
  \end{figure}
  \begin{description}
  \item[$r_1 < r < r_C$:] Eine reelle Nullstelle $\lambda < 0$, zwei
    komplexe Nullstellen $\lambda_{2,3}$ mit $\real \lambda_{2,3} <
    0$. Dabei ist $\bm x_{2,3}$ weiterhin ein stabiler Fixpunkt
    (Konvektion).

  \item[$r = r_C$:] $\lambda_1 < 0$, $\lambda_{2,3} = \pm \ii \omega$ mit
    $\omega \in \mathbb R$: Hopf-Bifurkation

  \item[$r > r_C$:] $\lambda_1 < 0$, $\lambda_{2,3}$ sind komplex mit
    $\real \lambda_{2,3} > 0$. Die Fixpunkte $\bm x_{2,3}$ werden
    instabil.
  \end{description}
  In der Hopf-Bifurkation entstehen zwei stabile Grenzzyklen (stabile
  periodische Bahnen) gegen die das System für $t \to \infty$
  strebt. Daraus resultiert eine periodische Änderung der
  Konvektionsrollen.

  Ein weiterer Anstieg der Temperaturdifferenz ($r \gg r_C$) führt zu
  turbulenter Strömung (Chaos).
\end{enumerate}

\section%
[Mathematische Modelle zum deterministischen Chaos]%
{Mathematische Modelle zum deterministischen Chaos: Abbildungen (Maps)}

Sei $\bm x \in \mathbb R^N$ und $\bm f$ eine Funktion $\bm f : \mathbb
R^N \to \mathbb R^N, \bm x \mapsto \bm y = \bm f(\bm x)$.  Sei $\bm
x_0 \in \mathbb R^N$ ein Startpunkt, so erzeugt die Abbildung $\bm f$
eine Folge von Punkten
\[ \bm x_{n+1} = \bm f(\bm x_n) \; , \quad n = 0,1,2,\ldots \]

\begin{example}
  \begin{enumerate}
  \item Stückweise lineare eindimensionale Abbildung
    (Bernoulli-Shift):
    \begin{equation*}
      x_{n+1} = 2 x_n \bmod 1
    \end{equation*}
  \item Zweidimensionale Bäcker-Transformation:
    \begin{align*}
      x_{n+1} &= 2 x_n \bmod 1 \\
      y_{n+1} &= \left(a y_n + \frac{1}{2} [2 x_n]\right) \; , \quad a \leq \frac{1}{2}
    \end{align*}
  \item Eindimensionale quadratische Abbildung (logistische Abbildung)
    \begin{equation*}
      x_{n+1} = r x_n (1-x_n) \; , \quad x_n \in [0,1], 0<r\leq 4
    \end{equation*}
  \item Hénon-Abbildung
    \begin{align*}
      x_{n+1} &= 1 - a x_n^2 + y_n \\
      y_{n+1} &= b x_n \; , \quad |b| < 1
    \end{align*}
  \item Chivikov-Abbildung
    \begin{align*}
      P_{n+1} &= P_n - k \sin \Theta_n \\
      \Theta_{n+1} &= \Theta_n + P_{n+1}
    \end{align*}
  \end{enumerate}
\end{example}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: