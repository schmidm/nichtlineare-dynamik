\begin{notice}[Wiederholung:]
  Wir hatten kennengelernt:
  \begin{enumerate}
  \item Verallgemeinerte Dimension $D_q$
  \item Verallgemeinerte Entropie $K_q$
    \begin{equation*}
      K_q = - \lim_{\ell\to0} \lim_{n\to\infty} \frac{1}{q-1} \frac{1}{n}
      \ln \sum_{i_0\ldots i_n} P_{i_1\ldots i_n}^q
    \end{equation*}
    Der Limes $\lim_{q\to1} K_q = K_1 = K$ liefert die $K$-Entropie.
  \end{enumerate}
  Außerdem hatten wir $\sum_i P_i^q$ berechnet.
\end{notice}

Nun berechnen wir die $K$-Entropie ähnlich zur Berechnung $\sum_i
P_i^q$ für $D_q$:
\begin{equation*}
  \sum_{i_0\ldots i_n} P_{i_1\ldots i_n}^q =
  \frac1N \sum_{i=1}^N \left[ \frac1N \sum_{j=1}^N
    \Theta\left( \ell - \sqrt{
        \sum_{m=0}^n (\bm x_{i+m} - \bm x_{j+m})^2} \right) \right]^{q-1}
  = C_n^q(\ell)
\end{equation*}
\index{Verallgemeinertes Korrelationssignal} 
Für den Spezialfall $q=2$ ist $C_n^2(\ell) \equiv C_n(\ell)$ die
\acct*{Verallgemeinerung des Korrelationssignals} $C(\ell)$. Man
beachte, dass $C_{n=0}^q(\ell) = C_q(\ell)$. Die Funktion
$C_n^q(\ell)$ lässt sich für jede Trajektorie direkt berechnen. Es ist
keine explizite Zellaufteilung des Phasenraumes erforderlich. Es
gilt\footnote{ohne Rechnung}:
\begin{equation*}
  \lim_{\ell\to0} \lim_{n\to\infty} C_n^q(\ell) =
  (q-1) D_q \log \ell + n (q-1) K_q
\end{equation*}
Die verallgmeinerte Dimension $D_q$, sowie die verallgemeinerte
Entropie $K_q$ können somit aus experimentell gemessenen Daten bestimmt
werden. Tragen wir das Korrelationssignal logarithmisch über
$\log \ell$ auf, können die Größen abgelesen werden (siehe
Abbildung~\ref{fig:8.1}).
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw[<->] (0,3) node[left] {$\log C_n^q(\ell)$}
    |- (3,0) node[below] {$\log\ell$};
    \draw[name path=A,dashed] (2,-2pt) node[below] {$0$} -- (2,3);
    \draw[name path=B,DarkOrange3] ($(.2,.95)!-.4!(1.2,1.2)$)
    -- node[sloped,below] {Steigung $(q-1)D_q$} ($(.2,.95)!2.2!(1.2,1.2)$);
    \draw[MidnightBlue] plot[only marks,mark=x] coordinates {
      (.2,.95) (.4,1) (.6,1.05) (.8,1.1) (1,1.15)
      (1.2,1.2) (1.4,1.4) (1.6,1.7) (1.8,2.3) (1.9,3) };
    \path[name intersections={of=A and B,name=i}]
    (i-1) node[DarkOrange3,dot,pin={[DarkOrange3]above right:$n(q-1)K_q$}] {};
  \end{tikzpicture}
  \caption{Auftragung von $\log C_n^q(\ell)$ über $\log\ell$.}
  \label{fig:8.1}
\end{figure}

\subsection{Bilder seltsamer Attraktoren und fraktaler Grenzen}

Betrachte eine komplexe Funktion $f(z) = z^3 - 1 = 0$. Wir finden drei
Nullstellen und zwar bei $z_1 = 0$, $z_2 = \ee^{2\pi\ii/3}$, $z_3 =
\ee^{4\pi\ii/3}$. Verwende das Newtonsche Verfahren zur Bestimmung der
Nullstellen
\begin{equation*}
  f(z) \approx f(z_0) + f'(z_0) (z-z_0) = 0
\end{equation*}
Iteration liefert
\begin{equation*}
  z_{n+1} = z_n - \frac{f(z_n)}{f'(z_n)}
\end{equation*}
Die Anwendung auf die gegebene Funktion $f(z)$ liefert uns
\begin{align*}
  f(z) &= z^3 - 1 \\
  f'(z) &= 3 z^2 \\
  z_{n+1} &= z_n - \frac{z_n^3 - 1}{3 z_n^2}
\end{align*}
Offensichtlich hat die Abbildung drei stabile Fixpunkte bei den
Nullstellen von $f(z)$. Nun stellt sich die Frage, auf welchen der
drei Fixpunkte die Abbildung konvergiert, bei gegebenem Startwert
$z_0$. Anders gesagt: Welche Form haben die Attraktionsgebiete der
drei Fixpunkte? Eine naive Vorstellung ist in Abbildung~\ref{fig:8.2}
dargestellt.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}
    \draw[->] (0,-2) -- (0,2);
    \draw[->] (-2,0) -- (2,0);
    \foreach \i in {0,2/3,4/3} {
      \node[MidnightBlue,dot] at (\i*180:1) {};
    }
    \foreach \i in {1/3,3/3,5/3} {
      \draw[DarkOrange3] (0,0) -- (\i*180:2);
    }
  \end{tikzpicture}
  \caption{Naive Vorstellung der Attraktionsgebiete.}
  \label{fig:8.2}
\end{figure}

Eine numerische Untersuchung zeigt jedoch, dass die Attraktionsgebiete
sehr kompliziert und miteinander verwoben sind, es sind selbstähnliche
Strukturen.

Die Grenzen der Attraktionsgebiete rationaler Abbildungen werden als
\acct{Julia-Mengen} bezeichnet (Julia, 1918). Julia-Mengen sind
normalerweise Fraktale, die Iterierten von Punkten dieser Menge zeigen
chaotisches Verhalten.

Ein weiteres Beispiel einer Julia-Menge ist
\begin{equation*}
  z_{n+1} = f_C(z_n) = z_n^2 + c
\end{equation*}
Ein Fixpunkt ist formal bei $z=\infty$, was die Grenze des
Attraktionsgebietes ist. Dieses bildet die Julia-Menge $J_c$ (hängt
von $c$ ab).
\begin{equation*}
  J_c = \text{Rand von }\{z\mid\lim_{n\to\infty} f_c^n(z) \to \infty\}
\end{equation*}

\begin{theorem}[Theorem von Julia und Fatou:]
  $J_c$ ist zusammenhängend genau dann, wenn gilt
  \begin{equation*}
    \lim_{n\to\infty} f_c^n(0) \nrightarrow \infty
  \end{equation*}
\end{theorem}

Die Mandelbrotmenge ist definiert als
\begin{align*}
  M &= \{ c \mid \text{$J_c$ ist zusammenhängend} \} \\
  &= \{ c \mid \lim_{n\to\infty} f_c^n(0) \nrightarrow \infty \}
\end{align*}

\section{Der Übergang von Quasiperiodizität zum Chaos}

Frage: Wie ist das Einsetzen von zeitlicher Turbulenz in Flüssigkeiten
mit dem Auftreten eines seltsamen Attraktors verbunden?

\begin{notice}[Wiederholung:]
  Die \acct{Hopf-Bifurkation} erhält man aus der folgenden
  Differentialgleichung in Polarkoordinaten:
  \begin{align*}
    \begin{drcases*}
      \frac{\diff r}{\diff t} = -(\Gamma r + r^3) \\
      \frac{\diff \theta}{\diff t} = \omega \\
    \end{drcases*}
    \implies
    \begin{dcases*}
      r^2(t) = \frac{\Gamma r_0^2 \ee^{-2\Gamma t}}
      {r_0^2(1 - \ee^{-2\Gamma t}) + \Gamma} & mit $r_0 = r(t_0)$ \\
      \theta(t) = \omega t & mit $\theta(t=0) = 0$ \\
    \end{dcases*}
  \end{align*}
  \begin{description}
  \item[$\Gamma \ge 0$:] Trajektorie läuft zum Ursprung
  \item[$\Gamma < 0$:] Grenzzyklus mit Radius $r_\infty =
    \sqrt{-\Gamma}$.
  \end{description}
  Bei einer Hopf-Bifurkation überquert ein Paar komplex konjugierter
  Eigenwerte die imaginäre Achse.
\end{notice}

\subsection{Die Landau-Route zur Turbulenz}
Das System durchläuft eine Vielzahl von Hopf-Bi\-fur\-ka\-tio\-nen
(vgl.\ Feigenbaumszenario) mit Übergang
$\Gamma_i > 0 \to \Gamma_i < 0$. Jede Hopf-Bifurkation führt eine neue
Frequenz mit $\omega_i$ in das System ein. Bei unendlich vielen
Hopf-Bifurkationen wird im Grenzfall ein kontinuierliches
Leistungsspektrum (und damit Chaos) erreicht. Experimente zeigen, dass
die Landau-Route zur Turbulenz beim Bénard-Experiment nicht beobachtet
wird. Schon nach dem Auftreten von zwei Fundamentalfrequenzen (also
schon nach zwei Hopf-Bifurkationen) zeigt das Experiment ein
kontinuierliches Leistungsspektrum.

\subsection{Die Ruelle-Takens-Newhouse-Route zum Chaos:}
Theoretische Untersuchungen (1978) zeigen, dass bereits nach drei
Hopf-Bifurkationen die reguläre Bewegung instabil werden kann und die
Trajektorien können sich dann auf einen seltsamen Attraktor
zubewegen. Dies entspricht einem gegenüber der Landau-Route verkürzten
Weg zur Turbulenz.

Ein Beweis des Theorems ist mathematisch aufwändig.
\begin{notice}[Bemerkungen:]
  \begin{itemize}
  \item Es ist kein Chaos möglich vor der dritten
    Hopf-Bifurkation. Nach der zweiten Hopf-Bifurkation läuft die
    Trajektorie auf einen Torus (zweidimensionale Mannigfaltigkeit).
    Auf diesem Torus ist dann Chaos nach dem Poincaré-Bendixon-Theorem
    verboten.
  \item Nach der dritten Hopf-Bifurkation läuft die Bewegung auf einen
    Dreiertorus. Es wurde gezeigt, dass bestimmte Störungen existieren
    (die zwar infinitesimal klein sein können, aber gewisse
    Eigenschaften erfüllen müssen), welche dann die quasiperiodische
    Bewegung auf einem Dreiertorus in die chaotische Bewegung auf
    einem seltsamen Attraktor umwandeln.
  \item Prinzipiell möglich ist auch das Aufbrechen eines Zweiertorus,
    z.B.\ wenn der Dreiertorus direkt nach der Entstehung bereits so
    instabil ist, dass die dritte inkommensurable Frequenz nicht
    beobachtet werden kann.
  \end{itemize}
\end{notice}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: