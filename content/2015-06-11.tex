\begin{notice}[Wiederholung:] Im Zuge der semiklassischen
  Quantisierung integrabler Systeme haben wir die
  EKB-Torusquantisierung kennen gelernt, diese lautet
  \begin{equation*}
    E_{\bm{n}} = H \left( I = \left(\bm{n} + \frac{\bm{\alpha}}{4}\right) \hbar \right)
  \end{equation*}
  mit einem vollständigen Satz von Quantenzahlen $\bm{n}$. Mit Hilfe
  der Berry-Tabor-Formel kann die Zustandsdichte integrabler Systeme
  angegeben werden. Für ein System mit zwei Freiheitsgraden ($N=2$)
  folgt
  \begin{equation*}
    d(E) = \bar{d}(E) + \frac{1}{\pi \hbar^{3/2}} \sum_{\bm{M} \neq 0} \frac{T_{\bm{M}}}{\sqrt{M^3_2 \left|d''_{E}\right|}} \cos\left( \frac{S_{\bm{M}}}{\hbar} - \frac{\pi}{2} \sigma_{\bm{M}} - \frac{\pi}{4} \right)
  \end{equation*}
  mit dem Thomas-Fermi-Term
  \begin{equation*}
    \bar{d}(E) = \frac{1}{(2\pi\hbar)^2} \iint \diff \bm{q} \diff \bm{p} \, \delta \left( E - H(\bm{p}, \bm{q}) \right) \, ,
  \end{equation*}
  den Umlaufzahlen $\bm{M} = (M_1,M_2)$ der periodischen Bahnen auf
  resonanten Tori: $M_1 : M_2 = \omega_1 : \omega_2$ rational, den
  Umlaufzeiten der periodischen Bahnen $T_{\bm{M}}$, der Wirkung der
  periodischen Bahnen $S_{\bm{M}}$ und dem Maslov-Index der
  periodischen Bahnen $\sigma_{\bm{M}}$. Die Funktion $d_E(I_1)$ ist
  hierbei definiert durch
  \begin{equation*}
    H(I_1,I_2 = d_E(I_1)) = E.
  \end{equation*}
  Wichtig ist dabei, dass in der Berry-Tabor-Formel kein vollständiger
  Satz an Quantenzahlen $n_k$ mit $k=1, \ldots,N$ eingeht. Da die
  Berry-Tabor-Formel nur die semiklassische Zustandsichte für integrable
  Systeme beschreibt, bleibt die Frage nach einer semiklassischen
  Theorie, welche die Zustandsdichte für nicht integrable (chaotische)
  Systeme richtig wiedergibt.
\end{notice}


\subsection{Spurformel}
Die \acct{Spurformel} ist in der Lage eine semiklassische
Zustandsdichte für nicht integrable (chaotische) Systeme zu
liefern. Dazu wollen wir zunächst rein quantenmechanisch vorgehen. Die
Zustandsdichte für ein gebundenes Spektrum lautet
\begin{equation*}
  d(E) = \sum_m c_m \, \delta(E-E_m) \, ,
\end{equation*}
dabei beschreibt $c_m$ die Multiplizität, also den Entartungsgrad und
$E_m$ die Energieeigenwerte, wobei $m$ nur ein Abzählindex
darstellt. Der \acct*{Greensche Operator} \index{Greenscher Operator}
(retardiert) ist gegeben durch
\begin{equation*}
  G^+_E = \sum_n \frac{\Ket{n}\Bra{n}}{E-E_n + \ii \varepsilon} \, ,
\end{equation*}
wobei das $+$ für retardiert steht. Der Zusammenhang zwischen
quantenmechanischer Zustandsdichte und dem Greenschen Operator liegt
in der Relation
\begin{equation*}
  d(E) = - \frac{1}{\pi} \textrm{Im}\left(\tr{G^+_E} \right) \, .
\end{equation*}

\begin{proof}
  Der Beweis hierfür wird mit Hilfe einer kontinuierlichen Basis $\ket{x}$ durchgeführt. Der Greensche Operator in dieser kontinuierlichen Dartsellung ergibt sich zu
  \begin{align*}
    G^+_E(x,x') &= \sum_n \frac{\braket{x|n} \braket{n|x'}}{E-E_n + \ii \varepsilon} \\
                &= \sum_n \frac{\psi^\ast_n(x) \psi_n(x')}{E-E_n + \ii \varepsilon} \, .
  \end{align*}
  Bildung der Spur dieses Operators führt auf
  \begin{align*}
    \tr{G^+_E(x,x')} &= \int \diff^3 x G^+_E(x,x) \\
                     &= \sum_n \frac{\int  |\psi_n(x)|^2 \diff^3 x}{E-E_n + \ii  \varepsilon} \\
                     &= \sum_n \frac{1}{E-E_n + \ii \varepsilon} \\
                     &= \sum_n P \frac{1}{E-E_n} - \ii \pi \delta(E-E_n)
  \end{align*}
  mit dem \acct{Cauchy Hauptwert} $P$, der definiert ist als
  \begin{equation*}
    \frac{1}{x + \ii \varepsilon} = P \frac{1}{x} - \ii \pi \delta(x) \, .
  \end{equation*}
  Der Imaginärteil der Spur des Greenschen Operators liefert dann genau die Zustandsdichte
  \begin{equation*}
    - \frac{1}{\pi} \textrm{Im}\tr{G^+_E(x,x')} = \sum_n \delta(E-E_n) = d(E) \, .
  \end{equation*}
\end{proof}

Die Ausgangsformel für semiklassische Spurformeln ist:
\begin{align*}
  G^+_{E,\mathrm{qm}} &\to  G^+_{E,\mathrm{SCL}} \\
  \tr{G^+_{E,\mathrm{qm}}} &\to \int \diff^3 x  G^+_{E,\mathrm{SCL}} 
\end{align*}
wobei der Index $\mathrm{SCL}$ für semi-classical, auf deutsch semiklassisch, steht.

\subsubsection{Quantenmechanischer und Semiklassischer Propagator}
Den quantenmechanischen Greenschen Operator erhält man durch geschickte Umformung des Termes
\begin{equation*}
    \frac{1}{E - E_n + \ii \varepsilon} = \frac{1}{E - H + \ii \varepsilon} = \frac{1}{\ii \hbar} \int_{0}^{\infty} \ee^{\ii (E-H+\ii \varepsilon)t/\hbar } \diff t \, .
\end{equation*}
Es ergibt sich daher
\begin{align*}
  G^+_{E,\mathrm{qm}}(x,x') &= \frac{1}{\ii \hbar} \int_{0}^{\infty} \diff t \, \ee^{\ii (E + \ii \varepsilon) t/\hbar} \sum_n \psi^\ast_n(x) \psi_n(x') \ee^{-\ii E_n t/\hbar}
\end{align*}
und unter Verwendung des Propagators
\begin{align*}
 \boxed{ G^+_{E,\mathrm{qm}}(x,x')  \equiv \frac{1}{\ii \hbar} \int_{0}^{\infty} \diff t \, \ee^{\ii (E + \ii \varepsilon) t/\hbar} K_{\mathrm{qm}}(x,t,x',t'=0) }
\end{align*}
Mit dem \acct*{quantenmechanischen Propagator}
\index{Quantenmechanischer Propagator}
\begin{equation*}
  K_{\mathrm{qm}} (x,t,x',t'=0) = \sum_n \psi^\ast_n(x) \psi_n(x') \ee^{-\ii E_n t/\hbar} \, .
\end{equation*}

\subsubsection{Feynmansche Pfadintegraldarstellung des quantenmechanischen Propagators}

In der \acct{Pfadintegraldarstellung} lässt sich der quantenmechanische Propagator schreiben als
\begin{equation*}
  \boxed{K_{\mathrm{qm}} (x,t,x',t') = \int \mathscr{D}[y(\tau)] \ee^{\ii/\hbar \int_{t'}^{t} \diff \tau \, L(\dot{y},y,\tau)} }
\end{equation*}
Diese Definition enthält neben der Lagrangefunktion $L(\dot{y},y,\tau)$ auch die Integration über alle Pfade $\mathscr{D}[y(\tau)]$, die den gleichen Anfangspunkt $y(t')$ und Endpunkt $y(t)$ besitzen. Dabei muss beachtet werden, dass auch die nicht klassischen Pfade enthalten sind. Der semiklassische Propagator folgt aus der Berechnung der Feynmanschen Pfadintegrale unter stationärer Phasenapproximation. Die Stationaritätsbedingung lautet dabei
\begin{equation*}
  \delta \int_{t'}^{t} \diff \tau \, L(\dot{y},y,\tau) = 0 
\end{equation*}
und ist gerade das Hamiltonsche Variationsprinzip, d.h. $y(\tau)$ beschreibt gerade die klassische Bahn.

Die \acct{Van-Vleck-Formel} ist 
\begin{equation*}
 \boxed{  K_{\mathrm{SCL}}(x,t,x',t'=0) = (2\pi \ii \hbar)^{N/2} \sum_{\mathrm{SCL}} \sqrt{|c|} \ee^{\ii/\hbar R(x,x',t) - \ii \frac{\pi}{2} \kappa} }
\end{equation*}
Die Summe $\sum_{\mathrm{SCL}}$ läuft dabei über alle klassischen Trajektorien mit Startpunkt $x'(0)$ und Endpunkt $x(t)$. Der Koeffizient $c$ ist gegeben durch
\begin{equation*}
  c = \det{\frac{\partial^2 R}{\partial x \partial x'}}
\end{equation*}
wobei die Funktion $R(x,x',t)$ die klassische Lagrangefunktion enthält:
\begin{equation*}
  R(x,x',t) = \int_{0}^{t} \diff \tau \, L(\dot{x}(\tau),x,\tau)
\end{equation*}
Die Variable $\kappa$ ist die Zahl der negativen Eigenwerte der zweiten Variation von $R$ nach $x$ und entspricht damit der Anzahl der Kaustiken entlang des Weges von $x'(0)$ nach $x'(t)$.

Die semiklassiche Greensche Funktion ergibt sich nun durch Ersetzung des quantenmechanischen Propagators durch sein semiklassisches Pendant.
\begin{align*}
  G^+_{E,\mathrm{SCL}} &= \frac{1}{\ii \hbar} \int_{0}^{\infty} \diff t \ee^{\ii /\hbar (E + \ii \varepsilon) t} K_{\mathrm{SCL}}(x,t,x',t'=0) \\
  &= \frac{(2\pi \ii \hbar)^{-N/2}}{\ii \hbar} \sum_{\mathrm{SCL} , \textrm{E var.}} \int_{0}^{\infty} \diff t \, \sqrt{|c|} \ee^{\ii / \hbar (R(x,x',t) + Et)- \ii \kappa \pi/2} 
\end{align*}
Die Summe $\sum_{\mathrm{SCL} , \textrm{E var.}}$ ist dabei so zu verstehen, dass sie über alle klassische Trajektorien läuft und die Energie $E$ dabei variabel ist. Unter der Bedingung der stationären Phase 
\[ E = - \frac{\partial R}{\partial t} \bigg|_{t_0} \]
ergibt sich 
\begin{equation*}
  G^+_{E,\mathrm{SCL}} = \frac{2\pi}{(2\pi \ii \hbar)^{(N+1)/2}} \sum_{\mathrm{SCL} , \textrm{E fest}} \sqrt{|D|} \ee^{\ii S(x,x',E) - \ii \mu \pi/2}
\end{equation*}
mit der Wirkung entlang der klassischen Bahn
\begin{equation*}
  S(x,x',E) = R(x,x',t) + Et = \int_{x'}^{x} p \diff \tilde{x} 
\end{equation*}
und
\begin{equation*}
  D(x,x',E) = \frac{c}{\left( \frac{\partial^2 R}{\partial t^2}\right)_{t_0}} = \det{
    \begin{pmatrix}
      \frac{\partial^2 S}{\partial \bm{x} \partial \bm{x}'} &      \frac{\partial^2 S}{ \partial \bm{x}' \partial E}  \\
          \frac{\partial^2 S}{\partial E \partial \bm{x}} &      \frac{\partial^2 S}{\partial E \partial E}  
    \end{pmatrix}
}
\end{equation*}
sowie.
\begin{equation*}
  \mu =
  \begin{cases}
    \kappa    &, \frac{\partial^2 R}{\partial t^2}\big|_{t_0} > 0  \\
    \kappa +1 & , \frac{\partial^2 R}{\partial t^2}\big|_{t_0} < 0 
  \end{cases}
\end{equation*}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End:
