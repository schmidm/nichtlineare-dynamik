\chapter{Quantenchaos}

\section{Chaos in klassischen Systemen und Quantensystemen}
\begin{itemize}
\item Klassische chaotische Systeme: Bahnen sind instabile Lösungen da
  nichtlineare Bewegungsleichungen (Liapunov Exponent $\lambda >0$)
  auftreten.
\item Quantenmechanik: Wellenfunktionen (Zustände) sind Lösungen der
  Schrödingergleichung (lineare SGL)
  \begin{align*}
     \ii \hbar \partial_t \psi(\bm{x},t) &= \hat{H}\psi(\bm{x},t),
  \end{align*}
  wobei der Hamiltonoperator $\hat{H}$ und die zeitliche Ableitung
  $\partial_t$ lineare Operatoren sind. Hier stellt sich die Frage
  nach dem Chaos, bzw.\ was man unter Quantenchaos verstehen
  kann. Somit suchen wir letztlich eine Definition von Quantenchaos
  ohne den Rückgriff auf das korrespondierende klassische System.
\end{itemize}

Beispiele von Spektren (Dargestellt in Abbildung~\ref{fig:2.7.15-0}):
\begin{itemize}
\item Wasserstoffatom im Magnetfeld
\item Poisson: Unkorrelierte, zufällige Ereignisse (z.B.\ Radioaktiver
  Zerfall)
\item Folge von Primzahlen
\item Streuprozess: $ \mathrm{n} + ^{166}\mathrm{Er}$,  Kernanregungen
  (Vielteilchensystem mit starker Wechselwirkung)
\item Sinai-Billard
\item Nullstellen der \acct{Riemannschen Zetafunktion}
  \begin{align*}
    \zeta(s) &= \sum_{n=1}^{\infty} \frac{1}{n^s} = \prod_{p} \bigg(\sum_{n=0}^{\infty} \frac{1}{p^{ns}} \bigg) = \prod_{p}\left(1-p^{-s} \right)^{-1},
  \end{align*}
  wobei $p$ nach dem zweiten Gleichheitszeichen das Produkt über alle
  Primzahlen symbolisiert. Die nichttrivialen Nullstellen liegen auf
  der Achse $s=1/2 + \ii x$ mit $x \in \mathbb{R}$ (Riemannsche
  Vermutung).
\item Uniformes Spektrum (z.B.\ 1d harmonischer Oszillator)
\end{itemize}

\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = 0.5\textwidth]{spektrum.png}};
  \end{tikzpicture}
  \caption{Segmente verschiedener Spektren mit je 50 Niveaus. Die
    Pfeilspitzen kennzeichnen das Auftreten von Energienieaus deren
    Abstand kleiner als $1/4$ ist. Von \cite{Dunkl}.}
  \label{fig:2.7.15-0}
\end{figure}

\minisec{Hinweis auf Quantenchaos} 

Wir betrachten die statistische Verteilung von Niveauabständen, wie
sie etwa in Abbildung~\ref{fig:2.7.15-0} zu sehen ist.. Der Abstand zweier
benachbarter Linien
\begin{align*}
  s_n &= E_{n+1} - E_n
\end{align*}
führt auf eine Häufigkeitsverteilung $P(s)$, bspw.\ ein Histogramm. Da
die mittlere Zustandsdichte $\overline{d}(E)$ jedoch meist von der
Energie abhängt, stellt dies ein Problem dar. Abhilfe schafft die
Normierung des Spektrums, bzw.\ Normierung der Linienabstände auf
einen konstanten mittleren Abstand. Dies wird auch als \frqq
Entfaltung des Spektrums\flqq \, bezeichnet.

\minisec{Entfaltung des Spektrums}
Anzahl der Zustände
\begin{align*}
  N(E) &= \sum_n \Theta(E-E_N) = \overline{N}(E) + N_f(E)
\end{align*}
mit $N_f(E)$, den fluktuierenden Anteilen und
\begin{align*}
  \overline{N}(E) &= \frac{1}{(2 \pi \hbar)^d} \int \diff^d q \diff^d p \,  \Theta(E-H(\bm{p},\bm{q}))
\end{align*}
im Phasenraum ($H\le E$). Siehe Abbildung~\ref{fig:2.7.15-1}.
\begin{figure}[tb]
  \centering
\begin{tikzpicture}[gfx]
  \begin{axis}[
    % xtick = \empty,
    % ytick = \empty,
    xlabel = $E$,
    ylabel = $N(E)$,
    xmin = 0,	xmax = 6,
    ymin = 0,	ymax = 3.5,
    width = 6cm,
    xtick = {0.5,1.5,2,2.5,3.5,5.5},
    xticklabels = {$E_0$,$E_1$,$E_2$,$E_3$,$E_4$,$E_5$},
    ytick = {0.25,0.75,1.25,1.75,2.25,2.75},
    yticklabels = {$x_0$,$x_1$,$x_2$,$x_3$,$x_4$,$x_5$},
    ]
    \addplot[smooth,MidnightBlue] plot coordinates {
      (0,0)
      (0.5,0.25)
      (1.5,0.75)
      (2,1.25)
      (2.5,1.75)
      (3.5,2.25)
      (5.5,2.75)
      (6,3.25)
    }; 
    \draw[DarkOrange3] (axis cs: 0,0) -| (axis cs: 0.5,0.5) -| (axis cs: 1.5,1) -| (axis cs: 2,1.5) -| (axis cs: 2.5,2) -| (axis cs: 3.5,2.5) -| (axis cs: 5.5,3) -| (axis cs: 6,3.5);
    \node [MidnightBlue] at (axis cs: 3,3) {$\overline{N}(E)$};
  \end{axis}
\end{tikzpicture}
\caption{Anzahl der Zustände über der Energie. In blau ist die
  mittlere Teilchenanzahl $\overline{N}(E)$ dargestellt. Der
  Schnittpunkt beider Funktionen bestimmt die neuen Levels (abzulesen
  an der $y$-Achse). Die Werte $x_i$ haben per Konstruktion im Mittel
  einen Abstand von $1$. }
  \label{fig:2.7.15-1}
\end{figure}
Die Entfaltung ist eine Abbildung $x_i = \overline{N}(E_i)$ mit
\begin{align*}
  \overline{\frac{\diff x_i}{\diff i}} &= \frac{\diff \overline{N}}{\diff E} \bigg|_{E_i} \cdot \overline{\frac{\diff E_i}{\diff i}} = \overline{d(E_i)} \cdot \frac{1}{\overline{d(E_i)}} = 1.
\end{align*}
Statistische Untersuchung der entfalteten Folge $x_i$.

\minisec{Nächste-Nachbar-Verteilung} 

Häufigkeitsverteilung: $P(s)$ für die Abstände $s_i = x_{i+1} - x_i$
liefert ein Histogramm, in dem wir verschieden starke Ausprägungen von
Niveauabstoßungen beobachten.

\minisec{Nieveauabstoßung als vermiedene Kreuzungen}

Wir betrachten den Energieabstand zwischen zwei Niveaus, beschrieben
durch eine hermitesche $2\times 2$ Matrix $H$
\begin{align*}
  H &=
      \begin{pmatrix}
        H_{11} & H_{12} \\
        H_{12}^{\ast} & H_{22}
      \end{pmatrix}.
\end{align*}
Das System kann von einem oder mehreren externen Kontrollparametern
abhängen (z.B.\ magnetische oder elektrische Felder, Seitenverhältnis
eines Rechteckbillard, \ldots). Die Eigenwerte sind
\begin{align*}
  E_{\pm} &= \frac{1}{2} \left(H_{11} + H_{22} \right) \pm \sqrt{\Delta}
\end{align*}
mit
\begin{align*}
  \Delta &= \frac{1}{4} \left(H_{11} - H_{22} \right)^2 + |H_{12}|^2.
\end{align*}
Mögliche Fälle:
\begin{enumerate}
\item Keine Wechselwirkung zwischen den Zuständen, d.h.\ $H_{12} = 0$.
  \begin{align*}
    E_{+} = H_{11}(\lambda), \quad E_{-} = H_{22}(\lambda),
  \end{align*}
  mit dem Kontrollparameter $\lambda$. Es kann eine exakte
  Niveaukreuzung auftreten (bei geeigneter Wahl eines
  Kontrollparameters)
\item $H_{12} \neq 0$, aber die Matrix ist reell. Niveaukreuzung falls
  \begin{align*}
    \Delta &= \frac{1}{4} (H_{11} + H_{22})^2 - H_{12}^2 \stackrel{!}{=} 0.
  \end{align*}
  Typischerweise nur möglich, wenn es $n=2$ Kontrollparameter gibt,
  bspw.\ $\lambda_1$ und $\lambda_2$.
\item $H_{12} \neq 0$ mit komplex hermitescher Matrix
  \begin{align*}
    \Delta = \frac{1}{4} (H_{11}+H_{22})^2 + (\mathrm{Re} \, H_{12})^2 + (\mathrm{Im} \, H_{12})^2 \stackrel{!}{=} 0.
  \end{align*}
  Erfordert $n=3$ Kontrollparameter, bspw.\ $\lambda_1$, $\lambda_2$
  und $\lambda_3$.
\end{enumerate}

\paragraph{Klassisch integrable Systeme:} EBK-Torusquantisierung
liefert einen vollständigen Satz von Quantenzahlen $\bm{n}$
\begin{align*}
  E_{\bm{n}}(\lambda) \approx H \left( \bm{I} = \hbar \left(\bm{n} + \frac{\bm{\alpha}}{4} \right),\lambda \right).
\end{align*}
Zustände sind durch den vollständigen Satz von Quantenzahlen $\bm{n}$
beschrieben. Typischerweise genügt die Variation eines
Kontrollparameters $\lambda$, um die Kreuzung zweier benachbarter
Niveaus zu erreichen (Fall (1)). Wir erwarten, dass bei klassisch
integrablen Systemen Niveaukreuzungen mit großer Wahrscheinlichkeit
auftreten.

\paragraph{Die Fälle (2) und (3):} Reell symmetrische, bzw.\ komplex
hermitesche Matrizen lassen sich mittels orthogonalen, bzw.\ unitären
Transformationen diagonalisieren. Erforderliche Anzahl an
Kontrollparameter für Niveaukreuzungen
\begin{align*}
  n &=
      \begin{cases}
        2 &\text{, orthogonal} \\
        3 &\text{, unitär} \\
        5 &\text{, symplektisch}
      \end{cases}
\end{align*}
Zeitumkehrinvarianz für $n=2$ und $n=5$: $T$, $T^2 = \mathds{1}$,
bzw.\ $T$, $T^2 = -\mathds{1}$. Der Grad der Niveauabstoßung ausgedrückt durch
die Verteilung $P(s)$ bei kleinen Abständen $s$ hängt von $n$ ab
\begin{align*}
  P(s) \propto s^{n-1} = s^\beta, \quad \text{für}\quad  s\to 0
\end{align*}
mit $\beta = n-1$.
\begin{proof}
  Es ist
  \begin{align*}
    P(s) &= \sum_i \delta(s- \Delta E_i) = \braket{\delta(s- \Delta E)},
  \end{align*}
  mit $\braket{\bullet}$ der Mittlung über alle $\Delta E$. Für kleine
  $s$ (gegenüber dem mittleren Abstand der Niveaus) ist $\Delta E$
  gerade
  \begin{align*}
    \Delta E &= \sqrt{\bm{x}^2} = (x_1^2 + x_2^2 + \ldots + x_n^2)^{1/2}.
  \end{align*}
  Mittelung über alle $\bm{x}$ (mit geeigneter Gewichtsfunktion
  $W(\bm{x})$)
  \begin{align*}
    P(s) &= \int \diff^n x W(\bm{x}) \delta\left(s-\sqrt{\bm{x}^2}\right).
  \end{align*}
  Substitution mit $\bm{x} = s \bm{y}$ ergibt
  \begin{align*}
    P(s) &= \int \diff^n(sy) W(s\bm{y}) \delta\left(s-s\sqrt{\bm{y}^2}\right) \\
    &= s^{n-1} \int \diff^n y W(s \bm{y}) \delta\left(1 - \sqrt{\bm{y}^2}\right),
  \end{align*}
  wobei
  \begin{align*}
    \lim\limits_{s \to 0} W(s \bm{y}) = W(0) \neq 0
  \end{align*}
  und somit 
  \begin{align*}
    \boxed{P(s) \propto  s^{\beta}}
  \end{align*}
  mit $\beta = n-1$.
\end{proof}
Somit erhalten wir also
\begin{align*}
  P(s) &\propto
         \begin{cases}
           s &\text{, orthogonal} \\
           s^2 &\text{, unitär} \\
           s^4 &\text{, symplektisch}
         \end{cases}
\end{align*}
für kleine $s$.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: