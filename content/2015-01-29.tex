Die neue Hamiltonfunktion lautet damit:
\begin{align*}
  \hat H(\bm{\hat{J}})
  &= H(\bm J(\bm{\hat{J}},\hat{\bm \theta}),\bm \theta(\bm{\hat{J}},\hat{\bm \theta})) \\
  &= H_0(\bm{\hat{J}})
  + \left. \frac{\partial H_0}{\partial \bm J} \right|_{\bm{\hat{J}}} (\bm J - \bm{\hat{J}})
  + \underbrace{\frac{\partial H_0}{\partial \bm \theta}}_{=0}
  + \varepsilon H_1(\bm{\hat{J}},\bm\theta) + \mathcal O(\varepsilon^2) \\
  &= H_0(\bm{\hat{J}})
  + \ii \varepsilon \sum_{\bm m} (\bm m \cdot \bm \omega(\bm{\hat{J}})) S_{\bm m}(\bm{\hat{J}}) \ee^{\ii\bm m\cdot \bm\theta}
  + \varepsilon \sum_{\bm m \neq 0} H_{1,\bm m}(\bm{\hat{J}}) \ee^{\ii\bm m\cdot\bm\theta}
  + \mathcal O(\varepsilon^2)
\end{align*}
Die Bedingung, damit $\hat H$ unabhängig von $\bm\theta$ ist, führt auf
\begin{align*}
  S_{\bm m}(\bm{\hat{J}}) &= \ii \frac{H_{1,\bm m}(\bm{\hat{J}})}{\bm m\cdot\bm\omega(\bm{\hat{J}})}
  \quad\text{für } \bm m\neq0\quad (S_0(\bm{\hat{J}}) = 0)
\intertext{somit}
  S(\bm{\hat{J}},\bm\theta) &= \bm{\hat{J}}\cdot\bm\theta
  + \ii\varepsilon\sum_{\bm m\neq0} \frac{H_{1,\bm m}(\bm{\hat{J}})}{\bm m\cdot\bm\omega(\bm{\hat{J}})} \ee^{\ii\bm m\cdot\bm\theta}
\end{align*}
ist die gesuchte Erzeugende für die kanonische Transformation.
\begin{equation*}
  H(\bm J,\bm\theta) = H_0(\bm{\hat{J}}) + \varepsilon H_1(\bm{\hat{J}},\theta)
  \to \hat H(\bm{\hat{J}}) + \mathcal O(\varepsilon^2)
\end{equation*}

\begin{notice}[Diskussion:]
  \begin{itemize}
  \item Die $\bm{\hat{J}}$ sind (neue) Erhaltungsgrößen des gestörten
    Systems (bis Ordnung $\varepsilon^2$).
  \item Die Störungstheorie lässt sich zu höheren Ordnungen in
    $\varepsilon$ fortsetzen.  Dabei stellt sich die Frage, ob dann
    noch alles integrabel ist.
  \item $\bm m\cdot\bm\omega=0$ entspricht rationalen
    Frequenzverhältnissen, also Resonanzen.  Die einzelnen Terme der
    Störungsreihe divergieren für resonante Tori.
  \end{itemize}
\end{notice}

Die entscheidende Frage lautet: Wann konvergiert die Störungsreihe für
einen Torus des Systems $H = H_0 + \varepsilon H_1$ bzw.\ welche Tori
\enquote{überleben} die Störung $\varepsilon H_1$ und werden nur
deformiert?

Die Antwort gibt das \acct{KAM-Theorem} (Kolmogorov 1954, Arnold 1963,
Moser 1967).  Eine qualitative Formulierung des Theorems wäre, dass
fast alle invarianten Tori eine endlich große Störung
\enquote{überleben} und nur deformiert werden.  \enquote{Fast alle
  Tori} sind hier diejenigen, die genügend irrational sind.

Kommen wir zur mathematischen Formulierung, also der Frage nach
lokaler Konvergenz der Störungstheorie für einen einzelnen Torus mit
Frequenz $\bm\omega$ des integrablen Systems $H_0$ beim Einschalten
einer endlichen (kleinen) Störung.

\begin{theorem}[KAM-Theorem (mathematische Formulierung)]
  Der invariante Torus des gestörten Systems $H = H_0 + \varepsilon H_1$
  mit denselben Frequenzen $\bm\omega$ (wie der ungestörte Torus)
  existiert, wenn die folgenden drei Bedingungen erfüllt sind.
  \begin{enumerate}[(B1)]
  \item\label{item:KAM-B1} Lineare Unabhängigkeit oder genügende
    Nichtlinearität der $\bm\omega(\bm J)$
    \begin{equation*}
      \det \frac{\partial \omega_i}{\partial J_j}
      = \det \frac{\partial^2 H_0}{\partial J_i \partial J_j}
      \neq 0
    \end{equation*}
  \item\label{item:KAM-B2} Die Störung $H_1(\bm J,\bm \theta)$ ist
    eine genügend glatte Funktion von $\bm J$ und $\bm\theta$.
  \item\label{item:KAM-B3} Die Frequenzen $\bm\omega$ erfüllen die
    Bedingung der genügenden Irrationalität, d.h.\ für alle $\bm
    m\neq0$ gilt
    \begin{equation*}
      |\bm m\cdot\bm\omega| \geq \frac{K(\varepsilon,\bm\omega,\sigma)}{|m|^{N-1+\sigma}}
      \quad\text{mit } |m| = \sum_i |m_i|
    \end{equation*}
    und $\sigma > 0$ und einer Konstante
    $K(\varepsilon,\bm\omega,\sigma)$ wobei $K \to 0$ für $\varepsilon
    \to 0$.
  \end{enumerate}
\end{theorem}

Lesweise (B3): Für jeden irrationalen Torus ($\bm
m\cdot\bm\omega\neq 0$ für alle $\bm m\neq0$) gibt es ein endliches
$\varepsilon > 0$ für das der Torus die Störung $\varepsilon H_1$
\enquote{überlebt}.  Bei zunehmender Stärke der Störung
($K(\varepsilon,\bm\omega,\sigma)$ wächst) wird der Torus jedoch in
der Regel zerstört.

Das KAM-Theorem sagt nichts über die rationalen Tori, diese können im
Prinzip durch beliebig kleine Störungen zerstört werden.

\begin{proof}[Beweisskizze]
  Setze
  \begin{equation*}
    \bm p = \bm J - \bm J_0 \;,\quad
    \bm q = \bm\theta \;,\quad
    \bm\omega = \frac{\partial H_0}{\partial \bm J} \;,\quad
    h_{ij} = \frac{\partial^2 H_0}{\partial J_i \partial J_j}
  \end{equation*}
  Entwicklung der ungestörten Hamiltonfunktion in der Umgebung von
  $\bm J_0$ (Störungstheorie in der lokalen Umgebung des Torus mit
  Wirkungsvariable $\bm{J}_0$ und Frequenzen $\bm \omega$):
  \begin{equation*}
    H_0 = \underbrace{H_0(\bm J_0)}_{\equiv n} + \bm\omega\cdot\bm p
    + \frac12 \sum_{i,j=1}^N h_{ij} p_i p_j + \mathcal O(p^3)
  \end{equation*}
  Frage: Gibt es kanonische Transformationen $(q,p) \to (Q,P)$, sodass
  das gestörte System
  \begin{equation}
    \label{eq:14.1}
    H = n + \bm\omega\cdot\bm p + \frac12 \sum_{i,j=1}^N h_{ij} p_i p_j
    + \varepsilon \underbrace{\left\{ A(\bm q) + \sum_{\ell=1}^N B_\ell(\bm q) p_\ell \right\}}_{\text{Störung}}
    + \mathcal O(p^3,\varepsilon p^2,\ldots)
  \end{equation}
  in der Form
  \begin{equation}
    \label{eq:14.2}
    H = M(\varepsilon) + \bm\omega\cdot\bm P + \mathcal O(\bm p^2, \varepsilon^2,\varepsilon \bm p)
    \implies
    \dot{\bm Q} = \frac{\partial H}{\partial \bm P} = \bm\omega
    \implies
    \bm Q(t) = \bm\omega t
  \end{equation}
  geschrieben werden kann. Wir sehen, dass H mit denselben Frequenzen
  $\bm\omega$ wie in $H_0$ erscheint und $M(\varepsilon)$ einer von
  $\varepsilon$ nicht aber $(\bm Q,\bm P)$ abhängigen Konstanten.

  Ansatz für kanonische Transformation:
  \begin{equation*}
    F(\bm q,\bm P) = \bm q\cdot\bm P + \varepsilon \{ \bm P\cdot\bm Y(\bm q)
    + \bm\xi\cdot\bm q + X(\bm q) \}
  \end{equation*}
  Damit gilt
  \begin{align*}
    Q_i &= q_i + \varepsilon Y_i(\bm q), \\
    p_i &= P_i + \varepsilon \left\{ \sum_\ell P_\ell \frac{\partial Y_\ell}{\partial q_i} + \xi_i + \frac{\partial X}{\partial q_i} \right\}.
  \end{align*}
  Einsetzen von $\bm p$ in \eqref{eq:14.1} führt auf
  \begin{multline*}
    H = \bm\omega\cdot\bm P + \underbrace{n +
      \varepsilon \left[ A(\bm q) + \sum_\ell \omega_\ell \left( \xi_\ell +
          \frac{\partial X}{\partial q_\ell} \right) \right]}_{\text{vgl.\
        \eqref{eq:14.2}: $M(\varepsilon)$}} \\
    + \varepsilon \sum_\ell P_\ell \underbrace{\left\{
        B_\ell(\bm q) + \sum_k \omega_k \frac{\partial Y_\ell}{\partial q_k} +
        \sum_k h_{\ell k} \left( \xi_k + \frac{\partial X}{\partial q_k}
        \right) \right\}}_{\text{vgl.\ \eqref{eq:14.2}: $0$}} + \mathcal O(P^2,
    \varepsilon^2).
  \end{multline*}
  Also besitzt das gestörte System $H$ einen Torus mit den selben
  Frequenzen $\bm\omega$ wie das ungestörte System $H_0$, wenn
  \begin{align*}
    A(\bm q) + \sum_\ell \omega_\ell \left( \xi_\ell + \frac{\partial
        X}{\partial q_\ell} \right) = \zeta &= \frac{M(\varepsilon) -
      n}{\varepsilon}, \\
    B_\ell(\bm q) + \sum_k h_{\ell k} \xi_k + \underbrace{\sum_k
      h_{\ell k} \frac{\partial X}{\partial q_k}}_{Z_\ell(\bm q)}
    + \sum_k \omega_k \frac{\partial Y_\ell}{\partial q_k} &= 0
  \end{align*}
  %
  % Ab hier 05.02.2014
  %
  Im nächsten Schritt folgt eine Fourierentwicklung aller von $\bm q =
  \bm\theta$ abhängigen Funktionen.
  \begin{equation*}
    \begin{aligned}
      A(\bm q) = \sum_{\bm m} a(\bm m) \ee^{\ii\bm m\cdot\bm q} \\
      B_\ell(\bm q) = \sum_{\bm m} b_\ell(\bm m) \ee^{\ii\bm m\cdot\bm q} \\
      X(\bm q) = \sum_{\bm m} x(\bm m) \ee^{\ii\bm m\cdot\bm q} \\
      Y_\ell(\bm q) = \sum_{\bm m} y_\ell(\bm m) \ee^{\ii\bm m\cdot\bm q} \\
      Z_\ell(\bm q) = \sum_{\bm m} z_\ell(\bm m) \ee^{\ii\bm m\cdot\bm q}
    \end{aligned}
    \quad\text{mit}\quad \bm m\in\mathbb Z^N
  \end{equation*}
  Damit erhalten wir das Gleichungssystem
  \begin{subequations}
    \begin{align}
      \label{eq:14.3a}
      a(0) + \sum_\ell \omega_\ell \xi_\ell &= \zeta, \\
      \label{eq:14.3b}
      a(\bm m) + \ii(\bm m\cdot\bm\omega) x(\bm m) &= 0 \;,\quad \bm m\neq0, \\
      \label{eq:14.3c}
      b_\ell(0) + \sum_k h_ {\ell k} \xi_k + z_\ell(0) &= 0, \\
      \label{eq:14.3d}
      b_\ell(\bm m) + z_\ell(\bm m) + \ii(\bm m\cdot\bm\omega) y_\ell(\bm m) &= 0 \;,\quad \bm m\neq0.
    \end{align}
  \end{subequations}
  Man erhält $h_{\ell k}$ aus $H_0$, die Konstanten $\xi_k$ und
  $\zeta$ sind noch zu bestimmen.  Die Koeffizienten $a(\bm m)$,
  $b_\ell(\bm m)$ erhält man aus $H_1$.  Schließlich sollen $x(\bm
  m)$, $z_\ell(\bm m)$ und $y_\ell(\bm m)$ konvergent sein.

  Zur Bedingung (B1): $\det h_{\ell k}\neq 0$ ist
  erforderlich zur Bestimmung der Konstanten $\xi_k$ aus Gleichung
  \eqref{eq:14.3c}, d.h.\ es ist ein lineares Gleichungssystem zu
  lösen.

  Zu den Bedingungen (B2) und (B3): Die
  genügende Glattheit von $H_1$ ist notwendig um zu garantieren, dass
  $a(\bm m)$ und $b_\ell(\bm m)$ hinreichend schnell abnehmen, wenn
  $|\bm m|\to\infty$, sodass man für $X(\bm q)$, $Z_\ell(\bm q)$ und
  $Y_\ell(\bm q)$ konvergente Fourierreihen erhält.


  Konkret bedeutet das, wenn $A(\bm q)$ gerade $R$ stetige Ableitungen
  besitzt, dann (ohne Beweis) $a(\bm m) \to |\bm m|^{-(N+R+1)}$ für
  $|\bm m| \to \infty$.

  Aus der Gleichung \eqref{eq:14.3b} folgt
  \begin{equation*}
    |x(\bm m)| = \left| \frac{\ii a(\bm m)}{\bm m\cdot\bm\omega} \right|
    \xrightarrow{|\bm m|\to\infty} \frac{|\bm m|^{-(N+R+1)}}{|\bm m\cdot\bm\omega|}
    \stackrel{\eqref{eq:14.3b}}{\leq} \frac{|\bm m|^{\sigma-R-2}}{K(\varepsilon,\bm\omega,\sigma)}
  \end{equation*}
  Die Reihe ist für $X(\bm q)$ konvergent, wenn
  \begin{equation*}
    \boxed{R \geq N-1+\sigma} \quad\text{($X$ stetig)}
  \end{equation*}
  Notwendige Bedingung an die Glattheit der Störung.
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End:
