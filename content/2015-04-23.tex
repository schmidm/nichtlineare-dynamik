\begin{example}
  Als Beispiel für die Stabilitätsmatrix dient das Wasserstoffatom im
  Magnetfeld.  Der Hamiltonoperator ist gegeben durch
  \begin{equation*}
    H = \frac12 (p_\mu^2 + p_\nu^2) - E (\mu^2 + \nu^2)
    + \frac18 \mu^2 \nu^2 (\mu^2 + \nu^2) = 2 \; .
  \end{equation*}
  Die Bewegungsgleichungen ergeben sich in der symplektischen
  Formulierung aus
  \begin{equation*}
    \bm\gamma = (\mu,\nu,p_\mu,p_\nu)
    \;,\quad
    \dot{\bm\gamma} = \mathscr{J} \frac{\partial H}{\partial \bm\gamma} \; .
  \end{equation*}
  Die Stabilitätsmatrix lässt sich mittels
  \begin{equation*}
     \frac{\diff}{\diff t} \mathscr{M} = \mathscr{J} \frac{\partial^2 H}{\partial \bm\gamma \partial \bm\gamma} \mathscr{M}
    \; , \quad
    \mathscr{M}(0) = \mathds{1}_{4\times 4} \; .
  \end{equation*}
  berechnen. Da $\mathscr{M}$ symplektisch ist treten die Eigenwerte
  in Paaren $(\lambda_i,1/\lambda_i)$ auf.
\end{example}

\minisec{Bifurkationen periodischer Bahnen, Normalform- und Katastrophentheorie}

Betrachten wir die Linearisierung der Poincaré-Abbildung in der
Umgebung der Fixpunkte.  Die Fixpunkte entsprechen gerade den
periodischen Bahnen.  Für $N=2$ Freiheitsgrade erhalten wir dann
\begin{equation*}
  \begin{pmatrix}
    q_{n+1} \\
    p_{n+1} \\
  \end{pmatrix}
  =
  \underbrace{
  \begin{bmatrix}
    m_{11} & m_{12} \\
    m_{21} & m_{22} \\
  \end{bmatrix}
  }_{\mathscr{M}}
  \begin{pmatrix}
    q_n \\
    p_n \\
  \end{pmatrix}
\end{equation*}
mit der smyplektischen Monodromiematrix $\mathscr{M}$.  Der Fixpunkt
liegt bei $(0,0)$.  Für das charakteristische Polynom der
Monodromiematrix gilt
\begin{align*}
  \chi(\lambda) &= \lambda^2 - (\tr\mathscr{M}) \lambda + \det\mathscr{M} \\
  &=  \lambda^2 - (\tr\mathscr{M}) \lambda + 1 \stackrel{!}{=} 0 \, .
\end{align*}
Für die Eigenwerte der Monodromiematrix können verschiedene Fälle
auftreten:
\begin{enumerate}
\item $\lambda_{1,2} = \pm \ee^{\pm u}$ mit $u>0$.  (Invers)
  hyperbolischer Fixpunkt.
  \begin{equation*}
    \pm \ee^u + \ee^{-u} = \pm (2 \cosh u)
    \implies
    \Sp\mathscr{M} =
    \begin{cases}
      > 2 \\
      < -2 \\
    \end{cases}
  \end{equation*}
\item $\lambda_{1,2} = \ee^{\pm\ii\varphi}$ mit $0<\varphi<\pi$.
  Elliptischer Fixpunkt.
  \begin{equation*}
    \ee^{\ii\varphi} + \ee^{-\ii\varphi} = 2 \cos\varphi
    \implies
    |\Sp\mathscr{M}| < 2
  \end{equation*}
\item $\lambda_{1,2} = \pm 1$ mit $|\tr \mathscr{M}| =
  2$. Parabolischer Fixpunkt, der als marginal stabil bezeichnet wird.
\end{enumerate}

\begin{theorem}[Definition]
  Die Windungszahl der Bahn ist definiert als
  \begin{equation*}
    \frac{\varphi}{2 \pi} \; .
  \end{equation*}
\end{theorem}

Sei $\varphi$ ein rationales Vielfaches von $\pi$, also $\varphi = \pi
n/m$.  Die Monodromiematrix für die Bahn nach $m$ Umläufen ist
$\mathscr{M}^m$, d.h.\ die Eigenwerte sind $\lambda_1 = \lambda_2 =
\pm 1$.  Es gibt also zwei Möglichkeiten für die Wahl der Matrix: $\mathscr{M}^m = \mathds{1}$ oder
\begin{tikzpicture}[baseline=(X.base),x=1.2em,y=.8ex]
  \draw (-2,0) -- (2,0) node[above right] (X) {Fixpunkte};
  \foreach \i in {-3,-2,-1,1,2,3} {
    \draw[->] (\i/2,\i) -- (0,\i);
  }
\end{tikzpicture}.
\begin{equation*}
  \begin{bmatrix}
    1 & a \\
    0 & 1 \\
  \end{bmatrix}
  \begin{pmatrix}
    0 \\
    \varepsilon \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    a \varepsilon \\
    \varepsilon \\
  \end{pmatrix}
  \; .
\end{equation*}

In diesem Fall, also dem einer neutralen Bahn reicht eine
Linearisierung der Map (Poincaré-Abbildung) nicht mehr zur
Beschreibung der Strukturen in der Umgebung der Fixpunkte aus.  Es ist
erforderlich höhere Terme in der Taylorentwicklung der Abbildung
mitzunehmen.  Dies führt uns auf die Normalformen.  Sehen wir uns
zunächst die Standardform an.  Der Ausdruck
\begin{equation*}
  p^2 + a q^2 = \const
\end{equation*}
liefert uns Ellipsen für $a>0$ und Hyperbeln für $a<0$.

Sei $\varepsilon$ ein Kontrollparameter des Systems derart, dass die
Resonanz bei $\varepsilon = 0$ auftritt:
\begin{equation*}
  \varphi(\varepsilon = 0) = \pi \frac{n}{m}
  \implies
  \lambda_{1,2}^m(\varepsilon = 0) = \pm 1 \; .
\end{equation*}
Für das Wasserstoffatom wählt man z.B.\ $\varepsilon = E - E_b$ mit
der Bifurkationsenergie $E_b$.  In Normalform gilt
\begin{equation*}
  p^2 + \varepsilon q^2
\end{equation*}
und daher Entartung bei $\varepsilon = 0$.  Eine Berücksichtigung der
nächst höheren Terme liefert, z.B.\
\begin{gather*}
  p^2 + \varepsilon q^2 + q^3 \\
  p^2 + \varepsilon q^2 + q^4
\end{gather*}

Die Zahl der Fixpunkte ändert sich beim Nulldurchgang unseres
Kontrollparameters $\varepsilon$.  Dies bezeichnet man als
\acct[Bifurkation]{Bifurkationen}.  Damit bezeichnet man die
Entstehung oder das Verschwinden periodischer Bahnen.  Die Fixpunkte
entstehen und verschwinden immer paarweise.  Diese Bifurkationen
können in der Normalformtheorie ($N=2$, Bahnen ohne besondere
Symmetrien) klassifiziert werden.

\begin{table}[tb]
  \centering
  \begin{tabular}{rl}
    \toprule
    $m$ & Bifurkationstyp \\
    \midrule
    $1$ & Sattel-Knoten-Bifurkation \\
    $2$ & Periodenverdopplung \\
    $3$, $4$ &  \enquote{Touch and go}-Bifurkation \\
    $\geq 5$ & \enquote{Five island chain}- bzw.\ \enquote{$m$ island chain}-Bifurkation \\
    \bottomrule
  \end{tabular}
  \caption{Einige Bifurkationstypen}
  \label{tab:2015-04-23-1}
\end{table}

\begin{example}
  Als Beispiel dient wieder einmal das Wasserstoffatom im Magnetfeld.
  Bei der Bifurkationsenergie von $\tilde E_b = \num{-0.011544916}$
  existiert eine Sattel-Knoten-Bifurkation. Dies entspricht $m=1$.
\end{example}

Für eine Trajektorienschar in der Nähe der Bifurkation lassen sich die
folgenden Fälle unterscheiden.
\begin{enumerate}
\item Sattel-Knoten-Bifurkation $\leftrightarrow$ Fold-Katastrophe
\item Periodenverdopplung $\leftrightarrow$ Cusp-Katastrophe
\item Four-island-chain $\leftrightarrow$ Butterfly-Katastrophe
\end{enumerate}
Eine systematische Untersuchung und Klassifikation der geometrischen
Strukturen nennt man Katastrophentheorie.  (Thoms Klassifikation der
elementaren Katastrophen).  Drei der sieben elementaren Katastrophen
sind Fold, Cusp und Butterfly neben swallow tail, elliptic/hyperbolic
und parabolic umbilic.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: