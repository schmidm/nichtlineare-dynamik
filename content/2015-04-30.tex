\section{Semiklassische Theorien}

Wir beschäftigen uns zunächst mit der WKB-Quantisierung
eindimensionaler Systeme, danach mit der semiklassichen Quantisierung
mehrdimensionaler separabler bzw.\ integrabler Systeme.

\subsection{WKB Methode}
Bei der WKB-Methode (nach Wenzel, Kramer und Brillouin) gehen wir von
der Schrödingergleichung für ein eindimensionales System mit
Potential $V(x)$ aus.
\begin{equation*}
  \psi'' + \frac{2m}{\hbar^2} \bigl( E-V(x) \bigr) \psi = 0 \; .
\end{equation*}
Wir machen einen Ansatz, bei dem wir in Amplitude und Phase separieren
\begin{equation*}
  \psi(x) = A(x)\,\ee^{\ii S(x)/\hbar} = \ee^{\ii\omega(x)/\hbar}
\end{equation*}
mit $\omega(x) = S(x) + \frac\hbar\ii \ln A(x)$.  Setzen wir diesen
Ansatz ein, so ergibt sich
\begin{alignat*}{3}
  \text{(I)}  &\quad& S'^2 - 2 m \bigl( E-V(x) \bigr) &=
  \underbrace{\hbar^2 \frac{A''}{A}}_{(\ast)} \\
  \text{(II)} &\quad& 2 A' S' + A S'' &= 0
\end{alignat*}
Was passiert wenn $(\ast) = 0$?  Die Lösung für (II), welche eine
Kontinuitätsgleichung darstellt, ist
\begin{equation*}
  A = c (S')^{-1/2} \; .
\end{equation*}
Einsetzen in (I) liefert
\begin{equation*}
  S'^2 = 2 m (E-V) + \hbar^2 \left[
    \frac34 \left( \frac{S''}{S'} \right)^2
    - \frac12 \frac{S'''}{S'}
  \right] \; .
\end{equation*}
Diesen Ausdruck entwickeln wir nach $\hbar$
\begin{align*}
  S &= S_0 + \hbar^2 S_1 + \ldots \\
  S \approx S_0 : \quad
  S_0'^2 &= 2 m (E-V) \qquad \text{Hamilton-Jacobi-DGL}\\
  \implies
  S_0(x) &= \pm \int \sqrt{2m(E-V(x))} \diff x \\
  &= \pm \int p \diff x \, .
\end{align*}
Der Ausdruck unter der Wurzel sollte positiv sein.  Daher nennt man
$V(x) < E$ den klassisch erlaubten, $V(x) > E$ den klassisch
verbotenen Bereich.  Das Problem ist, dass $A\sim(S')^{-1/2}$ für
$S'=0$ divergiert, also an den klassischen Umkehrpunkten $p=0$. Daher
linearisieren wir das Potential am Umkehrpunkt, bspw.\ $x=a$

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[no marks,samples=51,smooth,xlabel=$x$,ylabel=$V(x)$,
      xtick={-1.5708,1.5708},xticklabels={$b$,$a$},ytick=\empty,
      enlargelimits=false,xmin=-pi,xmax=pi,ymin=-1.1,ymax=1.1]
      \addplot[MidnightBlue,domain=-pi:pi] {-cos(deg(x))};
      \addplot[DarkOrange3,domain=-pi/2:pi/2] {.1*cos(deg(3*pi*x))};
      \addplot[Purple,domain=-pi:-pi/2-.05] {-.1/(x+pi/2)};
      \addplot[Purple,domain=pi/2+.05:pi]   { .1/(x-pi/2)};
      \addplot[DarkGreen,domain=0:pi] {x-1.5708};
    \end{axis}
  \end{tikzpicture}
  \caption{Das Problem ist die Anpassung der Wellenfunktion.  Die
    Lösung liegt in der Linearisierung des Potential an den
    Umkehrpunkten, z.B.\ $x=a$.}
  \label{fig:2015-04-30-1}
\end{figure}

\begin{equation*}
  E-V(x) \approx F_0(x-a)
  \implies
  p(x) = \sqrt{2mF_0(x-a)},
\end{equation*}
wobei $F_0 \equiv -\partial V / \partial x |_{x=a}$. Setzen wir ohne
Beschränkung der Allgemeinheit $a=0$ und erhalten
\begin{equation*}
  \psi'' + \frac{2mF_0}{\hbar^2} x \psi = 0.
\end{equation*}
Die Lösung dieser Schrödingergleichung ist bekannt, denn sie
entspricht der Airy-DGL.  In der WKB-Näherung:
\begin{equation*}
  \int p(x) \diff x
  = \int \sqrt{2 m F_0 x} \diff x
  = \frac23 \sqrt{2mF_0} x^{3/2} +\alpha
\end{equation*}
Im klassisch erlaubten Bereich $x>0$ ergibt sich
\begin{equation*}
  \psi(x) \sim A(x) \cos\left[
    \frac23 \frac{\sqrt{2mF_0}}{\hbar} x^{3/2} - \alpha \right] \; .
\end{equation*}
IM Vergleich mit der Airy-Funktion finden wir
\begin{equation*}
  \psi(x) = c \Ai\left[ -\left(\frac{2mF_0}{\hbar}\right)^{1/3} x \right] \; .
\end{equation*}
Dies liefert im Limes $x \to -\infty$ die asymptotische Lösung
\begin{equation*}
  \psi(x) = \frac{c'}{(-x)^{1/4}} \cos\left[
    \frac23 \frac{\sqrt{2mF_0}}{\hbar} x^{3/2} - \frac\pi4 \right] \; .
\end{equation*}
Folglich muss $\alpha = \pi/4$ sein. Innerhalb von den Umkehrpunkten
$b<x<a$ (klassische Zone) müssen die Wellenfunktionen aufgrund der
Eindeutigkeit übereinstimmen (vgl.\ Anschluss- und
Stetigkeitsbedingung)
\begin{gather*}
  \begin{aligned}
    \psi(x)
    &\sim \cos\left( \frac1\hbar \int_x^a p \diff x - \frac\pi4 \right) \\
    &\stackrel!\sim \cos\left( \frac1\hbar \int_b^x p \diff x - \frac\pi4 \right)
  \end{aligned} \\
  \implies
  \frac1\hbar \int_b^a p \diff x - \frac\pi2 = n \pi \\
  \boxed{
    \int_b^a p \diff x = \pi \hbar \left( n + \frac12 \right)
    \; , \quad 0,1,2,3,\ldots
  }
\end{gather*}
Dies ist die \acct{WKB-Quantisierungsbedingung} für gebundene
Zustände.
\begin{notice}
  Um auf die Quantisierungbedingung zu kommen, bedarf es einer kleinen
  Rechnung, in der wir zunächst die Integrale umschreiben
  \begin{align*}
    \underbrace{\int_{x}^{a} p(x) \diff x}_{=S_1} &= \underbrace{\int_{b}^{a} p(x) \diff x}_{\equiv S/2} - \underbrace{\int_{b}^{x} p(x) \diff x}_{=S_2} \\
    \implies S_1 &= \frac{S}{2} - S_2.
  \end{align*}
  Anschließend können wir den Kosinus der Wellenfunktion umschreiben,
  d.h.\
  \begin{align*}
     \cos\left( \frac1\hbar \int_x^a p \diff x - \frac\pi4 \right) &=  \cos\left( \frac{S_1}{\hbar}- \frac\pi4 \right)  \\
    &=  \cos\left( \left(\frac{S}{2 \hbar} - \frac{\pi}{2}\right) - \left( \frac{S_2}{\hbar} - \frac{\pi}{4} \right) \right) \\
    &= \cos \left( \frac{S}{2\hbar} - \frac{\pi}{2} \right) \cos \left( \frac{S_2}{\hbar} - \frac{\pi}{4} \right) + \sin \left( \frac{S}{2\hbar} - \frac{\pi}{2} \right) \sin \left( \frac{S_2}{\hbar} - \frac{\pi}{4} \right).
  \end{align*}
  Da dies mit dem Term
  \begin{align*}
    \cos\left( \frac{S_2}{\hbar} - \frac{\pi}{4} \right)
  \end{align*}
  für alle Werte übereinstimmen muss, müssen die Sinus-Terme
  verschwinden und der Kosinus-Term gleich $1$ sein, was ja gerade der
  Fall ist, wenn dessen Argumente die Werte $\pi n$ mit
  $n \in \mathbb{N}_0$ annehmen. Daraus folgt unmittelbar die
  Quantisierungsbedingung $S/(2\hbar) - \pi/2 = \pi n$.
\end{notice}

\subsection{Torusquantisierung}
Die WKB-Methode ist jedoch nicht anwendbar auf Systeme mit zwei oder
mehr Freiheitsgraden.  Deshalb widmen wir uns einer alternativen
Methode, die \acct{Torusquantisierung}.

\subsubsection{Im eindimensinalem Fall}

Betrachte dazu einen Oszillator im Phasenraum (Vibration, Libration).
Die Hamiltonfunktion lautet
\begin{equation*}
  H(p,q) = \frac{p^2}{2m} + V(q)
\end{equation*}
mit den Bewegungsgleichungen
\begin{equation*}
  \dot q = \frac{\partial H}{\partial p}
  \; , \quad
  \dot p = - \frac{\partial H}{\partial q}
\end{equation*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[no marks,samples=51,smooth,xlabel=$q$,ylabel=$p$,
      xtick=\empty,ytick=\empty,
      enlargelimits=false,xmin=-1.2,xmax=1.2,ymin=-1.2,ymax=1.2]
      \addplot[name path=a,MidnightBlue,domain=0:2*pi+.1] ({cos(deg(x))},{sin(deg(x))+.025*rand});
      \draw[name path=x,help lines] (axis cs:-1.2,0) -- (axis cs:1.2,0);
      \draw[name path=y,help lines] (axis cs:0,-1.2) -- (axis cs:0,1.2);
      \draw[DarkOrange3,name intersections={of=a and x,name=i}]
      node[dot,label={above left:A}] at (i-1) {}
      node[dot,label={above right:B}] at (i-2) {};
      \draw[DarkOrange3,name intersections={of=a and y,name=i}]
      node[dot,label={below left:C}] at (i-1) {}
      node[dot,label={above right:D}] at (i-2) {};
    \end{axis}
  \end{tikzpicture}
  \caption{Phasenportrait eines Oszillators mit den Punkten A und B
    der $q$-Umkehr und den Punkten C und D der $p$-Umkehr.}
  \label{fig:2015-04-30-2}
\end{figure}

Die Hamilton-Jacobi-Gleichung des Systems lautet (vgl. WKB-Methode mit
$\hbar \to 0$)
\begin{equation*}
  H\left(q,\frac{\diff \bar S}{\diff q}\right) - E = 0 \; .
\end{equation*}
Eine Lösung $S(q)$ kann nur bis zu den Stellen bei A und B verwendet
werden (siehe Abbildung~\ref{fig:2015-04-30-2}), an denen
Singulartitäten/Kaustiken auftreten.  Die kanonische Transformation
vom Ortsraum in den Impulsraum ist definiert über
\begin{gather*}
  \bar S(p) = S(q) - p q
  \; , \quad
  q = - \frac{\diff \bar S}{\diff p} \\
  H\left(-\frac{\diff S}{\diff p}, p\right) - E = 0 \; .
\end{gather*}
Diese so transformierte Lösung $\bar S(p)$ ist singulär bei C und
D. Somit starten wir im Zweig ADB und verfolgen $S(q)$ bis kurz vor
$C$ und wechseln dann in den Impulsraum. Anschließend verfolgen wir
$\bar S(p)$ über $C$ hinaus und wechseln dann in den Ortsraum
zurück. Dieses Spiel wiederholen wir für einen ganzen Umlauf.

\minisec{Maslov-Index}

\begin{equation*}
  \sign\left(\frac{\diff p}{\diff q}\right) =
  \begin{cases}
    +1 & \frac{\diff p}{\diff q} > 0 \\
    -1 & \frac{\diff p}{\diff q} < 0
  \end{cases}
\end{equation*}
Für die Maslov-Index-Funktion gilt
\begin{itemize}
\item $\sigma_q(x)$ ist eine ganze Zahl auf jedem $q$-Zweig.
\item $\sigma_p(x)$ ist eine ganze Zahl auf jedem $p$-Zweig.
\item $\sigma_p(x) = \sigma_q(x) - \sign\left(\frac{\diff p}{\diff
      q}\right)$.
\end{itemize}
Die Maslov-Index-Funktion dient zur Buchführung über die Phasen der
Wellenfunktion beim Bildwechsel. Der \acct{Maslov-Index} ist
\begin{equation*}
  \alpha \equiv \frac{[\sigma]}{2}
\end{equation*}
wobei $[\sigma]$ die Änderung von $\sigma$ in einem Umlauf darstellt.
Für einen Oszillator (Vibration, Libration) gilt
\begin{equation*}
  [\sigma] = 4 \; , \quad \alpha = 2  \;
\end{equation*}
und im Fall der Rotation
\begin{equation*}
  [\sigma] = 0 \; , \quad \alpha = 0 \; .
\end{equation*}
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[no marks,samples=31,smooth,xlabel=$q$,ylabel=$p$,
      xtick=\empty,ytick=\empty,
      enlargelimits=false,xmin=0,xmax=6*pi,ymin=-1.2,ymax=1.2]
      \addplot[MidnightBlue,domain=0:6*pi] {sin(deg(x))};
      \pgfplotsinvokeforeach{0,1,2}{
        \node[DarkOrange3,dot,label={[DarkOrange3]above:D}] at (axis cs:{3/2*pi+#1*2*pi},-1) {};
        \node[DarkOrange3,dot,label={[DarkOrange3]below:C}] at (axis cs:{1/2*pi+#1*2*pi},1) {};
      }
    \end{axis}
  \end{tikzpicture}
  \caption{Für eine Rotation ist $[\sigma] = 0$ und damit auch
    $\alpha=0$.}
  \label{fig:2015-04-30-3}
\end{figure}

Definiere die lokale semiklassische Wellenfunktion
\begin{equation*}
  \psi(q,\mathcal R) =
  \begin{cases}
    B(q) \, \ee^{\ii [S(q)/\hbar - \sigma_q \pi/4]} & q \in \mathcal R \\
    0 & \text{sonst}
  \end{cases}
\end{equation*}
Hierbei entspricht $\mathcal R$ einem begrenzten Bereich im $q$-Raum
(z.B.\ ein Zweig).  Transformation in den Impulsraum erfolgt durch
Fourier-Transformation.
\begin{equation*}
  \bar\psi(p,\bar{\mathcal R}) \approx
  \frac{1}{\sqrt{2 \pi\hbar}} \int_{\mathcal R} \diff q \;
  B(q) \, \ee^{\ii [S(q)/\hbar - \sigma_q \pi/4 - q p/\hbar]}
\end{equation*}
Der Integrand oszilliert stark in $q$ außer wenn $p = \diff S/\diff q$
(stationäre Phase).  Berechnen wir also das Integral in stationärer
Phasenapproximation.
\begin{gather}
  \notag
  S \approx S_0 + \underbrace{\frac{\diff S}{\diff q}}_{p} (q-q_0)
  + \frac12 \frac{\diff^2 S}{\diff q^2} (q-q_0)^2 \\
  \tag{$\ast$}
  \bar\psi(p,\bar{\mathcal R}) =
  \begin{cases}
    \bar B(p) \, \ee^{\ii [\bar S(p)/\hbar - \sigma_p \pi/4]} & p \in \bar{\mathcal R} \\
    0 & \text{sonst}
  \end{cases}
\end{gather}
mit
\begin{equation*}
  \bar S(p) = S(q) - pq
  \; , \quad
  \bar B(p) = \frac{B(q)}{\sqrt{\left|\frac{\diff p}{\diff q}\right|}}
  \; , \quad
  \sigma_p = \sigma_q - \sign\left(\frac{\diff p}{\diff q}\right)
\end{equation*}
Somit betrachten wir die Wellenfunktion als Funktion der
Phasenraumpunkte $\bm{x}$ der klassischen Kurven $\mathcal{C}$. Im
Falle eines Torus sind diese in Abbildung~\ref{fig:7.5.15-9}
dargestellt. Zusammensetzen der semiklassischen Wellenfunktion einer
klassischen Kurve $\mathcal{C}$ erfolgt gemäß:
\begin{enumerate}
\item $\psi_q(x) = \psi(q,\mathcal R)$ auf dem $q$-Zweig
\item $\psi_p(x) = \bar\psi(p,\bar{\mathcal R})$ auf dem $p$-Zweig
\item $\psi$, $\bar\psi$ transformieren gemäß $(\ast)$
\item Die geschlossenen Kurven $\psi_q(x)$ und $\psi_p(x)$ müssen
  eindeutig sein und liefern die globale Wellenfunktion.  Daraus
  erhalten wir die Quantisierungsbedingung.
  \begin{equation*}
    \frac{[S_q]}{\hbar} - [\sigma_q] \frac{\pi}{4} = 2 \pi n
  \end{equation*}
\end{enumerate}

Definiere die Funktion
\begin{equation*}
  \boxed{
    I \equiv \frac{[S_q]}{2\pi} = \left( n + \frac{\alpha}{4} \right) \hbar
    \; , \quad
    \alpha =
    \begin{cases}
      0 & \text{Rotation} \\
      2 & \text{Vibration (Libration)}
    \end{cases}
    \; , \quad
    n = 0,1,2,\ldots
  }
\end{equation*}
was gerade der \acct{Torusquantisierung} entspricht.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,scale=1]
  \begin{axis}[
    hide axis,
    view={60}{30},
    axis equal image,
    ]
    \addplot3[
    surf,% shader=interp,
    point meta=x,
    colormap={custom}{color(0)=(MidnightBlue) color(2)=(white) color(4)=(MidnightBlue)},
    samples=50,
    samples y=30,
    z buffer=sort,
    domain=0:360,
    y domain=0:360
    ] (
    {(3.5 + 1*cos(y))*cos(x)},
    {(3.5 + 1*cos(y))*sin(x)},
    {1*sin(y)});
    \addplot3 [
    samples=40,
    samples y=1,
    domain=0:360,
    thick,
    dashed, DarkOrange3
    ] (
    {(3.5 + 1*cos(80))*cos(x)},
    {(3.5 + 1*cos(80))*sin(x)},
    {1*sin(80)});
    \addplot3 [
    samples=15,
    samples y=1,
    domain=-65:130,
    thick,
    dashed,
    DarkOrange3
    ] (
    {3.5 + 1*cos(x)},
    {0},
    {1*sin(x)});
    \node [DarkOrange3] at (1.5,1.25,-1.25) {$\mathcal{C}_1$};
    \node [DarkOrange3] at (1,0,-1.25) {$\mathcal{C}_2$};
  \end{axis}
\end{tikzpicture}
\caption{Unabhängigen Kurven $\mathcal{C}_1$ und $\mathcal{C}_2$ auf
  einem Torus.}
\label{fig:7.5.15-9}
\end{figure}
%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: