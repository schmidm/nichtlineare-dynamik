\begin{notice}[Wiederholung zur Logistischen Abbildung:]
  \begin{description}
  \item Die Abbildungsvorschrift lautet
    \begin{equation*}
      x_{n+1} = f_r(x_n) = r x_n (1-x_n)
    \end{equation*}
    wobei $0 < r \le 4$ und $f_r : [0,1] \to [0,1]$.

    Die Eigenschaften dieser Abbildung sind:
    \begin{enumerate}
    \item Der periodische Bereich tritt auf bei $0 < r < r_\infty = \num{3.5699456}$.
      \begin{itemize}
      \item Eine Periodenverdopplung findet über eine Gabelbifurkation
        statt (Feigenbaumszenario)
      \item Skalierungseigenschaften
        \begin{enumerate}
        \item Bifurkationspunkte des $2^n$-Zyklus:
          $r_n = r_\infty - c \delta^{-n}$ (für $n \gg 1$)
        \item \enquote{Superzyklen} sind definiert durch
          \begin{equation*}
            \frac{\diff}{\diff x_0} f_{R_n}^{2^n}(x_0) = \prod_i f_{R_n}'(x_i) = 0
          \end{equation*}
          mit den Abständen
          \begin{equation*}
            d_n = f_{R_n}^{2^{n-1}} \left(\frac{1}{2}\right) - \frac{1}{2}
            \;,\quad
            R_n = r_\infty - c' \delta^{-n}
            \;,\quad
            \frac{d_n}{d_{n+1}} = - \alpha
            \quad\text{für $n \gg 1$}
          \end{equation*}
          mit den Feigenbaumkonstanten
          \begin{equation*}
            \alpha = \num{2.5029}\ldots
            \;,\quad
            \delta = \num{4.6692}\ldots
          \end{equation*}
        \end{enumerate}
      \end{itemize}
      
    \item Der chaotische Bereich befindet sich bei $r_\infty < r \le 4$
    \end{enumerate}
  \end{description}
\end{notice}

Die Abstände sind gegeben durch
\begin{equation*}
  d_n = f_{R_n}^{2^{n-1}} \left(\frac{1}{2}\right) - \frac{1}{2}.
\end{equation*}
Diese Form ist eher unhandlich und wir führen eine
Koordinatentransformation (Verschiebung) durch.
\begin{equation*}
  x \to x - \frac{1}{2}
  \;,\quad 
  f \to f - \frac{1}{2}
\end{equation*}
Damit lautet die Abbildungsvorschrift
\begin{equation*}
  x_{n+1} = f_r(x_n) = r \left(\frac{1}{4} - x_n^2\right) - \frac{1}{2}
\end{equation*}
wobei $f:[-1/2,1/2] \to [-1/2,1/2]$. Damit haben wir $x=1/2$ nach
$x=0$ verschoben, sodass die Abstände $d_n$ nun wie folgt geschrieben
werden können
\begin{equation*}
  d_n = f_{R_n}^{2^{n-1}}(0).
\end{equation*}

\minisec{Skalierungsverhalten (Selbstähnlichkeit)}

Aus den numerischen Ergebnissen haben wir bereits ein bestimmtes
Saklierungsverhalten beobachtet, welches auf die Feigenbaumkonstanten
$\alpha$ und $\delta$ führt. Es gibt ein $\alpha$, so dass
\begin{align*}
  - \alpha f_{R_{n+1}}^{2^n}\left(\frac{x}{-\alpha}\right)
  &\stackrel{\text{= für $n\gg1$}}{\approx}
  f_{R_n}^{2^{n-1}}(x) \\
  &\stackrel{x=0}{\implies}
  - \alpha d_{n+1} \approx d_n \\
  \implies
  \frac{d_n}{d_{n+1}} &\approx -\alpha. 
\end{align*}
Reskalierung führt auf
\begin{align*}
  \lim_{n\to\infty} (-\alpha)^n f_{R_{n+1}}^{2^n}\left(\frac{x}{(-\alpha)^n}\right) &\equiv g_1(x).
\end{align*}
Verallgemeinerung diese Prinzips ergibt
\begin{align*}
  -\alpha f_{R_{n+i}}^{2^n} \left(\frac{x}{-\alpha}\right) &\approx f_{R_{n-1+i}}^{2^{n-1}}(x), \\
  \implies
  \lim_{n\to\infty} (-\alpha)^n f_{R_{n+i}}^{2^n} \left(\frac{x}{(-\alpha)^n}\right) &\equiv g_i(x).
\end{align*}
Es gilt dann
\begin{align*}
  \boxed{ g_{i-1}(x) = -\alpha g_i\left(g_i\left(\frac{x}{-\alpha}\right)\right) \equiv T g_i(x)}
\end{align*}
mit dem Verdopplungsoperator $T$.

\begin{proof}
  \begin{align*}
    g_{i-1}(x)
    &= \lim_{n\to\infty} (-\alpha)^n f_{R_{n+i-1}}^{2^n}\left(\frac{x}{(-\alpha)^n}\right) \\
    &= \lim_{n\to\infty} (-\alpha)(-\alpha)^{n-1}
      \underbrace{f_{R_{n-1+i}}^{2^{n-1+1}}}_{f_{R_{n-1+i}}^{2^{n-1}} \circ f_{R_{n-1+i}}^{2^{n-1}}}
      \left(-\frac{1}{\alpha}\frac{x}{(-\alpha)^{n-1}}\right) \\
    &\stackrel{m=n-1}{=} \lim_{m\to\infty} (-\alpha)(-\alpha)^m f_{R_{m+i}}^{2^m}
      \biggl(\frac{1}{(-\alpha)^m} \underbrace{(-\alpha)^m f_{R_{m+i}}^{2^m}\left(-\frac{1}{\alpha}\frac{x}{(-\alpha)^m}\right)}_{\to g_i(x/-\alpha)} \biggr) \\
    &= -\alpha g_i\left(g_i\left(\frac{x}{-\alpha}\right)\right)
  \end{align*}
\end{proof}
Für $i \to \infty$ erhalten wir
\begin{align*}
  g_i(x) &= T g_i(x) \\
  i\to\infty: \quad
  g(x) &= T g(x) 
\end{align*}
$g(x) = \lim_{i\to\infty} g_i(x)$ ist Lösung der
Funktionalgleichung.
\begin{align*}
  \boxed{g(x) = T g(x) = -\alpha g\left(g\left(\frac{x}{-\alpha}\right)\right)}
\end{align*}
Die Lösung der Funktionalgleichung liefert die
Feigenbaumkonstante $\alpha$
\begin{align*}
  x = 0
  \implies
  g(0) = -\alpha g(g(0))
  \implies
  \alpha = - \frac{g(0)}{g(g(0))}.
\end{align*}
Beachte, dass mit $g(x)$ auch $\mu g(x/\mu)$ Lösung der
Funktionalgleichung ist. Setzen wir o.B.d.A.\ $g(0)=1$, führt dies auf
\begin{equation*}
  \implies \alpha = - \frac{1}{g(1)}
\end{equation*}

Lösungsansatz: Taylorreihe für $g(x)$ und Koeffizientenvergleich. Ein
grober Ansatz ist
\begin{align*}
  g(x) &= 1 + b x^2 \\
  \implies -\alpha g\left(g\left(-\frac{x}{\alpha}\right)\right)
  &= - \alpha (1+b) - \frac{2 b^2}{\alpha} x^2 + \mathcal O(x^4) \\
  &\stackrel!= 1 + b x^2
\end{align*}
Koeffizientenvergleich:
\begin{align}
  \tag{*}\label{eq:stern4.1}
  -\alpha(1+b) &= 1 \\
  \tag{**}\label{eq:stern4.2}
  -\frac{2b^2}{\alpha} &= b
\end{align}
Aus \eqref{eq:stern4.2} folgt
\begin{equation*}
  2b = - \alpha
\end{equation*}
Einsetzen in \eqref{eq:stern4.1}
\begin{align*}
  -\alpha\left(1-\frac{\alpha}{2}\right) &= 1 \\
  \alpha^2 - 2\alpha - 2 &= 0 \\
  \alpha &= 1 \pm \sqrt{3} = \num{2.73}
\end{align*}
Genauer: Siehe Mathematica-Skript.
\begin{align*}
  \alpha &= \num{2.5029078751} \\
  g(x) &= 1 - \num{1.52763}\,x^2 + \num{0.104815}\,x^4
         + \num{0.0267057}\,x^6 + \ldots
\end{align*}

Den Wert für die Feigenbaumkonstante $\delta$ erhalten wir mittels des
Skalierungsverhältnisses
\begin{equation*}
  R_n - R_\infty \sim \delta^{-n} \quad\text{für $n\gg1$}
\end{equation*}
Entwicklung von $f_R(x)$ um $f_{R_\infty}(x)$
\begin{equation*}
  f_R(x) = f_{R_{\infty}}(x) + (R-R_\infty) \delta f(x) + \ldots
\end{equation*}
Entwicklung von $T f_R(x)$:
\begin{equation*}
  T f_R = T f_{R_\infty} + (R-R_\infty) L_{f_{R_\infty}} \delta f
\end{equation*}
wobei $L_{f_{R_\infty}}$ die Linearisierung des Verdopplungsoperators
ist und $\delta f \equiv \partial f_R(x)/\partial R
|_{R_{\infty}}$. Die Linearisierung von $T$ ist 
\begin{align*}
  L_f \delta f
  &= - \alpha \{ (f+\delta f)[(f+\delta f)
    (-x/\alpha)]-f[f(-x/\alpha)]\} \\
  &= - \alpha \{f'[f(-x/\alpha)] \delta f + \delta f[f(-x/\alpha)]\}.
\end{align*}
Damit folgt für $n$-faches Anwenden von $T$
\begin{align*}
  T^n f_R
  &=  \underbrace{T^n f_{R_\infty}}_{\mathclap{\xrightarrow{n \to \infty} g(x)}} + (R-R_\infty)
    \underbrace{L_{T^{n-1} f_{R_\infty}} \ldots L_{T f_{R_\infty}} L_{f_{R_\infty}} \delta f}_{\xrightarrow{n \to \infty} L_g^n \delta f}
    + \mathcal O[(\delta f)^2].
\end{align*}
Entwicklung von $\delta f(x)$ nach den Eigenfunktionen von $L_g$:
\begin{align*}
  L_g \varphi_\nu = \lambda_\nu \varphi_\nu
  \;,\quad
  \delta f = \sum_\nu c_\nu \varphi_\nu \\
  \implies L_g^n \delta f = \sum_\nu c_\nu \lambda_\nu^n \varphi_\nu
  \xrightarrow{n\gg1}
  c_1 \lambda_1^n \varphi_1
\end{align*}
wobei $\lambda_1$ der größte Eigenwert ist. Damit und mit
$\delta=\lambda_1$ und $h(x)=\varphi_1$:
\begin{align*}
  T^n f_{R_n}(0) &\approx g(0) + (R_n-R_\infty) \delta^n c_1 h(0) \\
  \implies R_n - R_\infty &= - \frac{1}{c_1 h(0)} \delta^{-n}
\end{align*}
Also ist die Feigenbaumkonstante $\delta$ gegeben als größter
Eigenwert der Eigenwertgleichung
\begin{align*}
 \boxed{ L_g h(x) = \delta h(x) = - \alpha \left\{
  g' \left[ g\left(-\frac{x}{\alpha}\right) \right]
  h\left(-\frac{x}{\alpha}\right)
  + h \left[ g\left(-\frac{x}{\alpha}\right) \right]
  \right\}}
\end{align*}

\minisec{Universalität beim Feigenbaumszenario} Abbildung
$x_{n+1} = f_r(x_n)$ mit einem Maximum bei $x_0 = 0$ und eine negative
Schwarzsche Ableitung
\begin{align*}
  \frac{f'''}{f'} - \frac{3}{2} \left(\frac{f'''}{f'}\right)^2 < 0,
\end{align*}
Es gibt eine Selbstähnlichkeit in der Umgebung des Maximums in jeder
Stufe der Periodenverdopplung bei geeigneter Reskalierung:
\begin{align*}
  -\alpha f_r\left(f_r\left(-\frac{x}{\alpha}\right)\right)
  \equiv T f_r(x) \approx f_r(x)
\end{align*}
Aus dieser Selbstähnlichkeit folgt
\begin{align*}
  \lim_{i\to\infty}\lim_{n\to\infty} f_{r=R_{n+i}}(x) \equiv g(x)
\end{align*}
Diese Funktion $g(x)$ ist eine universelle Funktion (unabhängig von
$f_r$) und Lösung der Funktionalgleichung
\begin{align*}
  \boxed{T g(x) = - \alpha g\left( g\left(-\frac{x}{\alpha}\right) \right) = g(x)}
\end{align*}
Der Ansatz mit einer Taylorreihe liefert
$\alpha = \num{2.5029}\ldots$.  Die Feigenbaumkonstante ist der größte
Eigenwerte der linearisierten Eigenwertgleichung
\begin{equation*}
  L_g h(x) = \delta h(x)
\end{equation*}
mit
\begin{equation*}
  \delta = \num{4.6692}\ldots
\end{equation*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: