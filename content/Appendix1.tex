\chapter{Zur Hausdorff-Dimension}

Im Unterschied zur Minkowski-Dimension (im Skript als
Hausdorff-Dimension bezeichnet) beschränkt sich die
Hausdorff-Dimension nicht auf endliche Überdeckungen, wodurch die
beiden Größen im Allgemeinen nicht gleich sind. Allerdings stimmen sie
in den relevanten Fällen oft überein, weswegen die Minkowski-Dimension
oft zur numerischen Approximation der Hausdorff-Dimension verwendet
wird.

Wir folgen bei der Behandlung der Hausdorff-Dimension der
Herangehensweise in \cite{dodson}. Den Ausgangspunkt für die
Definition des Hausdorff-Dimension bildet dabei das $d$-Dimensionale
Hausdorff-Maß.

\begin{theorem}[Definition]
Es sei $E \subseteq \mathbb{R}^n$, $\delta > 0$ und $s>0$. Dann definieren wir
\begin{equation*}
\mathcal{H}_{\delta,s}(E) = \inf_{\beta(\delta)} \sum_{U \in \beta(\epsilon)} d(U)^s\,,
\end{equation*}
wobei das Infimum über alle abzählbaren Überdeckungen
$\beta(\delta) \in \mathcal{P}(\mathbb{R}^n)$ von $E$ mit
$d(U)\leq \delta$ für alle $U \in \beta(\delta)$ genommen wird. Dabei
ist
$d(U) =\mathrm{sup} \left\lbrace |x-y| \;:\; x,y \in U \right\rbrace$.
Das $s$-dimensionale Hausdorff-Maß von $E$ ist dann definiert als
\begin{equation*}
\mathcal{H}_s (E) = \lim_{\delta \to 0} \mathcal{H}_{\delta,s} (E)\,.
\end{equation*}
\end{theorem}

Für jedes $\delta > 0$ ist
$\beta_Q = \left\lbrace B_\delta (q) \;:\; q \in \mathbb{Q}^n
\right\rbrace$
eine abzählbare Überdeckung von $\mathbb{R}^n$, es existiert also
insbesondere immer eine Überdeckung $\beta(\delta)$ wie in der
Definition verwendet.

Nach Definition ist $d(U) \geq 0\; \forall U \subseteq \mathbb{R}^n$,
da immer mindestens eine Überdeckung existiert, die den Anforderungen
der Definition genügt, ist
\begin{equation*}
\forall E \in \mathbb{R}^n,\, s \geq 0,\,\delta > 0 \; : \mathcal{H}_{\delta,s}(E) \geq 0\,.
\end{equation*}
Beachte, dass $\mathcal{H}_{\delta,s}(E)$ nicht endlich sein muss.

Zudem ist $\mathcal{H}_{\delta,s}(E)$ monoton fallend bezüglich
$\delta$, da für $\delta' \geq \delta$ jede Überdeckung
$\beta(\delta)$ auch eine Überdeckung $\beta(\delta')$ ist. Damit
existiert der Grenzwert und das Hausdorff-Maß ist wohldefiniert.

\begin{notice}
  Das Hausdorff-Maß stellt ein äußeres Maß dar. Für eine Behandlung
  der Maßtheoretischen Eigenschaften von $\mathcal{H}_s$, siehe
  \cite{hausdorff}.
\end{notice}

Anhand des Hausdorff-Maßes kann nun die Hausdorff-Dimension eingeführt
werden. Dies wird durch den folgenden Satz gewährt.

\begin{theorem}
  Für jede Menge $E \in \mathbb{R}^n$ existiert ein eindeutiges
  $s_0(E) \in [0,\infty)$ für welches gilt:
\begin{equation*}
\mathcal{H}_s (E)  = \begin{cases} 0 &,s > s_0(E) \\ \infty &, s < s_0(E) \end{cases}
\end{equation*} 
Die Zahl $s_0(E)$ heißt Hausdorff-Dimension von $E$.
\end{theorem}

\begin{proof}
Es sei 
\begin{equation*}
s_0= \inf \left\lbrace s \geq 0\;: \mathcal{H}_s(E) = 0 \right\rbrace\,,
\end{equation*}
insbesondere ist $\mathcal{H}_s(E) > 0 $ für alle $s<s_0$.  Sei
$\delta, r >0$ und $\beta$ eine Überdeckung von $E$ so dass
$d(U) <\delta$ für alle $U\in \beta$, dann ist
\begin{equation}
\sum_{U\in \beta} d(U)^{s+r} \leq \delta^r \sum_{U\in \beta} d(U)^s\,.
\end{equation}
Und damit auch 
\begin{equation*}
\mathcal{H}_{\delta,s+r}(E) \leq \delta^r \mathcal{H}_{\delta,s}(E)\,.
\end{equation*}
Im Grenzwert $\delta \to 0$ liefert dies
\begin{equation}
0 \leq \mathcal{H}_{s+r} (E) \leq \lim_{\delta \to 0} \delta^r \mathcal{H}_{\delta,s}(E)\,.
\end{equation}
Falls also $\mathcal{H}_{s}(E) < \infty$ ist, so ist
$\mathcal{H}_{s+r}(E)=0$ für alle $r > 0$. Damit erfüllt $s_0$ dann
$\mathcal{H}_{s}(E) = 0 \Rightarrow s \geq s_0$ und
$s>s_0 \Rightarrow \mathcal{H}_s(E)=0$, da für jedes $s > s_0$ ein
$s_0 \leq s' <s$ exisitert so dass $\mathcal{H}_{s'} (E) =0$ ist.

Angenommen, es exisitert ein $s<s_0$ sodass
$\mathcal{H}_s(E) <\infty$ ist, so folgt dann, dass auch
\[\mathcal{H}_{\frac{s+s_0}{2}}(E) =0,\] was im Widerspruch zur
Definition von $s_0$ steht. Also gilt auch
\[s < s_0 \Rightarrow \mathcal{H}_s(E) = \infty,\]
womit die Behauptung folgt.
\end{proof}

\begin{notice}
  Es gibt Mengen mit Hausdorff-Dimension $0$, zum Beispiel abzählbare
  Mengen, nicht aber mit Hausdorff-Dimension $\infty$. Dies wird hier
  nicht explizit gezeigt, es gilt aber $\mathcal{H}_s(E) =0$ für alle
  $E\in \mathbb{R}^n$ falls $s>n$ ist.
\end{notice}
