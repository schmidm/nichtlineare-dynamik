\paragraph{Mathematisches Modell:} 
Nullstellen der Riemannschen Zetafunktion
\begin{equation*}
  \zeta(s) = \sum_{n=1}^{\infty} \frac{1}{n^s} = \prod_{\textrm{Primzahlen} \, p} \underbrace{\left( \sum_{m=0}^{\infty} p^{m s} \right)}_{(1-p^{-s})^{-1}}
\end{equation*}
Die Zetafunktion hat folgende Eigenschaften:
\begin{itemize}
\item Einfacher Pol bei $s=1$ (harmonische Reihe)
\item Triviale Nullstellen bei $s = -2,-4,-6, \ldots$
\item Nichttriviale Nullstellen im Streifen $0 < \real s < 1$
\item Riemannsche Vermutung: $\real s = 1/2$ für alle nichttrivialen
  Nullstellen
\end{itemize}
Mit der Substitution $s = 1/2 -\ii x$ folgt für die Zetafunktion
\begin{equation*}
  \zeta(x) = \prod_{p} \left( 1 - p^{\ii x -\frac{1}{2}} \right)^{-1} \, .
\end{equation*}
Man erkennt hierbei eine Analogie zur dynamischen Zetafunktion
\begin{equation*}
  \zeta(E) = \prod_{\mathrm{PO}} ( 1- t_{\mathrm{PO}}(E))
\end{equation*}
Die Dichte der Nullstellen der Zetafunktion kann nun berechnet werden.
\begin{align*}
  \varrho(x) &= - \frac{1}{\pi} \imag \frac{\diff}{\diff x} \ln \zeta(x) \\
  &=  \frac{1}{\pi} \imag  \frac{\diff}{\diff x} \sum_p \ln \left(1 - p^{\ii x -\frac{1}{2}}  \right) \\
  &= - \frac{1}{\pi} \imag  \frac{\diff}{\diff x} \sum_p \sum_{m=1}^{\infty} \frac{1}{m} p^{m(\ii x - \frac{1}{2})} \\
  &= - \frac{1}{\pi} \imag \sum_p \sum_{m=1}^{\infty} \, \underbrace{\ii \frac{\ln p}{p^{m/2}}}_{A_p} \, \exp(\ii \underbrace{ m \ln(p) x}_{S_p(x)})
\end{align*}
Hier erkennt man die formale Analogie zur Gutzwiller-Spurformel mit Bahnparametern $A_p$ und $S_p(x)$ gegeben durch Primzahlen $p$. Bisher ist jedoch kein physikalisches System bekannt, dessen Wirkung durch Primzahlen wiedergegeben wird. Allgemein wird vermutet, dass die Nullstellen sich asymptotisch so verhalten, wie ein GUE-Ensemble der Random-Matrix-Theorie. 

Es bleibt jedoch eine offene Frage: Kann ein hermitescher Operator
ohne Zeitumkehrinvarianz (bzw. keine Vertauschung mit antiunitären
Operatoren) gefunden werden, sodass die Eigenwerte die
Riemannschen-Nullstellen wiedergeben.

\section{Weitere Methoden}
Die Nächste-Nachbar-Verteilung beschreibt die lokalen Korrelationen im
Spektrum. Weitere Funktionen beschreiben auch mehr global die
Fluktuationen und lassen sich mit Aussagen der Random-Matrix-Theorie
vergleichen.

\subsection{Number Variance} 
Für ein entfaltetes Spektrum, lässt sich der Mittelwert für die Anzahl
der Zustände im Intervall $[E - L/2, E +L/2]$ beschreiben durch
\begin{equation*}
  \Braket{\int_{E-L/2}^{E+L/2} \varrho(E') \diff E' }_E = L \, .
\end{equation*}
Eine charakteristische Größe für die Fluktuationen im Spektrum ist die Varianz der Anzahl der Zustände, sie wird auch \acct{Number Variance} genannt. 
\begin{equation*}
  \boxed{\Sigma^2(L) = \Braket{\left( \int_{E-L/2}^{E+L/2} \varrho(E') \diff E' - L \right)^2}_E}
\end{equation*}
Sie gibt somit die Fluktuationen um $L$ an.  Für den Poisson-Prozess,
bzw. Random-Matrix-Theorie ergibt sich:
\begin{equation*}
  \Sigma^2(L) =
  \begin{cases}
    L &, \textrm{Poisson} \\
    \frac{1}{\pi^2} \left[ \ln(2 \pi L ) + \gamma  +1 - \frac{\pi^2}{8}\right] + \mathcal{O} \left( \frac{1}{L} \right) &, \textrm{GOE} \\
    \frac{1}{\pi^2} \left[  \ln(2 \pi L ) + \gamma  +1\right] + \mathcal{O} \left( \frac{1}{L} \right) &, \textrm{GUE} \\
    \frac{1}{2\pi^2} \left[ \ln(4 \pi L ) + \gamma  +1 + \frac{\pi^2}{8} \right] + \mathcal{O} \left( \frac{1}{L} \right) &, \textrm{GSE}
  \end{cases}
\end{equation*}
Dabei ist $\gamma$ die Euler-Konstante
\begin{equation*}
  \gamma = \lim_{n \to \infty} \sum_{k=1}^{n} \frac{1}{k} - \ln(n) \approx 0.57722
\end{equation*}
Bei der Poisson-Verteilung sind die Fluktuationen der Anzahl der Zustände am Mittelwert $L$ gleich $\sqrt{L}$. Für reguläre, bzw. chaotische Systeme muss für eine Übereinstimmung mit der Number Variance die Länge $L$ so gewählt werden, dass $L$ nicht zu klein ist, aber auch nicht zu groß $L < \frac{\hbar}{T_{\mathrm{PO}}}$, wobei $T_{\mathrm{PO}}$ die Periode der kleinsten periodischen Bahn ist. 
\subsection{Spektrale Steifheit}

Neben der Number Variance gibt es auch noch weitere (ähnliche) Größen zur Charakterisierung der Fluktuationen. Ein Beispiel ist hierbei die \acct{spektrale Steifheit} (englisch \emph{spectral rigidity}). Sie ist definiert durch
\begin{equation*}
  \boxed{\Delta_3(L) = \frac{1}{L} \Braket{\textrm{min}\{a,b\} \, \int_{E-L/2}^{E+L/2} \diff \tilde{E} \left[ \int_{E_0}^{\tilde{E}} \varrho(E') \diff E' - (a+b\tilde{E}) \right]^2 }_E}
\end{equation*}
und entspricht einem Vergleich mit einer Geraden mit Steigung und
Konstante $a$, $b$. Für den Poisson-Prozess, bzw. der
Random-Matrix-Theorie ergibt sich:
\begin{equation*}
  \Delta_3(L) =
  \begin{cases}
    L/15 &, \textrm{Poisson} \\
    \frac{1}{\pi^2} \left[ \ln(2 \pi L ) + \gamma - \frac{5}{4} - \frac{\pi^2}{8}\right] + \mathcal{O} \left( \frac{1}{L} \right) &, \textrm{GOE} \\
    \frac{1}{\pi^2} \left[  \ln(2 \pi L ) + \gamma  -\frac{5}{4}\right] + \mathcal{O} \left( \frac{1}{L} \right) &, \textrm{GUE} \\
    \frac{1}{2\pi^2} \left[ \ln(4 \pi L ) + \gamma  - \frac{5}{4} + \frac{\pi^2}{8} \right] + \mathcal{O} \left( \frac{1}{L} \right) &, \textrm{GSE}
  \end{cases}
\end{equation*}
Die Ergebnisse ähneln dabei denen der Number Variance. Für das
Sinai-Billard ist die spektrale Steifheit in
Abbildung~\ref{fig:23.7.15-1} zu sehen.

\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = 0.35\textwidth]{steifheit.png}};
  \end{tikzpicture}
  \caption{Spektrale Analyse des Sinai-Billard und GOE-Vorhersage für
    die spektrale Steifheit. Zum Vergleich ist auch die
    Poisson-Vorhersage eingezeichnet. Von \cite{bohigas}.}
  \label{fig:23.7.15-1}
\end{figure}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End:
