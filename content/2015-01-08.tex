\minisec{Ausblick}

\begin{enumerate}[I]
\item Dissipative Systeme
\item Konservative Systeme
  \begin{enumerate}[a)]
  \item Ziel der klassischen Mechanik seit Newton
    \begin{itemize}
    \item Erkenntnisse über Naturgesetze, denen Bewegungen gehorchen
    \item Vorhersagen über Bewegungsabläufe
    \end{itemize}
    Deterministisches Weltbild: Wir kennen die Kräfte und
    Zwangbedingungen zwischen den Körpern
    \begin{enumerate}[{$\Rightarrow$}]
    \item Deterministische Bewegungsgleichung
    \item Bewegungsablauf als Lösung der Differentialgleichung bei
      gegebenen Anfangsbedingungen.  \enquote{Nur} ein technisches
      Problem? Nein.
    \end{enumerate}
    Es gibt deterministisches Chaos auch in konservativen Systemen.
    Der Unterschied zwischen dissipativen und klassischen
    Hamiltonsches Systemen ist, dass bei Hamiltonschen Systemen das
    Phasenraumvolumen erhalten bleibt (Satz von Liouville).

  \item Quantensysteme
    \begin{itemize}
    \item \enquote{Alte} Quantenmechanik: Diskrete Energieniveaus
      durch Quantisierung klassischer Bahnen (Bohr-Sommerfeld
      Quantisierungsregel):
      \begin{equation*}
        \oint \bm p \cdot \diff \bm r = n \hbar \;,\quad n = 1,2,3,\ldots
      \end{equation*}
      Erfolge: Rydbergserien des Wasserstoffatoms (Bohrsches Atommodell)

      Versagen: Schon für den Grundzustand des He-Atoms (Dreikörperproblem)

    \item \enquote{Neue} Quantenmechanik: Schrödingergleichung
      $H\psi=E\psi$ liefert korrekte Ergebnisse, aber ohne jeden Bezug
      zu klassischen Bahnen oder klassische Dynamik.

    \item \enquote{Alte} Quantenmechanik geriet lange Zeit in
      Vergessenheit. Frage: Gibt es Chaos auch in Quantensystemen?
    \end{itemize}
  \end{enumerate}
\end{enumerate}


\chapter{Konservative Systeme}

\section{Klassisches Chaos}

Wir starten mit einer Wiederholung der klassischen Mechanik.

Die Lagrangefunktion lautet
\begin{equation*}
  L =L(q_i,\dot q_i,t) = T-V
\end{equation*}
mit den generalisierten Koordinaten $q_i$, welche die
Zwangsbedingungen beinhalten.  Die Euler-Lagrange-Gleichung lautet
\begin{equation*}
  \frac{\diff}{\diff t} \frac{\partial L}{\partial \dot q_i}
  = \frac{\partial L}{\partial q_i}
\end{equation*}
Dies ist äquivalent zur Hamiltonfunktion und den Hamiltonschen
Gleichungen:
\begin{align*}
  p_i &= \frac{\partial L}{\partial \dot q_i} \;,\quad\text{kanonische Impulse} \\
  H &= \sum_i p_i \dot q_i - L \\
  \dot p_i &= - \frac{\partial H}{\partial q_i} \\
  \dot q_i &= \frac{\partial H}{\partial p_i}
\end{align*}
Die Mechanik lässt sich auch symplektisch formulieren.  Dazu definieren
wir den Phasenraumvektor $\bm\gamma = (\bm q,\bm p)$, der $2N$
Dimensionen hat.  Weiterhin benötigen wir die symplektische Matrix
$\mathscr J$
\begin{equation*}
  \mathscr J =
  \begin{pmatrix}
    \mathbb{O}_{N\times N} & \mathds{1}_{N\times N} \\
    -\mathds{1}_{N\times N} & \mathbb{O}_{N\times N} \\
  \end{pmatrix}
  \in \mathbb{R}^{2N\times2N}
\end{equation*}
womit wir die Hamiltonschen Bewegungsgleichungen schreiben können als
\begin{equation*}
  \frac{\diff}{\diff t} \bm \gamma
  = \mathscr J \frac{\partial H}{\partial \bm \gamma}
\end{equation*}
Für ein System mit $N$ Freiheitsgraden gilt, dass die
Bewegungsgleichungen meist \enquote{einfach} zu lösen sind, wenn es
geeignete Koordinaten gibt, in denen die Hamiltonfunktion eine
\enquote{einfache} Form annimmt.  Eine einfache Hamiltonfunktion wäre
zum Beispiel eine, bei der alle Koordinaten zyklisch sind, das heißt
$H$ hängt nicht explizit von den Koordinaten ab.
\begin{align*}
  H = H(P)
  \implies
  \dot{P}_i &= 0
  \implies
  P_i(t) = \alpha_i = \const \\
  \dot Q_i &= \frac{\partial H}{\partial P_i} = \omega_i = \const
  \implies
  Q_i(t) = \omega_i t + \beta_i
\end{align*}
Für ein geschlossenes System bedeutet dies eine Bewegung auf einem
$N$-dimensionalen Torus.

Frage: Wie finden wir die \enquote{richtigen} (zyklischen) Variablen?

Diese findet man mittels einer kanonischen Transformation $(p,q) \to
(P,Q)$ über eine erzeugende Funktion, z.B.\ $F_2 = S(\bm q,\bm P,t)$.
\begin{align*}
  p_i &= \frac{\partial S}{\partial q_i} \\
  Q_i &= \frac{\partial S}{\partial P_i} \\
  H(\bm P,\bm Q,t) &= H(\bm p,\bm q,t) +
  \underbrace{\frac{\partial}{\partial t} S(\bm q,\bm P,t)}_{
    \mathclap{\text{$=-E$ für $H$ zeitunabh.}}}
\end{align*}
Ein geeignetes $S[H=H(\bm P); \bm P_i(t)=\const;Q_i(t)=\omega_i t +
\beta_i]$ ist Lösung der Hamilton-Jacobi-Gleichung
\begin{equation*}
  H\left(\frac{\partial S}{\partial q_i},\bm q,t\right)
  + \frac{\partial S}{\partial t} = 0
\end{equation*}
welche eine partielle Differentialgleichung ist.  Die Hamiltonfunktion
heißt integrabel, falls eine globale Lösung der
Hamilton-Jacobi-Gleichung existiert.

Problem: Die Existenz einer globalen Lösung ist nicht gesichert.

Eine Unterklasse der integrablen Systeme sind die separablen Systeme. $H$ heißt separabel in den Koordinaten $q_i$, wenn die Hamilton-Jacobi-Gleichungen mit dem Separationsansatz
\begin{equation*}
  S=\sum_i S_i(q_i,\alpha_1,\ldots,\alpha_N)
\end{equation*}
in $N$ gewöhnliche Differentialgleichungen
\begin{equation*}
  H_i\left(\frac{\partial S}{\partial q_i}, q_i \right) = \alpha_i
\end{equation*}
zerfallen, was $N$ Erhaltungsgrößen (Integrale der Bewegung)
entspricht.  Für die Umlauffrequenz $\omega_i$ auf dem Torus gilt im
Allgemeinen
\begin{equation*}
  \omega_i \neq \frac{\partial H}{\partial \alpha_i}
\end{equation*}

\subsection{Wirkungs- und Winkelvariablen}

Die Wahl der $\alpha_i$ ist nicht eindeutig, ebenso $J_i =
J_i(\bm\alpha)$ bzw.\ $\alpha_i = \alpha_i(\bm J)$.  Als physikalisch
sinnvoll erweist sich die Umlauffrequenz auf dem Torus
\begin{equation*}
  \frac{\partial H}{\partial J_i} = \omega_i
\end{equation*}
Die Erzeugende für die kanonische Transformation ist gegeben durch
\begin{align*}
  \bar S(\bm q,\bm J) &= S(\bm q,\bm\alpha(\bm J)) \\
  H(\bm\alpha) &\to \bar{H}(\bm{J})
\end{align*}
Für die \acct{Wirkungsvariable} gilt
\begin{equation*}
  J_i = \frac{1}{2\pi}\oint p_i \diff q_i
  = \frac{1}{2\pi}\oint \frac{\partial S_i(q_i,\bm\alpha)}{\partial q_i} \diff q_i
\end{equation*}
Dies liefert uns einen neuen Satz von $N$ (erhaltenen) Impulsen.

Die \acct{Winkelvariable} zu den $J_i$ kanonisch konjugierten
Koordinaten $Q_i$ ist bei $J_i = \const$ durch
\[\theta_i=\omega_i t+\beta_i\] gegeben, d.h.\
\begin{align*}
  \theta_i &= \frac{\partial S(q_i,J_i)}{\partial J_i}.
\end{align*}


\begin{example}
  \begin{enumerate}
  \item Einfachster Fall: Eindimensionale konservative Systeme
    (integrabel)
    \begin{align*}
      H &= \frac12 p^2 + V(q) = E = \const \\
      J &= \frac{1}{2\pi} \oint p \diff q
      = \frac{1}{2\pi} \oint \sqrt{2(E-V(q))} \diff q
    \end{align*}
    Für ein Pendel:
    \begin{align*}
      H &= \frac{p_\varphi^2}{2 m \ell^2} + m g \ell (1-\cos\varphi) = E \\
      J &= \frac{1}{2\pi}\oint\sqrt{2m\ell^2(E-mg\ell(1-\cos\varphi))}\diff\varphi
    \end{align*}
    Fallunterscheidung:
    \begin{enumerate}
    \item $E<0$: Es existiert keine Lösung
    \item $0<E<2mg\ell$: $\varphi$-Bewegung hat zwei Umkehrpunkte
      gegeben durch $\cos\varphi_{1,2}=1-E/(2mg\ell)$: Vibration
      \begin{equation*}
        J = \frac{1}{\pi}\int_{\varphi_1}^{\varphi_2}
        \sqrt{2m\ell^2(E-mg\ell(1-\cos\varphi))}\diff\varphi
      \end{equation*}
    \item $E>2mg\ell$: $1-\cos\varphi=E/(mg\ell)>2$ Es gibt also keine
      Umkehrpunkte der $\varphi$-Bewegung: Rotation
      \begin{equation*}
        J = \frac{1}{2\pi}\int_0^{2\pi}
        \sqrt{2m\ell^2(E-mg\ell(1-\cos\varphi))}\diff\varphi
      \end{equation*}
    \end{enumerate}
    Veranschaulichung der verschiedenen Bewegungstypen im
    Phasenportrait erhält man durch Auftragung $p(q) = \pm
    \sqrt{2(E-V(q))}$

  \item Zentralkraftproblem $V(\bm r) = V(r = |\bm r|)$: In
    Kugelkoordinaten $(r,\vartheta,\varphi)$
    \begin{gather*}
      H = \frac{1}{2m} \left(
        p_r^2 + \frac{p_\vartheta^2}{r^2} + \frac{p_\varphi^2}{r^2 \sin^2\vartheta}
      \right) + V(r) = E \\
      p_r = \frac{\partial S}{\partial r}\;,\quad
      p_\vartheta = \frac{\partial S}{\partial \vartheta}\;,\quad
      p_\varphi = \frac{\partial S}{\partial \varphi}
    \end{gather*}
    Mit dem Separationsansatz $S = S_r(r) + S_\vartheta(\vartheta) +
    S_\varphi(\varphi)$ lautet die Hamilton-Jacobi-Gleichung:
    \begin{equation*}
      \underbrace{
        \frac{1}{2m} \biggl\{
        \left( \frac{\partial S}{\partial r} \right)^2
        + \frac{1}{r^2}
        \underbrace{
          \biggl[
          \left( \frac{\partial S}{\partial \vartheta} \right)^2
          + \frac{1}{\sin^2\vartheta} \biggl(
          \underbrace{
            \frac{\partial S}{\partial \varphi}
          }_{\mathclap{\alpha_\varphi = L_Z = \const}}
          \biggr)^2
          \biggr]
        }_{\alpha_\vartheta^2 = \bm L^2 = \const}
        \biggr\} + V(r)
      }_{\alpha_r = E = \const}
      = E
    \end{equation*}
    % ab hier eigentlich 15.01.2015
    \begin{align*}
      \frac{\partial S_\varphi}{\partial \varphi}
      &= L_z
      &\overset{\text{Wirkungsvariable}}{\implies}&&
      J_\varphi &= \frac{1}{2\pi} \oint L_z \diff\varphi
      = \frac{1}{2\pi} \int_0^{2\pi} L_z \diff\varphi
      = L_z \\
      \frac{\partial S_\vartheta}{\partial \vartheta}
      &= \sqrt{L^2 - \frac{L_z^2}{\sin^2\vartheta}}
      &\overset{\text{\phantom{Wirkungsvariable}}}{\implies}&&
      J_\vartheta &= \frac{1}{2\pi} \oint \sqrt{L^2 - \frac{L_z^2}{\sin^2\vartheta}} \diff\varphi
      = L - L_z \\
      \frac{\partial S_r}{\partial r}
      &= \sqrt{2 m E - \frac{L^2}{r^2} - V(r)}
      &\overset{\text{\phantom{Wirkungsvariable}}}{\implies}&&
      J_r &= \frac{1}{2\pi} \oint \sqrt{2 m E - \frac{L^2}{r^2} - V(r)} \diff r
    \end{align*}

    Beim Keplerproblem haben wir ein Zentralpotential gegeben durch
    \begin{equation*}
      V(r) = -\frac{k}{r} \overset{E<0}{\implies}
      J_r = -(J_\vartheta + J_\varphi) + k \sqrt{\frac{m}{-2E}}
    \end{equation*}
    In Wirkungs-Winkel-Variablen erhält man
    \begin{equation*}
      H = E = - \frac{m k^2}{2 (J_r + J_\vartheta + J_\varphi)^2}
    \end{equation*}
    Frequenzen:
    \begin{equation*}
      \omega_r = \omega_\vartheta = \omega_\varphi
      = \frac{\partial H}{\partial J_{(r,\vartheta,\varphi)}}
      = \frac{mk^2}{(J_r + J_\vartheta + J_\varphi)^3}
    \end{equation*}
    Dies entspricht einer Entartung der $\omega$ und hängt mit den
    Symmetrien zusammen.  Wir erhalten also geschlossene Bahnen, hier die
    bekannten Keplerellipsen.
    \begin{equation*}
      H = H(J_r,\underbrace{J_\vartheta + J_\varphi}_{L=|\bm L|})
      \implies \omega_\vartheta = \omega_\varphi
      \quad\text{$O(3)$-Symmetrie}
    \end{equation*}
    Im Allgemeinen ist $\omega_r \neq \omega_\vartheta$.  Das
    Keplerproblem besitzt eine höhere Symmetrie ($O(4)$-Symmetrie) als für
    ein Zentralkraftproblem zu erwarten wäre.

    Separation in verschiedenen Koordinatensystemen (nicht nur
    Kugelkoordinaten möglich), z.B.\ Kugelkoordinaten, parabolische
    Koordinaten (vier Systeme im Ortsraum, sechs Systeme im Impulsraum).

  \item Wasserstoffatom im elektrischen Feld (Stark-Effekt)
    \begin{align*}
      V(\bm r) &= - \frac{k}{r} + F\cdot z \\
      H &= \frac{\bm p^2}{2 m} - \frac{k}{r} + F\cdot z
    \end{align*}
    Eine Separation in Kugelkoordinaten ist nicht möglich, jedoch eine
    Separation in parabolische Koordinaten gegeben durch
    \begin{equation*}
      \xi = r + z \;,\quad
      \eta = r - z \;,\quad
      \varphi
    \end{equation*}
    Damit und mit $m=1$ lautet die Hamiltonfunktion
    \begin{equation*}
      H = \frac{2}{\xi + \eta} (\xi p_\xi^2 + \eta p_\eta^2)
      + \frac{p_\varphi^2}{2\xi\eta} - \frac{2 k}{\xi + \eta}
      + \frac{F}{2} (\xi-\eta) = E
    \end{equation*}
    Der Separationsansatz für die Hamilton-Jacobi-Gleichung lautet
    \begin{equation*}
      S = S_\xi(\xi) + S_\eta(\eta) + S_\varphi(\varphi)
    \end{equation*}
  \end{enumerate}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: