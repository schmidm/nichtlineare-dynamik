\subsection{Die Kolmogorov-Entropie}

Wir kennen den Begriff der Entropie aus der statistischen Mechanik
\begin{equation*}
  S \sim \sum_i P_i \ln P_i
\end{equation*}
wobei $P_i$ die Wahrscheinlichkeit das System im Zustand $\{P_i\}$
oder genauer in der Phasenraumzelle $i$ zu finden ist. Die Entropie
$S$ misst (nach Shanon) die zusätzliche Information, welche benötigt
wird, um das System in einem bestimmten Zustand $i$ zu lokalisieren.

\begin{example}
  Adiabatische Gasexpansion.
  \begin{center}
    \begin{tikzpicture}[gfx]
      \draw (-2,0) rectangle (2,2);
      \draw[->] (0,0) -- (0,2.2);
      \node[below] at (-1,0) {$V_1$};
      \node[below] at (1,0) {$V_2$};
      \pgfmathsetseed{1337}
      \foreach \i in {1,...,15} {
        \node[circle,shade,ball color=white,inner sep=2pt]
        at (-1.8*rnd-.1,1.8*rnd+.1) {};
      }
    \end{tikzpicture}
  \end{center}
  Die Entropie steigt an, sofern die Box nicht mehr unterteilt ist, da
  die Unkenntnis über das System anwächst. Als die Atome auf eine
  Hälfte des Volumens beschränkt waren, wussten wir mehr über die
  Position der Atome als nachher. D.h.\ Informationsverlust ist die
  Folge.
\end{example}

Wir übertragen den Entropiebegriff auf die Dynamik eines seltsamen
Attraktors. Dazu betrachten wir eine Trajektorie auf dem Attraktor
\begin{equation*}
  \bm x(t) =
  \begin{pmatrix}
    x_1(t) \\
    \vdots \\
    x_d(t) \\
  \end{pmatrix}.
\end{equation*}
Unterteile den $d$-dimensionalen Phasenraum in Kästchen der Größe
$\ell^d$. Diskretisiere den Zeitablauf durch endliche Zeitschritte
$\tau$ mit $t_n = n \tau$. $P_{i_0,\ldots,i_n}$ ist hierbei die
Wahrscheinlichkeit, dass $\bm{x}(t=0)$ in Kästchen $i_0$ ist,
$\bm{x}(t=\tau)$ in Kästchen $i_1$, \ldots und $\bm{x}(t=n\tau)$ in
Kästchen $i_n$. Die Größe
\begin{equation*}
  K_n = - \sum_{i_0\ldots i_n} P_{i_0\ldots i_n} \ln P_{i_0\ldots i_n}
\end{equation*}
ist dann proportional zur Information, die man braucht, um das System
auf einer speziellen Trajektorie $i_0\ldots i_n$ mit der Genauigkeit
$\ell$ zu lokalisieren. $K_{n+1}-K_n$ ist die Information, die man zur
Vorhersage braucht in welchem Kästchen $i_{n+1}$ das System zur Zeit
$t_{n+1}$ sein wird, wenn es zur Zeit $t_n$ in den Kästchen $i_0\ldots
i_n$ war. Dabei ist $i_0\ldots i_n$ kein exakter
Bahnverlauf. Umgekehrt entspricht dies einem Informationsverlust über
die Trajektorie beim Zeitschritt von $t_n$ nach $t_{n+1}$.

\begin{theorem}[Definition]
  Die \acct{Kolmogorov-Entropie} ($K$-Entropie) ist die mittlere Rate
  des Informationsverlustes.
  \begin{align*}
    K &= \lim_{\tau\to0}\lim_{\ell\to0}\lim_{N\to\infty} \frac{1}{N\tau}
    \sum_{n=0}^{N-1} (K_{n+1} - K_n) \\
    &= - \lim_{\tau\to0}\lim_{\ell\to0}\lim_{N\to\infty} \frac{1}{N\tau}
    \sum_{i_0\ldots i_N} P_{i_0\ldots i_N} \ln P_{i_0\ldots i_N}
  \end{align*}
  Beachte, dass die Grenzwerte nicht vertauschbar sind!
\end{theorem}

\begin{enumerate}
\item Reguläre Bewegung: Anfänglich benachbarte Bahnen bewegen sich
  benachbart (kein Informationsverlust).
  \begin{equation*}
    P_{i_0 i_1} = 1 \cdot P_{i_0} = P_{i_0}
    \to
    - \sum_{i_0\ldots i_N} P_{i_0\ldots i_N} \ln P_{i_0\ldots i_N}
  \end{equation*}
  ist unabhängig von $N$ und somit $K = 0$.

\item Chaotische Bewegung: Anfänglich benachbarte Punkte werden
  exponentiell voneinander getrennt.
  \begin{align*}
    P_{i_0 i_1} &= \ee^{-\lambda \tau} \cdot P_{i_0} \\
    P_{i_0\ldots i_N} &= \ee^{-\lambda N \tau} \cdot P_{i_0}
    \;,\quad\text{mit $P_{i_0} = 1$ für $i_0$, $0$ sonst} \\
    \ee^{\lambda N \tau} &\to
    - \sum_{i_0\ldots i_N} \underbrace{P_{i_0\ldots i_N}}_{\ee^{-\lambda N \tau}}
    \underbrace{\ln P_{i_0\ldots i_N}}_{-\lambda N \tau}
    = N \lambda \tau \\
    \implies K &= \lambda
  \end{align*}

  Die Verbindung von $K$ mit dem Liapunovexponenten ist:
  \begin{itemize}
  \item Ein-dimensionale Abbildung: $K$ ist gleich dem (positiven)
    Liapunovexponenten $\lambda$
  \item $d$-dimensionale Abbildung: $K$ ist die gemittelte Summe der
    positiven Liapunovexponenten:
    \begin{equation*}
      K = \int \diff^d x\ \varrho(\bm x) \sum_i \lambda_i^+(\bm x)
    \end{equation*}
    wobei $\varrho(\bm x)$ die invariante Dichte auf dem Attraktor ist.
  \end{itemize}

\item Stochastische (zufällige) Bewegung: Anfänglich benachbarte
  Punkte sind mit gleicher Wahrscheinlichkeit über alle erlaubten
  Kästchen verteilt.
  \begin{align*}
    P_{i_0} &= 1 \text{ für ein $i_0$}, 0 \text{ sonst} \\
    P_{i_0i_1} &= \frac{\ell^d}{V} \ll 1 \\
    \implies
    - \sum_{i_0\ldots i_N} &\underbrace{P_{i_0\ldots i_N}}_{\ell^d/V}
    \underbrace{\ln P_{i_0\ldots i_N}}_{\ln\ell^d/V}
    \sim \ln \frac1\ell \\
    \xRightarrow{\ell \to 0} K &\to \infty
  \end{align*}
\end{enumerate}

Die $K$-Entropie bestimmt die mittlere Zeit $T_m$, für die man den
Zustand eines Systems vorhersagen kann. Ein Intervall $\ell$ wächst
nach $n$ Zeitschritten zu $L \sim \ell \exp(\lambda n)$. Ist
$L > 1$, so können wir die Trajektorie nicht mehr lokalisieren,
d.h.\ genaue Vorhersagen sind nur für Zeiten $n<T_m$ möglich. Somit
können wir schreiben
\begin{gather*}
  L \sim \ell \ee^{\lambda T_m}
  \implies
  T_m \sim \frac1\lambda \ln\left(\frac1\ell\right) \\
  \boxed{ T_m \sim \frac1K \ln\left(\frac1\ell\right) }
\end{gather*}
$\ell$ ist die Genauigkeit der Lokalisierung des
Anfangszustandes. Im letzten Schritt haben wir den Liapunov-Exponent
$\lambda$ mit der Kolmogorov-Entropie ersetzt und so $T_m$ auf
höhere Dimensionen generalisiert. Wichtig ist hierbei, dass $T_m$
von $\ell$ nur logarithmisch beeinflusst wird.

Wie wir sehen werden, stellt die Kolmogorov-Entropie eine fundamentale
Größe dar, mit der wir chaotisches Verhalten charakterisieren
können. Ein Attraktor mit positiver Kolmogorov-Entropie kann somit als
seltsamer Attraktor definiert werden.

\subsection{Charakterisierung des Attraktors durch ein gemessenes Signal}

Frage: Können wir aus der Messung des Zeitsignals einer chaotischen
Trajektorie $\bm x(t) = (x_1(t),x_2(t),\ldots)$ auf Eigenschaften des
Attraktors (Fraktale Dimension, $K$-Entropie) schließen?

Betrachte dazu die Diskretisierung der Trajektorie $\bm x(t=0)$,
$\bm x(t=\tau)$, \ldots, $\bm x(t=N\tau)$ auf dem seltsamen Attraktor
und die Aufteilung des Phasenraumes in Zellen mit Kantenlänge
$\ell$. Dann ist
\begin{equation*}
  P_i = \lim_{N\to\infty} \frac{N_i}{N}
\end{equation*}
die Wahrscheinlichkeit die Trajektorie in Zelle $i$ zu finden mit
$N_i$ der Zahl der Punkte $\bm x(t=j\tau)$ in Kästchen $i$
($i=1,\ldots,M(\ell)$).

Es ist nützlich die Begriffe der Dimension und Entropie zu
verallgemeinern.
\begin{enumerate}
\item \acct{Verallgemeinerte Dimension} $D_q$:
  \begin{equation*}
    \boxed{D_q = \lim_{\ell\to0} \frac{1}{q-1}
    \frac{\ln\left(\sum_{i=1}^{M(\ell)} P_i^q\right)}{\ln \ell}
    \;,\quad q = 0,1,2,\ldots}
  \end{equation*}
  \begin{itemize}
  \item Im Spezialfall $q=0$ erhalten wir wieder die
    Hausdorff-Dimension
    \begin{equation*}
      D_0 = - \lim_{\ell\to0} \frac{\ln M(\ell)}{\ln \ell}
    \end{equation*}

  \item Für $q=1$ müssen wir den Vorfaktor speziell behandeln
    \begin{equation*}
      D_1 = \lim_{\ell\to0} \frac{S(\ell)}{\ln \ell}
      \;,\quad\text{mit }
      S(\ell) = - \sum_{i=1}^{M(\ell)} P_i \ln P_i
    \end{equation*}
    Dies bezeichnen wir als Informationsdimension.

  \item $D_2$, also für $q=2$ heißt \acct{Korrelationsdimension}.
  \end{itemize}
  Es lässt sich zeigen, dass
  \begin{equation*}
    D_{q'} \le D_q \text{ für } q' > q
  \end{equation*}

\item \acct{Verallgemeinerte Entropie} $K_q$:
  \begin{equation*}
   \boxed{ K_q = - \lim_{\ell\to0} \lim_{n\to\infty} \frac{1}{q-1} \frac{1}{n}
    \ln \sum_{i_0\ldots i_n} P_{i_1\ldots i_n}^q }
  \end{equation*}
  Der Limes $\lim_{q\to1} K_q = K_1 = K$ liefert die $K$-Entropie.
\end{enumerate}

Die Bestimmung der Wahrscheinlichkeiten aus einer chaotischen
Trajektorie (und damit die Berechnug der $D_q$ und $K_q$) ist im
Prinzip möglich, aber sehr aufwändig. Wir suchen einen
\enquote{einfacheren} Weg, dazu betrachte nur Kästchen, in denen wirklich
Punkte des Attraktors liegen. Ersetze hierzu die gleichmäßig
verteilten Kästchen in $\sum_i P_i^q$ durch die Summe über nicht
gleichmäßig verteilte Kästchen um Punkte $x_j$ der Zeitreihe, die
z.B.\ durch eine Abbildung $x_{j+1} = f(\bm x_j)$ generiert werden.
\begin{align*}
  \sum_i P_i^q
  &= \sum_i \left[ \int_{\text{Kästchen}} \varrho(\bm x) \diff^d x \right]^q \\
  &\approx \sum_i \left[ \varrho(x_i) \ell^d \right]^q \\
  &= \sum_i \varrho(x_i) \ell^d \left[ \varrho(x_i) \ell^d \right]^{q-1} \\
  &\approx \int \varrho(x) \diff^d x\ \tilde P(x)^{q-1} \\
  &\approx \frac{1}{N} \sum_j \left[ \tilde P[f^j(x_0)] \right]^{q-1} \\
  &= \frac{1}{N} \sum_j \tilde P_j^{q-1}
\end{align*}
Dabei ist $x_i$ ein Element im Kästchen $i$ und
\begin{equation*}
  \tilde P[f^j(x_o)] \equiv \tilde P(x_j) \equiv \tilde P_j
\end{equation*}
ist die Wahrscheinlichkeit die Trajektorie in einem Kästchen der Größe
$\ell$ um die Trajektorie $x_j = f^j(x_0)$ zu finden.

Die Textdefinition für $\tilde P_j$ lautet umgesetzt als Formel:
\begin{equation*}
  \tilde P_j = \frac{1}{N} \sum_i \Theta(\ell - |\bm x_i - \bm x_j|)
  \quad\text{mit}\quad
  \Theta =
  \begin{cases}
    0 &,x<0 \\
    1 &,x\ge0 \\
  \end{cases}
\end{equation*}
und $\bm x_i = \bm x_i(t=i\tau)$. Damit:
\begin{align*}
  \sum_i P_i^q = \frac{1}{N} \sum_{j=1}^N
  \biggl( \frac{1}{N} \sum_{i=1}^N \Theta(\ell - |\bm x_i - \bm x_j|) \biggr)^{q-1}
  \equiv C_q(\ell)
\end{align*}
Wichtig ist der Spezialfall $q=2$, der als \acct{Korrelationssignal}
bezeichnet wird $C_2(\ell) \equiv C(\ell)$. Dieses misst die
Wahrscheinlichkeit dafür zwei Punkte auf einem Attraktor in einer Zelle
der Größe $\ell$ zu finden. Hieraus lassen sich bestimmen:
\begin{itemize}
\item Die \acct{Korrelationsdimension} $D_2$
  \begin{equation*}
    D_2 = \lim_{\ell\to0} \frac{\ln\sum_i P_i^2}{\ln\ell} < D_0
  \end{equation*}
  untere Grenze für die Hausdorff-Dimension. $D_2$ ist die Steigung
  der Kurve bei Auftragung von $\ln C(\ell)$ über $\ln\ell$.
  \begin{align*}
    \text{Henon-Abbildung}\quad D_2 &= \num{1.21} \\
    \text{Lorenz-Modell}\quad D_2 &= \num{2.05}
  \end{align*}
\end{itemize}



%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: