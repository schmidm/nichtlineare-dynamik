\subsection{Anwendung der Gutzwiller-Spurformel}

\subsubsection{Interpretation der Quantenspektren klassisch chaotischer
  Systeme}
Dies ist besonders elegant für Systeme mit klassischer
Skalierungseigenschaft.
\begin{example}[Wasserstoffatom im Magnetfeld:]
  Die Hamiltonfunktion ist durch
  \begin{align*}
    H &= \frac{1}{2} \bm{p}^2 + \frac{1}{8} \gamma^2(x^2+y^2) - \frac{1}{r} = E,\\
    \gamma &= \frac{B}{\SI{2.35e5}{\tesla}}
  \end{align*}
  gegeben. Es gilt zu beachten, dass die Magnetfeldstärke in atomic
  units dargestellt ist. Es gilt die klassische Skalierung
  \begin{align*}
    \bm{r} &\to \tilde{\bm{r}} \gamma^{2/3}, \\
    \bm{p} &\to \tilde{\bm{p}} \gamma^{-1/3}.
  \end{align*}
  Die skalierte Hamiltonfunktion nimmt dann die Form 
  \begin{align*}
    \tilde{H} &= H \gamma^{-2/3} = \frac{1}{2} \tilde{\bm{p}}^2 - \frac{1}{\tilde{r}} + \frac{1}{8} \left(\tilde{x}^2+\tilde{y}^2 \right) = \tilde{E} = E \gamma^{-2/3}.
  \end{align*}
  an. Die Wirkung ist
  \begin{align*}
    S_{\mathrm{PO}} &= \tilde{S}_{\mathrm{PO}} \underbrace{\gamma^{-1/3}}_{=\omega},
  \end{align*}
  mit der skalierten Wirkung
  \begin{align*}
    \tilde{S}_{\mathrm{PO}} &= \oint \tilde{\bm{p}}\cdot \diff \tilde{\bm{r}}. 
  \end{align*}
  Wir erhalten dann aus der semiklassischen Spurformel für die
  Zustandsdichte bei fester skalierter Energie $E \gamma^{-2/3}$
  \begin{align*}
    d_{\mathrm{SCL}}(\omega) &= \overline{d}(\omega) + \sum_{\mathrm{PO}} \mathcal{A}_{\mathrm{PO}} \cos\left(\tilde{S}_{\mathrm{PO}} \omega - \frac{\pi}{2} \sigma_{\mathrm{PO}} \right),
  \end{align*}
  wobei $\tilde{S}_{\mathrm{PO}}$ einer Oszillation mit konstanter
  Frequenz und Amplitude entspricht. Mittels einer
  Fouriertransformation in $\omega$ erhalten wir das Spektrum
  \begin{align*}
    \mathcal{F}(\tilde{S}) &= \int\limits_{\omega_1}^{\omega_2} d(\omega) \ee^{\ii \tilde{S} \omega} \diff \omega.
  \end{align*}
  Quantenmechanisch sind die Eigenwerte für $\omega = \gamma^{-1/3}$
  \begin{align*}
    d_{\mathrm{qm}} &= \sum_{n} \delta (\omega-\omega_n).
  \end{align*}
\end{example}

\subsubsection{Berechnung semiklassischer Spektren mittels klassischer
  periodischer Bahnen}
Die semiklassische Spurformel ist durch
\begin{align*}
  d_{\mathrm{SCL}} &= \overline{d}(E) + \sum_{\mathrm{PO}} \mathcal{A}_{\mathrm{PO}} \cos\left( \frac{1}{\hbar} S_{\mathrm{PO}} - \frac{\pi}{2}\sigma_{\mathrm{PO}} \right) \\
  &= \overline{d} + d_{\mathrm{OSC}}
\end{align*}
gegeben, wobei $\mathcal{A}_{\mathrm{PO}}$ eine Amplitude ist. Das
Problem, welches sich nun stellt ist, dass es unendlich viele
periodische Bahnen gibt, bzw.\ bereits unendlich viele Repetitionen
einer einzigen primitiven periodische Bahn. Daher gilt nun im
Folgenden die Annahme, dass es nur eine isolierte instabile primitive
periodische Bahn (PPO) (und ihre Repetitionen) gibt. Für ein System
mit $N=2$ Freiheitsgraden erhalten wir dann
\begin{align*}
  d_{\mathrm{OSC}} &= \frac{T_{\mathrm{PPO}}}{\pi\hbar} \mathrm{Re} \sum_{r=1}^{\infty} \frac{ \ee^{\ii (S_{\mathrm{PPO}} - \pi \sigma_{\mathrm{PP0}}/2)r }  }{ \sqrt{ \left| \det\left( M_{\mathrm{PPO}}^r - \mathds{1} \right) \right| } },
\end{align*}
wobei $M_{\mathrm{PPO}}$, die symplektische Monodromiematrix, die
Eigenwerte $\lambda = \ee^u$ und $1/\lambda = \ee^{-u}$ enthält. Die
Determinante im Nenner ist demnach
\begin{align*}
  \left|\det\left(M_{\mathrm{PPO}}^r - \mathds{1} \right)   \right| &= \left( \ee^{\mu r} -1 \right)\left(\ee^{-\mu r} -1 \right) \\
  &= 4 \sinh^2\left( \frac{\mu r}{2} \right).
\end{align*}
Berücksichtigen wir die Wurzel, sowie $\mu r \gg 0$ kann der Nenner
wie folgt umgeschrieben werden
\begin{align*}
  \sqrt{\left|\det\left(M_{\mathrm{PPO}}^r - \mathds{1} \right)   \right|} &= 2 \sinh\left(\frac{\mu r}{2} \right) \approx \ee^{\frac{u r}{2}}.
\end{align*}
Damit erhalten wir
\begin{align*}
  \sum_{r=1}^{\infty} \frac{ \ee^{\ii \left( S_{\mathrm{PPO}} - \frac{1}{2}\pi \sigma_{\mathrm{PPO}}\right)r }}{ \sqrt{ \left| \det \left( M_{\mathrm{PPO}}^r - \mathds{1} \right) \right| }} \approx \sum_{r=1}^{\infty} \left[  \ee^{\ii\left( S_{\mathrm{PPO}} - \frac{\pi}{2}\sigma_{\mathrm{PPO}}- \frac{1}{2} u_{\mathrm{PPO}} \right)} \right]^r. 
\end{align*}
Unter Verwendung der geometrischen Reihe
\begin{align*}
  \sum_{r=1}^{\infty} z^r &= \frac{1}{1-z} ,
\end{align*}
die absolut konvergent für $|z|<1$ ist, nimmt der oszillierende Teil
der Zustandsdichte die Form
\begin{align*}
  d_{\mathrm{OSC}} &= \frac{T_{\mathrm{PPO}}}{\pi \hbar} \mathrm{Re}\left[ \frac{\ee^{\ii \left( S_{\mathrm{PPO}} - \frac{\pi}{2} \sigma_{\mathrm{PPO}}  \right) - \frac{1}{2} u_{\mathrm{PPO}}}}{1 - \ee^{\ii \left( S_{\mathrm{PPO}} - \frac{\pi}{2} \sigma_{\mathrm{PPO}}  \right) - \frac{1}{2} u_{\mathrm{PPO}}} }\right]
\end{align*}
an. Verschwindet der Nenner, so haben wir Pole in der Zustandsdichte.

\begin{example}[Offene Systeme:]
  \begin{itemize}
  \item Die Hamiltonfunktion des invertierte harmonische Oszillators
    ist
    \begin{align*}
      H &= \frac{1}{2m} (p_x^2 + p_y^2) + \frac{m}{2} (\omega_x^2x^2-\omega_y^2y^2).
    \end{align*}
    Es existiert eine isolierte instabile periodische Bahn entlang der
    $x$-Achse:
    \begin{align*}
      d_{\mathrm{OSC}}(E) &= \frac{1}{\hbar \omega_x} \sum_{r=1}^{\infty} \frac{(-1)^r}{ \sinh\left(r \pi \frac{\omega_x}{\omega_y} \right)} \cos\left( \frac{2 \pi r E}{ \hbar \omega_x} \right). 
    \end{align*}
  \item Der Stark Effekt bei $E>0$. Die Hamiltonfunktion ist
    \begin{align*}
      H &= \frac{1}{2}\bm{p}^2 - \frac{1}{r} + F z = E > 0.
    \end{align*}
    Es existiert eine isolierte periodische Bahn entlang der
    (positiven) $x$-Achse.
  \end{itemize}
\end{example}

Typisch für gebundene chaotische Systeme ist, dass die Anzahl der
(primitiven) periodischen Bahnen exponentiell mit der Wirkung wächst
\begin{align*}
  \boxed{\#(\text{PO mit $S_{\mathrm{PO}} <S$}) \propto \ee^{\mu S}}
\end{align*}
wobei $\mu$ als eine Art Entropie aufgefasst werden kann ($\mu >0$).

Zudem ist die Konvergenz der PO-Summe nicht gesichert, denn
\begin{align*}
  \boxed{ \mathcal{A}_{\mathrm{PO}} \propto \ee^{-\frac{1}{2} \lambda_{\mathrm{PPO}} S_{\mathrm{PPO}}} }
\end{align*}
Hier ist $\lambda >0$ der Liapunov Exponent. Die Spurformel ist jedoch
für
\begin{align*}
  \boxed{\lambda > 2 \mu}
\end{align*}
absolut konvergent.

Typische Systeme weisen einen Liapunov Exponenten mit
$\lambda < 2 \mu$ auf, weshalb keine (absolute) Konvergenz der
Spurformel auftritt.

Ein einfacher Versuch besteht darin die Bahnsumme abzuschneiden,
bspw.\ bei $S_{\mathrm{PO}} < S_{\mathrm{max}}$. Damit erhalten wir
die Zustandsdichte in niedriger Auflösung. Eventuell ist es dann
möglich einzelne semiklassische Eigenwerte zu identifizieren.

\begin{notice}[Wunsch:]
  Berechnung möglichst vieler und genauer semiklassischer Eigenwerte
  mittles einer möglichst geringen Anzahl von Bahnen.
\end{notice}

Entwicklung verschiedener Resummationstechniken zur Beschleunigung der
Konvergenz semiklassischer Spurfomeln. Spezielle Methoden:
\begin{itemize}
\item Cycle-Expansion (Eckhard und Civitanovic, 1989)
\item Pseudo-Orbit Entwicklung (Berry und Keating, 1990)
\item Harmonische Inversion (Anwendung nicht linearer Verfahren zur
  hochauflösenden Signalanalyse)
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../NichtlineareDynamik.tex"
%%% End: